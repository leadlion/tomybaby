<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Xnotif
 */
namespace Amasty\Xnotif\Block\Product\View\Type;

class Configurable
{
    protected $_moduleManager;
    protected $_jsonEncoder;
    protected $_registry;

    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Registry $registry
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_jsonEncoder = $jsonEncoder;
        $this->_registry = $registry;
    }

    public function beforeGetAllowProducts($subject)
    {
        if (!$subject->hasAllProducts()) {
            $allProducts = $subject->getProduct()->getTypeInstance(true)
                ->getUsedProducts($subject->getProduct());
            $subject->setAllowProducts($allProducts);
            $subject->setAllProducts(true);

            $subject->hasAllowProducts();
        }
        return $subject->getData('allow_products');
    }

    public function afterFetchView($subject, $html)
    {
        if (in_array($subject->getNameInLayout(), ['product.info.options.configurable', 'product.info.options.swatches'])
            && !$this->_moduleManager->isEnabled('Amasty_Stockstatus')
            && !$this->_registry->registry('amasty_xnotif_initialization')
        ) {
            $this->_registry->register('amasty_xnotif_initialization', 1);

            /*move creating code to Amasty\Xnotif\Plugins\ConfigurableProduct\Data */
            $aStockStatus = $this->_registry->registry('amasty_xnotif_data');
            $aStockStatus['changeConfigurableStatus'] = true;
            $data = $this->_jsonEncoder->encode($aStockStatus);

            $html
                = '<script type="text/x-magento-init">
                    {
                        ".product-options-wrapper": {
                                    "amnotification": {
                                        "xnotif": ' . $data . '
                                    }
                         }
                    }
                   </script>' . $html;

        }
        return $html;
    }
}
