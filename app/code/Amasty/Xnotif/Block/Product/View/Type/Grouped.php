<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Xnotif
 */
namespace Amasty\Xnotif\Block\Product\View\Type;

class Grouped
{
    public function afterGetTemplate($subject, $result)
    {
        if (strpos($result, 'type/grouped.phtml') !== false) {
            $result = "Amasty_Xnotif::product/view/type/grouped.phtml";
        }

        return $result;
    }
}
