<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Xnotif
 */


namespace Amasty\Xnotif\Plugins;

class Url
{
    protected $data;

    protected $_registry;

    protected $_productId;

    protected $_urlHash;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Amasty\Xnotif\Model\UrlHash $urlHash
    ) {
        $this->_registry = $registry;
        $this->_urlHash = $urlHash;
        $this->data = $registry->registry('amxnotif_data');
    }

    protected function getType($subject)
    {
        $type = null;
        if ($subject instanceof \Magento\ProductAlert\Block\Email\Price) {
            $type = 'price';
        }
        if ($subject instanceof \Magento\ProductAlert\Block\Email\Stock) {
            $type = 'stock';
        }
        return $type;
    }

    public function beforeGetUrl($subject, $route = '', $params = [])
    {
        if ($this->data['guest'] && $this->data['email']) {
            if ($type = $this->getType($subject)) {
                $hash = $this->_urlHash->getHash(
                    $this->_productId,
                    $this->data['email']
                );
                $params['product_id'] = $this->_productId;
                $params['email'] = urlencode($this->data['email']);
                $params['hash'] = urlencode($hash);
                $params['type'] = $type;
            }
        }

        return [$route, $params];
    }

    public function beforeGetProductUnsubscribeUrl($subject, $productId)
    {
        $this->_productId = $productId;
    }
}