<?php

namespace Bss\Checkout\Block\Cart;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\CatalogInventory\Helper\Stock as StockHelper;

class Crosssell extends \Magento\Checkout\Block\Cart\Crosssell
{
    private $scopeConfig;
    
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Catalog\Model\Product\LinkFactory $productLinkFactory,
        \Magento\Quote\Model\Quote\Item\RelatedProducts $itemRelationsList,
        StockHelper $stockHelper,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $checkoutSession,
            $productVisibility,
            $productLinkFactory,
            $itemRelationsList,
            $stockHelper,
            $data
        );
        $this->scopeConfig = $scopeConfig;
    }
            
    public function getItems()
    {
        $limit = $this->getLimitItems();
        if ($limit) {
            $this->_maxItemCount = $limit;
        }
        return parent::getItems();
    }
    
    protected function _getCollection()
    {
        $limit = $this->getLimitItems();
        if ($limit) {
            $this->_maxItemCount = $limit;
        }
        return parent::_getCollection();
    }
    
    private function getLimitItems()
    {
        return (int) $this->scopeConfig->getValue('crosssell/general/limit',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
