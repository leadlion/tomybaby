<?php
namespace Bss\EuroPaymentEuPlatesc\Model\ResourceModel\Order\Handler;

use Magento\Sales\Model\ResourceModel\Order\Handler\State as SalesState;
use Magento\Sales\Model\Order;

/**
 * Class State
 */
class State extends SalesState
{
    /**
     * Change order status with pending payment
     * @param Order $order
     * @return \Magento\Sales\Model\ResourceModel\Order\Handler\State
     */
    public function check(Order $order)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/order_state.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $currentState = $order->getState();
        $paymentMethod = $order->getPayment()->getMethod();
        if ($paymentMethod == 'eppay') {
            if (($currentState == Order::STATE_NEW || $currentState == 'pending_payment') && $order->getIsInProcess()) {
                $order->setState(Order::STATE_PROCESSING)
                ->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING));
                $currentState = Order::STATE_PROCESSING;

                $logger->info('order: ' .$order->getId());
                $logger->info('STATE_PROCESSING');
            }
        } else {
            if ($currentState == Order::STATE_NEW && $order->getIsInProcess()) {
                $order->setState(Order::STATE_PROCESSING)
                    ->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING));
                $currentState = Order::STATE_PROCESSING;

                $logger->info('order: ' .$order->getId());
                $logger->info('STATE_PROCESSING');
            }
        }

        if (!$order->isCanceled() && !$order->canUnhold() && !$order->canInvoice()) {
            if (in_array($currentState, [Order::STATE_PROCESSING, Order::STATE_COMPLETE])
                && !$order->canCreditmemo()
                && !$order->canShip()
            ) {
                $order->setState(Order::STATE_CLOSED)
                    ->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_CLOSED));

                $logger->info('order: ' .$order->getId());
                $logger->info('STATE_CLOSED');
            } elseif ($currentState === Order::STATE_PROCESSING && !$order->canShip()) {
                $order->setState(Order::STATE_COMPLETE)
                    ->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_COMPLETE));

                $logger->info('order: ' .$order->getId());
                $logger->info('STATE_COMPLETE');
            }
        }
        return $this;
    }
}
