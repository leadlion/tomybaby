<?php

namespace Bss\IksanikaProductmanage\Controller\Adminhtml\Product;

use Iksanika\Productmanage\Controller\Adminhtml\Product\SaveConfig as IksanikaSaveConfig;
use Magento\Backend\App\Action;
use Magento\Catalog\Controller\Adminhtml\Product;
use Magento\Backend\Model\Auth\Session;

class SaveConfig extends IksanikaSaveConfig
{    
    private $authSession;
    
    private $scopeConfig;
    
    public function __construct(
        Action\Context $context,
        Product\Builder $productBuilder,
        \Magento\Framework\App\Config $config,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Iksanika\Productmanage\Helper\Data $helper,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\App\Cache\Manager $cacheManager,
        \Magento\Framework\App\Cache\Type\Config $cacheTypeConfig,
        Session $authSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->authSession = $authSession;
        $this->scopeConfig = $scopeConfig;
        parent::__construct(
            $context,
            $productBuilder,
            $config,
            $resourceConfig,
            $helper,
            $cacheTypeList,
            $cacheFrontendPool,
            $cacheManager,
            $cacheTypeConfig
        );
    }
    
    protected function saveConfig($pathId, $value, $scope = 'default', $scopeId = 0)
    {
        if ($pathId == 'iksanika_productmanage/columns/showcolumns') {
            $oldValue = $this->scopeConfig->getValue($pathId);
            $userId = $this->authSession->getUser()->getId();
            $showColumns = [];
            if ($oldValue) {
                $showColumns = unserialize($oldValue);
            }
            $showColumns[$userId] = '';
            if ($value) {
                $valueArr = explode(',', $value);
                $showColumns[$userId] = $valueArr;
            }
            $value = serialize($showColumns);
        }
        return parent::saveConfig($pathId, $value, $scope, $scopeId);
    }
}
