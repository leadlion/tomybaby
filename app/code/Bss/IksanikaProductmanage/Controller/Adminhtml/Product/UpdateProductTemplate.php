<?php

namespace Bss\IksanikaProductmanage\Controller\Adminhtml\Product;

use Iksanika\Productmanage\Controller\Adminhtml\Product\Builder;
use Magento\Framework\Controller\ResultFactory;
use Magento\Catalog\Controller\Adminhtml\Product;
use Magento\Catalog\Model\ProductFactory;

class UpdateProductTemplate extends Product
{    
    private $productFactory;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Product\Builder $productBuilder,
        ProductFactory $productFactory
    ) {
        parent::__construct(
            $context,
            $productBuilder
        );
        $this->productFactory = $productFactory;
    }

    public function execute()
    {
        $productIds = (array) $this->getRequest()->getParam('product');
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $productTemplateId = (int) $this->getRequest()->getParam('product_template');
        $isUpdated = 0;
        foreach ($productIds as $itemId => $productId) {
            try {
                $product = $this->productFactory->create();
                if ($productId) {
                    try {
                        $product->setStoreId($storeId)->load($productId);
                    } catch (\Exception $e) {
                        $product->setTypeId(\Magento\Catalog\Model\Product\Type::DEFAULT_TYPE);
                        $this->logger->critical($e);
                    }

                    $product->setData('attribute_set_id', $productTemplateId)->save();
                    $product->setStoreId($storeId)->save();
                    $isUpdated++;
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage(__('Error in product %1', $productId).' : '.$e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something went wrong while updating the product(s) template. %1', $e->getMessage()));
            }
        }
        $this->messageManager->addSuccessMessage(__('A total %1 out of %2 record(s) have been updated.', $isUpdated, count($productIds)));

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('productmanage/*/', ['store' => $storeId]);
    }
}
