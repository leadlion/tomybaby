<?php

namespace Bss\IksanikaProductmanage\Helper;

use Magento\Backend\Model\Auth\Session;

class DataIksanika extends \Iksanika\Productmanage\Helper\Data
{
    protected static $authSessionAdmin = null;
    
    private $authSession;
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection $productAttrCollection,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $productLinkFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        Session $authSession
    ) {
        $this->authSession = $authSession;
        parent::__construct(
            $context,
            $storeManager,
            $productAttrCollection,
            $localeCurrency,
            $mediaConfig,
            $productLinkFactory,
            $productRepository
        );
    }
    
    public static function prepareColumnSettings()
    {
        $user = self::$authSessionAdmin;
        $userId = $user->getId();
        $scopeConfig = \Iksanika\Productmanage\Helper\Data::$_scopeConfig;
        $storeSettings = $scopeConfig->getValue('iksanika_productmanage/columns/showcolumns');
        if ($storeSettings) {
            $data = unserialize($storeSettings);
        }
        if (isset($data[$userId])) {
            $tempArr = $data[$userId];
        } else {
            $tempArr = [];
        }
        
        foreach($tempArr as $showCol)
        {
            \Iksanika\Productmanage\Helper\Data::$columnSettings[trim($showCol)] = true;
        }
    }
    
    public static function getColumnSettings()
    {
        if(count(\Iksanika\Productmanage\Helper\Data::$columnSettings) == 0)
        {
            self::prepareColumnSettings();
        }
        return self::$columnSettings;
    }
    
    public function colIsVisible($code)
    {
        $user = $this->authSession->getUser();
        $this->setAuthSession($user);
        $columnSettings = self::getColumnSettings();
        return isset($columnSettings[$code]);
    }
    
    public static function setAuthSession($authSession)
    {
        self::$authSessionAdmin = $authSession;
    }

    public static function getColumnForUpdate()
    {
        $fields = array('product');
        
        if (count(self::getColumnSettings())) {
            foreach(self::getColumnSettings() as $columnId => $status) {
                $fields[] = $columnId;
            }
        }
        return $fields;
    }
}
