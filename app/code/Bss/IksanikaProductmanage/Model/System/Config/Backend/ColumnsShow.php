<?php

namespace Bss\IksanikaProductmanage\Model\System\Config\Backend;

use Magento\Framework\App\Config\Value as ConfigValue;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ColumnsShow extends ConfigValue
{
    private $authSession;
    
    private $scopeConfig;
    
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        Session $authSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->authSession = $authSession;
        $this->scopeConfig = $scopeConfig;
        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $resource,
            $resourceCollection,
            $data
        );
    }
    
    public function beforeSave()
    {
        $userId = $this->authSession->getUser()->getId();
        $value = $this->getValue();
        $oldValue = $this->scopeConfig->getValue('iksanika_productmanage/columns/showcolumns');
        $showColumns = [];
        if ($oldValue) {
            $showColumns = unserialize($oldValue);
        }
        $showColumns[$userId] = $value;
        $this->setValue(serialize($showColumns));
    }

    protected function _afterLoad()
    {
        $userId = $this->authSession->getUser()->getId();
        $value = $this->getValue();
        $showColumns = [];
        if ($value) {
            $decodedValue = unserialize($value);
            if (isset($decodedValue[$userId])) {
                $showColumns = $decodedValue[$userId];
            }
        }
        $this->setValue($showColumns);
    }
}