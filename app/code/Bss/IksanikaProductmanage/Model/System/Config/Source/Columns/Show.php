<?php

namespace Bss\IksanikaProductmanage\Model\System\Config\Source\Columns;

class Show extends \Iksanika\Productmanage\Model\System\Config\Source\Columns\Show
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {        
        $columns = [
            [
                'value' => 'entity_id',   
                'label' => __('ID')
            ],
            [
                'value' => 'type_id',   
                'label' => __('Type (simple, bundle, etc)')
            ],
            [
                'value' => 'attribute_set_id',   
                'label' => __('Attribute Set')
            ],
            [
                'value' => 'qty',   
                'label' => __('Quantity')
            ],
            [
                'value' => 'is_in_stock',   
                'label' => __('Is in Stock')
            ],
            [
                'value' => 'websites',   
                'label' => __('Websites')
            ],
            [
                'value' => 'category_ids',   
                'label' => __('Category ID\'s')
            ],
            [
                'value' => 'category',   
                'label' => __('Categories')
            ],
            [
                'value' => 'related_ids',   
                'label' => __('Related: Relative Products IDs')
            ],
            [
                'value' => 'cross_sell_ids',   
                'label' => __('Related: Cross-Sell Products IDs')
            ],
            [
                'value' => 'up_sell_ids',   
                'label' => __('Related: Up-Sell Products IDs')
            ],
            [
                'value' => 'associated_groupped_ids',
                'label' => __('Associated IDs: for Groupped')
            ],
            [
                'value' => 'associated_configurable_ids',
                'label' => __('Associated IDs: for Configurable')
            ],
        ];
        $columns_value = array_column($columns, 'value');
        
        $columnsCollection = $this->eavAttributeCollection->setEntityTypeFilter($this->eavEntity->setType('catalog_product')->getTypeId())->addFilter('is_visible', 1);
        
        foreach($columnsCollection->getItems() as $column) 
        {
            if($column->getAttributeCode() != 'quantity_and_stock_status' && !in_array($column->getAttributeCode(), $columns_value))
            {
                $columns[] = [
                    'value' => $column->getAttributeCode(),   
                    'label' => $column->getFrontendLabel()
                ];
            }
        }
        
        return $columns;
    }
}
