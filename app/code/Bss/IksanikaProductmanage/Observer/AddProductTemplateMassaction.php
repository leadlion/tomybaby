<?php

namespace Bss\IksanikaProductmanage\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;


class AddProductTemplateMassaction extends \Iksanika\Productmanage\Block\Adminhtml\Product\Grid implements ObserverInterface 
{
    public function execute(EventObserver $observer)
    {
        $block = $observer->getEvent()->getBlock();
        $auth = $block->_authorization;
        if ($auth->isAllowed('Iksanika_Productmanage::ma_update_attributes')) {
            $productTemplateOptions = $this->_setsFactory->create()->setEntityTypeFilter(
                $this->_productFactory->create()->getResource()->getTypeId()
            )->load()->toOptionHash();
            
            $block->getMassactionBlock()->addItem('productTemplateActionDivider', $this->getDivider("Product Template"));
            $block->getMassactionBlock()->addItem(
                'addProductTemplate',
                [
                    'label' => __('Update: Product Template'),
                    'url' => $this->getUrl('productmanagecustom/*/updateProductTemplate', ['_current' => true]),
                    'additional' => [
                        'visibility' => [
                            'name' => 'product_template',
                            'type' => 'select',
                            'class' => 'required-entry',
                            'label' => __('Product Template: '),
                            'values' => $productTemplateOptions
                        ]
                    ]
                ]
            );
        }
    }
}
