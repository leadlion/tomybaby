<?php

namespace Bss\IksanikaProductmanage\Plugin\Product;

use Magento\Backend\Model\Auth\Session;

class MassUpdateProducts
{
	private $authSession;

	private $helperData;

	public function __construct(
		\Iksanika\Productmanage\Helper\Data $helperData,
        Session $authSession
    ) {
        $this->authSession = $authSession;
        $this->helperData = $helperData;
    }

	public function beforeExecute(\Iksanika\Productmanage\Controller\Adminhtml\Product\MassUpdateProducts $subject)
	{
		$user = $this->authSession->getUser();
        $this->helperData->setAuthSession($user);
		return [];
	}
}
