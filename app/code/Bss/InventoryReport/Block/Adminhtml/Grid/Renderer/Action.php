<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Block\Adminhtml\Grid\Renderer;

use Magento\Framework\DataObject;
use Magento\Backend\Block\Context;
use Bss\InventoryReport\Model\ReportFactory;
use Magento\Backend\Helper\Data;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;

class Action extends AbstractRenderer
{
    /**
     * @var ReportFactory
     */
    private $reportFactory;
    /**
     * @var Data
     */
    private $adminhtmlData;
    /**
     * @var OrderInterface
     */
    private $orderIn;

    /**
     * Action constructor.
     * @param Context $context
     * @param ReportFactory $reportFactory
     * @param Data $adminhtmlData
     * @param OrderInterface $orderIn
     */
    public function __construct(
        Context $context,
        ReportFactory $reportFactory,
        Data $adminhtmlData,
        OrderInterface $orderIn
    ) {
        parent::__construct($context);
        $this->reportFactory = $reportFactory;
        $this->adminhtmlData = $adminhtmlData;
        $this->orderIn = $orderIn;
    }

    /**
     * Action Column Render
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $value = $row['report_id'];
        if (isset($row['report_id'])) {
            $report = $this->reportFactory->create()->load($value);
            $orderCheck = $report->getCreateCheck();
            $cancelCheck = $report->getCancelCheck();
            $memoCheck = $report->getMemoCheck();
            if ($orderCheck == '1' || $cancelCheck == '1') {
                $orderIncrementId = $report->getIncrementId();
                $orderEntity = $this->orderIn->loadByIncrementId($orderIncrementId)->getId();
                $orderUrl = $this->adminhtmlData->getUrl('sales/order/view', ['order_id' => $orderEntity]);
                return '<a href="'.$orderUrl.'" target="_blank">'.__('View Order').'</a>';
            } elseif ($memoCheck == '1') {
                $memo = $report->getIncrementId();
                $memoUrl = $this->adminhtmlData->getUrl('sales/creditmemo/view', ['creditmemo_id' => $memo]);
                return '<a href="'.$memoUrl.'" target="_blank">'.__('View Memo').'</a>';
            }
        } 
    }
}