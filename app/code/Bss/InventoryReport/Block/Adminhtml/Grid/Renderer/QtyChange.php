<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Block\Adminhtml\Grid\Renderer;

use Magento\Framework\DataObject;
use Magento\Backend\Block\Context;
use Bss\InventoryReport\Model\ReportFactory;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;

class QtyChange extends AbstractRenderer
{
    /**
     * @var ReportFactory
     */
    private $reportFactory;
    /**
     * QtyChange constructor.
     * @param Context $context
     * @param ReportFactory $reportFactory
     */
    public function __construct(
        Context $context,
        ReportFactory $reportFactory
    ) {
        parent::__construct($context);
        $this->reportFactory = $reportFactory;
    }

    /**
     * Quanity Change Column Render
     * @param DataObject $row
     * @return mixed|string
     */
    public function render(DataObject $row)
    {
        $value = $row['report_id'];
        if (isset($row['report_id'])) {
            $report = $this->reportFactory->create()->load($value);
            $note = $row['note'];
            $qtyChangeTable = $report->getData('qty_change');
            if ($note == "Import Product") {
                $qtyBefore = $report->getData('qty_before');
                $qtyAfter = $report->getData('qty_after');
                $qtyChange = $qtyAfter - $qtyBefore;
                if ($qtyChange > 0) {
                    return '+ ' . $qtyChange;
                } else {
                    $qtyChange = abs($qtyChange);
                    return '- ' . $qtyChange;
                }
            } else {
                return $qtyChangeTable;
            }
        }
    }
}