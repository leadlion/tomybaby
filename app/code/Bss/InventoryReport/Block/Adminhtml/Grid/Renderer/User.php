<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Block\Adminhtml\Grid\Renderer;

use Magento\Framework\DataObject;
use Magento\Backend\Block\Context;
use Bss\InventoryReport\Model\ReportFactory;
use Magento\Backend\Helper\Data;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;

class User extends AbstractRenderer
{
    /**
     * @var ReportFactory
     */
    private $reportFactory;
    /**
     * @var Data
     */
    private $adminhtmlData;

    /**
     * User constructor.
     * @param Context $context
     * @param ReportFactory $reportFactory
     * @param Data $adminhtmlData
     */
    public function __construct(
        Context $context,
        ReportFactory $reportFactory,
        Data $adminhtmlData
    ) {
        parent::__construct($context);
        $this->reportFactory = $reportFactory;
        $this->adminhtmlData = $adminhtmlData;
    }

    /**
     * User Column Render
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $value = $row['report_id'];
        if (isset($row['report_id'])) {
            $report = $this->reportFactory->create()->load($value);
            $customerId = $report->getCustomerId();
            $customer = $report->getUser();
            $customerUrl = $this->adminhtmlData->getUrl('customer/index/edit', ['id' => $customerId]);
            if (strpos($customer, 'Admin') === false && strpos($customer, 'Guest') === false) {
                return '<a href="'.$customerUrl.'" target="_blank">'.$customer.'</a>';
            } else {
                return $customer;
            }
        }
    }
}