<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Block\Adminhtml\Product\Edit\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Catalog\Model\Product;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Bss\InventoryReport\Model\ReportFactory;
use MEQP2\Tests\NamingConventions\true\mixed;

class Reports extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var ReportFactory
     */
    private $reportFactory;
    /**
     * @var Product
     */
    private $productFac;

    /**
     * Reports constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param ReportFactory $reportFactory
     * @param Product $productFac
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        ReportFactory $reportFactory,
        Product $productFac,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
        $this->reportFactory = $reportFactory;
        $this->productFac = $productFac;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('bss_inventory');
        $this->setUseAjax(true);
    }

    /**
     * Prepare Collection by product id
     * {@inheritdoc}
     */
    protected function _prepareCollection()
    {
        $productId = $this->getRequest()->getParam('id');
        $collection = $this->reportFactory->create()->getCollection()
            ->addFieldToFilter(
                ['product_id'],
                [
                    ['eq' => $productId],
                ]
            );
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function _prepareLayout()
    {
        $config = $this->getData();
        $productId = $this->getRequest()->getParam('id');
        $product = $this->productFac->load($productId);
        $producType = $product->getTypeId();
        $config['config']['canShow'] = true;
        if ($producType == "configurable" || $producType == "bundle" ||
            $producType == "grouped" || $producType =="downloadable") {
            $config['config']['canShow'] = false;
        }
        $this->setData('config', $config['config']);
        return parent::_prepareLayout();
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'report_id',
            [
                'header' => __('Report ID'),
                'type' => 'text',
                'index' => 'report_id'
            ]
        );

        $this->addColumn(
            'time',
            [
                'header' => __('Time'),
                'type' => 'text',
                'index' => 'time'
            ]
        );

        $this->addColumn(
            'qty_after',
            [
                'header' => __('Quantity'),
                'index' => 'qty_after'
            ]
        );

        $this->addColumn(
            'stock_status',
            [
                'header' => __('Stock Status'),
                'index' => 'stock_status'
            ]
        );

        $this->addColumn(
            'qty_change',
            [
                'header' => __('Quantity Change'),
                'renderer' => 'Bss\InventoryReport\Block\Adminhtml\Grid\Renderer\QtyChange'
            ]
        );

        $this->addColumn(
            'note',
            [
                'header' => __('Note'),
                'index' => 'note'
            ]
        );

        $this->addColumn(
            'user',
            [
                'header' => __('User'),
                'renderer' => 'Bss\InventoryReport\Block\Adminhtml\Grid\Renderer\User'
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'renderer' => 'Bss\InventoryReport\Block\Adminhtml\Grid\Renderer\Action'
            ]
        );

        $this->setFilterVisibility(false);
        $this->setPagerVisibility(true);
        $this->setSortable(true);
        return parent::_prepareColumns();
    }

    /**
     * @param Product|\Magento\Framework\DataObject $row
     * @return bool
     */
    public function getRowUrl($row)
    {
        return false;
    }
}