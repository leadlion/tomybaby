<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Helper;

class ProductStock extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @return string $backOrder
     */
    public function getBackOrderValue()
    {
        $backOrder= $this->scopeConfig->getValue(
            'cataloginventory/item_options/backorders',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $backOrder;
    }

    /**
     * @return string $backOrder
     */
    public function getThresholdValue()
    {
        $threshold= $this->scopeConfig->getValue(
            'cataloginventory/item_options/min_qty',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $threshold;
    }
}