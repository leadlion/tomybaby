<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Report extends AbstractDb
{
    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init('bss_inventoryreport', 'report_id');
    }

    /**
     * @param string $currentTime
     * @param int $productId
     * @param string $productName
     * @param string $productSku
     * @param string $stockQtyAfter
     * @param int $qtyChange
     * @param string $stockStatus
     * @param string $user
     * @param string $note
     * @param string $productType
     * @param int $customerId
     * @param int $incrementId
     * @param int $orderCheck
     * @param int $cancelCheck
     * @param int $memoCheck
     * @param int $saveCheck
     * @return void
     */
    public function saveReport(
        $currentTime,
        $productId,
        $productName,
        $productSku,
        $stockQtyAfter,
        $qtyChange,
        $stockStatus,
        $user,
        $note,
        $productType,
        $customerId,
        $incrementId,
        $orderCheck,
        $cancelCheck,
        $memoCheck,
        $saveCheck
    ) {
        $connection = $this->getConnection();
        $bind = [
            'time' => $currentTime,
            'product_id' => $productId,
            'product_name' => $productName,
            'product_sku' => $productSku,
            'qty_after' => $stockQtyAfter,
            'qty_change' => $qtyChange,
            'stock_status' => $stockStatus,
            'user' => $user,
            'note' => $note,
            'type' => $productType,
            'customer_id' => $customerId,
            'increment_id'=> $incrementId,
            'create_check' => $orderCheck,
            'cancel_check' => $cancelCheck,
            'memo_check' => $memoCheck,
            'save_check' => $saveCheck,
        ];
        $connection->insert($this->getTable('bss_inventoryreport'), $bind);
    }

    /**
     * Update product by product sku after save product
     * @param string $productSku
     * @param int $productId
     * @param string $stockStatus
     * @param int $saveCheck
     * @return void
     */
    public function updateProductId($productSku, $productId, $stockStatus, $saveCheck)
    {
        $connection = $this->getConnection();
        $where = ['product_sku IN (?)' => $productSku, 'save_check=?' => '1'];
        $connection->update($this->getTable('bss_inventoryreport'),
            ['product_id' => $productId, 'stock_status' => $stockStatus, 'save_check' =>$saveCheck], $where);
    }

    /**
     * Save Qty Import Product
     * @param string $productSku
     * @param string $note
     * @param int $qty
     * @return void
     */
    public function saveQtyImport($productSku, $note, $qty)
    {
        $connection = $this->getConnection();
        $bind = [
            'product_sku' => $productSku,
            'note' => $note,
            'qty_before' => $qty,
        ];
        $connection->insert($this->getTable('bss_inventoryreport'), $bind);
    }

    /**
     * Update Import Product
     * @param string $productSku
     * @param date/time $currentTime
     * @param int $productId
     * @param string $note
     * @param string $productName
     * @param string $stockQtyAfter
     * @param string $stockStatus
     * @param string $user
     * @param int $check
     * @return void
     */
    public function updateImportProduct(
        $productSku,
        $currentTime,
        $productId,
        $note,
        $productName,
        $stockQtyAfter,
        $stockStatus,
        $user,
        $check
    ) {
        $connection = $this->getConnection();
        $where = ['product_sku IN (?)' => $productSku,'note In (?)' =>$note, 'import_check=?' => 1];
        $connection->update(
            $this->getTable('bss_inventoryreport'),
            ['product_id' => $productId, 'time' => $currentTime, 'product_name' => $productName,
                'qty_after' => $stockQtyAfter, 'stock_status' => $stockStatus, 'user' => $user, 'import_check' => $check
            ], $where);
    }

    /**
     * Update Select Import Not Update
     * @return array
     */
    public function selectImport()
    {
        $connection = $this->getConnection();
        $select = $connection->select()->from(
            $this->getTable('bss_inventoryreport')
        )->where(
            'import_check = ?',
            1
        );
        return $data = $connection->fetchAll($select);
    }

    /**
     * Delete Import Not Update
     * @return void
     */
    public function deleteImportNotUpdate() 
    {
        $connection = $this->getConnection();
        $connection->delete(
            $this->getTable('bss_inventoryreport'),
            ['import_check = ?' => 1]
        );
    }
}
