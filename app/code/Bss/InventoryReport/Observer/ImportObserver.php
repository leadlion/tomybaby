<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Backend\Model\Auth\Session;
use Magento\Catalog\Model\ProductRepository;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Psr\Log\LoggerInterface;
use Bss\InventoryReport\Helper\ProductStock;

class ImportObserver implements ObserverInterface
{
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var Report
     */
    protected $report;
    /**
     * @var Session
     */
    protected $adminSession;
    /**
     * @var ProductRepository
     */
    protected $productRepository;
    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var BackOrder
     */
    protected $productStockConfig;
    /**
     * ImportObserver constructor.
     * @param DateTime $date
     * @param StockRegistryInterface $stockRegistry
     * @param Report $report
     * @param Session $adminSession
     * @param ProductRepository $productRepository
     * @param LoggerInterface $logger
     * @param ProductStock $productStockConfig
     */
    public function __construct(
        DateTime $date,
        StockRegistryInterface $stockRegistry,
        Report $report,
        Session $adminSession,
        ProductRepository $productRepository,
        LoggerInterface $logger,
        ProductStock $productStockConfig
    ) {
        $this->date = $date;
        $this->stockRegistry = $stockRegistry;
        $this->report = $report;
        $this->adminSession = $adminSession;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->productStockConfig = $productStockConfig;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $adapter = $observer->getEvent()->getAdapter();
            $products = $adapter->getNewSku();
            $currentTime = $this->date->gmtDate();
            $user = $this->adminSession->getUser()->getUserName();
            $user = 'Admin/'.$user;
            $check = 2;
            foreach ($products as $product) {
                $productId = $product['entity_id'];
                $productCollection = $this->productRepository->getById($productId);
                $productType = $productCollection->getTypeId();
                if ($productType == "simple" || $productType == "virtual") {
                    $productName = $productCollection->getName();
                    $productSku = $productCollection->getSku();
                    $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
                    $stockQtyAfter = $stockItem->getQty();
                    $outStockThreshold = $stockItem->getMinQty();
                    $backOrderConfigProduct = $stockItem->getBackorders();
                    $backOrderValue = $this->productStockConfig->getBackOrderValue();
                    $getUseConfigBackOrder = $stockItem->getUseConfigBackorders();
                    $note = 'Import Product';
                    if (($stockQtyAfter > $outStockThreshold) 
                        || (($backOrderValue != 0 && $getUseConfigBackOrder) 
                        || $backOrderConfigProduct != 0)
                    ) {
                        $stockStatus = "In Stock";
                    } else {
                        $stockStatus = "Out of Stock";
                    }
                    $this->report->updateImportProduct($productSku, $currentTime, $productId, $note,
                        $productName, $stockQtyAfter, $stockStatus, $user, $check);
                }
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
