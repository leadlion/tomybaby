<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Backend\Model\Auth\Session;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Psr\Log\LoggerInterface;
use Bss\InventoryReport\Helper\ProductStock;

class OrderCancelObserver implements ObserverInterface
{
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;
    /**
     * @var Report
     */
    protected $report;
    /**
     * @var Session
     */
    protected $adminSession;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var BackOrder
     */
    private $productStockConfig;

    /**
     * OrderCancelObserver constructor.
     * @param DateTime $date
     * @param StockRegistryInterface $stockRegistry
     * @param Report $report
     * @param Session $adminSession
     * @param LoggerInterface $logger
     * @param ProductStock $productStockConfig
     */
    public function __construct(
        DateTime $date,
        StockRegistryInterface $stockRegistry,
        Report $report,
        Session $adminSession,
        LoggerInterface $logger,
        ProductStock $productStockConfig
    ) {
        $this->date = $date;
        $this->stockRegistry = $stockRegistry;
        $this->report = $report;
        $this->adminSession = $adminSession;
        $this->logger = $logger;
        $this->productStockConfig = $productStockConfig;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            $currentTime = $this->date->gmtDate();
            $user = $this->adminSession->getUser()->getUserName();
            $user = 'Admin/'.$user;
            $orderInCrement = $order->getIncrementId();
            $products = $order->getAllItems();
            foreach ($products as $product) {
                $productType = $product->getProductType();
                if ($productType == "simple" || $productType == "virtual") {
                    $productId = $product->getProductId();
                    $productName = $product->getName();
                    $productSku = $product->getSku();
                    $qtyChange = $product->getQtyOrdered();
                    $qtyChange = round($qtyChange, 0);
                    $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
                    $stockQtyAfter = $stockItem->getQty();
                    $useMinQtyConfig = $stockItem->getUseConfigMinQty();
                    $minQtyConfigProduct = $stockItem->getMinQty();
                    $threshold = $this->productStockConfig->getThresholdValue();
                    $stockStatus = $this->stockRegistry->getProductStockStatusBySku($productSku);
                    if ($stockStatus == 1 || $stockQtyAfter >0) {
                        if ($useMinQtyConfig && $stockQtyAfter > $threshold) {
                            $stockStatus = "In Stock";
                        } elseif ($stockQtyAfter > $minQtyConfigProduct) {
                            $stockStatus = "In Stock";
                        } else {
                            $stockStatus = "Out of Stock";
                        }
                    } else {
                        $stockStatus = "Out of Stock";
                    }
                    $note = 'Order Cancellation No '.$orderInCrement;
                    $qtyChange = '+ '.$qtyChange;
                    $cancelcheck = '1';
                    $this->report->saveReport($currentTime, $productId, $productName, $productSku,
                        $stockQtyAfter, $qtyChange, $stockStatus, $user, $note,
                        $productType, null, $orderInCrement, null, $cancelcheck, null, null);
                }
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
