<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Bss\InventoryReport\Helper\ProductStock;
use Psr\Log\LoggerInterface;

class OrderCreateObserver implements ObserverInterface
{
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;
    /**
     * @var \Bss\InventoryReport\Model\ResourceModel\Report
     */
    private $report;
    /**
     * @var BackOrder
     */
    private $productStockConfig;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderSale;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * OrderCreateObserver constructor.
     * @param DateTime $date
     * @param StockRegistryInterface $stockRegistry
     * @param Report $report
     * @param productStock $productStockConfig
     * @param OrderRepositoryInterface $orderSale
     * @param LoggerInterface $logger
     */
    public function __construct(
        DateTime $date,
        StockRegistryInterface $stockRegistry,
        Report $report,
        ProductStock $productStockConfig,
        OrderRepositoryInterface $orderSale,
        LoggerInterface $logger
    ) {
        $this->date = $date;
        $this->stockRegistry = $stockRegistry;
        $this->report = $report;
        $this->productStockConfig = $productStockConfig;
        $this->orderSale = $orderSale;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            $currentTime = $this->date->gmtDate();
            $items = $order->getAllItems();
            $customerName = $order->getBillingAddress()->getName();
            $customerId = $order->getCustomerId();
            if (!$customerId) {
                $customerName = "Guest";
            }
            $orderNumber = $order->getIncrementId();
            foreach ($items as $item) {
                $productType = $item->getProductType();
                if ($productType == "simple" || $productType == "virtual" || $productType == "grouped") {
                    $productId = $item->getProductId();
                    $productName = $item->getName();
                    $productSku = $item->getSku();
                    $qtyChange = $item->getQtyOrdered();
                    $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
                    $stockQtyAfter = $stockItem->getQty();
                    $backOrderConfigProduct = $stockItem->getBackorders();
                    $useMinQtyConfig = $stockItem->getUseConfigMinQty();
                    $minQtyConfigProduct = $stockItem->getMinQty();
                    $stockStatus = $this->stockRegistry->getProductStockStatusBySku($productSku);
                    $backOrderValue = $this->productStockConfig->getBackOrderValue();
                    $threshold = $this->productStockConfig->getThresholdValue();
                    if ($backOrderValue != 0 || $backOrderConfigProduct != 0) {
                        $stockStatus = "In Stock";
                    } elseif ($stockStatus == 1 && $stockQtyAfter > 0) {
                        if ($useMinQtyConfig && $stockQtyAfter > $threshold) {
                            $stockStatus = "In Stock";
                        } elseif ($stockQtyAfter > $minQtyConfigProduct) {
                            $stockStatus = "In Stock";
                        } else {
                            $stockStatus = "Out of Stock";
                        }
                    } else {
                        $stockStatus = "Out of Stock";
                    }
                    $note = 'Order Creation No: '.$orderNumber;
                    $qtyChange = '- '.$qtyChange;
                    $ordercheck = '1';
                    $this->report->saveReport($currentTime, $productId, $productName, $productSku,
                        $stockQtyAfter, $qtyChange, $stockStatus, $customerName, $note,
                        $productType, $customerId, $orderNumber, $ordercheck, null, null, null);
                }
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
