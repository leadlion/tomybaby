<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Magento\Backend\Model\Auth\Session;
use Magento\Catalog\Model\ProductRepository;
use Psr\Log\LoggerInterface;
use Bss\InventoryReport\Helper\ProductStock;

class OrderRefundObserver implements ObserverInterface
{
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;
    /**
     * @var Report
     */
    private $report;
    /**
     * @var Session
     */
    private $adminSession;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var BackOrder
     */
    private $productStockConfig;

    /**
     * OrderRefundObserver constructor.
     * @param DateTime $date
     * @param StockRegistryInterface $stockRegistry
     * @param Report $report
     * @param Session $adminSession
     * @param ProductRepository $productRepository
     * @param LoggerInterface $logger
     * @param ProductStock $productStockConfig
     */
    public function __construct(
        DateTime $date,
        StockRegistryInterface $stockRegistry,
        Report $report,
        Session $adminSession,
        ProductRepository $productRepository,
        LoggerInterface $logger,
        ProductStock $productStockConfig
    ) {
        $this->date = $date;
        $this->stockRegistry = $stockRegistry;
        $this->report = $report;
        $this->adminSession = $adminSession;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->productStockConfig = $productStockConfig;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $collection = $observer->getEvent()->getCreditmemo();
            $currentTime = $this->date->gmtDate();
            $user = $this->adminSession->getUser()->getUserName();
            $user = 'Admin/' . $user;
            $creaditId = $collection->getData('entity_id');
            $items = $collection->getAllItems();
            $creaditIncrement = $collection->getIncrementId();
            $memoCheck = '1';
            foreach ($items as $item) {
                $backToStock = $item->getData('back_to_stock');
                if ($backToStock) {
                    $productId = $item->getData('product_id');
                    $productCollection = $this->productRepository->getById($productId);
                    $productType = $productCollection->getTypeId();
                    if ($productType == "simple" || $productType == "virtual") {
                        $productName = $item->getName();
                        $productSku = $item->getSku();
                        $qtyChange = $item->getQty();
                        $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
                        $stockQtyAfter = $stockItem->getQty();
                        $useMinQtyConfig = $stockItem->getUseConfigMinQty();
                        $minQtyConfigProduct = $stockItem->getMinQty();
                        $threshold = $this->productStockConfig->getThresholdValue();
                        $stockStatus = $this->stockRegistry->getProductStockStatusBySku($productSku);
                        if ($stockStatus == 1 || $stockQtyAfter >0) {
                            $stockStatus = $this->getStatus($useMinQtyConfig, $stockQtyAfter, $threshold,
                                $minQtyConfigProduct);
                        } else {
                            $stockStatus = "Out of Stock";
                        }
                        $note = 'Credit Memo Refund No: ' . $creaditIncrement;
                        $qtyChange = '+ ' . $qtyChange;
                        $this->report->saveReport($currentTime, $productId, $productName,
                            $productSku, $stockQtyAfter, $qtyChange, $stockStatus, $user,
                            $note, $productType, null, $creaditId, null, null, $memoCheck, null);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }

    /**
     * @param int $useMinQtyConfig
     * @param int $stockQtyAfter
     * @param int $threshold
     * @param int $minQtyConfigProduct
     * @return string
     */
    private function getStatus($useMinQtyConfig, $stockQtyAfter, $threshold, $minQtyConfigProduct)
    {
        if ($useMinQtyConfig && $stockQtyAfter > $threshold) {
            $stockStatus = "In Stock";
        } elseif ($stockQtyAfter > $minQtyConfigProduct) {
            $stockStatus = "In Stock";
        } else {
            $stockStatus = "Out of Stock";
        }
        return $stockStatus;
    }
}
