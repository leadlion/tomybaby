<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Observer;

use Magento\Framework\Event\ObserverInterface;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Psr\Log\LoggerInterface;

class RemoveImportCheck implements ObserverInterface
{
    /**
     * @var Report
     */
    protected $report;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * RemoveImportCheck constructor.
     * @param Report $report
     * @param LoggerInterface $logger
     */
    public function __construct(
        Report $report,
        LoggerInterface $logger
    ) {
        $this->report = $report;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $request = $observer->getEvent()->getData('request');
        $name = $request->getFullActionName();
        if ($name == "inventory_report_index" || $name == "catalog_product_edit") {
            try {
                if ($this->report->selectImport()) {
                    $this->report->deleteImportNotUpdate();
                }
            } catch (\Exception $e) {
                $this->logger->debug($e->getMessage());
            }
        }
    }
}
