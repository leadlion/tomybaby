<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Observer;

use Magento\Framework\Event\ObserverInterface;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\ProductMetadataInterface;
use Psr\Log\LoggerInterface;

class SaveAfterObserver implements ObserverInterface
{
    /**
     * @var Report
     */
    protected $report;
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;
    /**
     * @var Session
     */
    protected $adminSession;
    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * SaveAfterObserver constructor.
     * @param Report $report
     * @param RequestInterface $request
     * @param DateTime $date
     * @param StockRegistryInterface $stockRegistry
     * @param Session $adminSession
     * @param ProductMetadataInterface $productMetadata
     * @param LoggerInterface $logger
     */
    public function __construct(
        Report $report,
        RequestInterface $request,
        DateTime $date,
        StockRegistryInterface $stockRegistry,
        Session $adminSession,
        ProductMetadataInterface $productMetadata,
        LoggerInterface $logger
    ) {
        $this->report = $report;
        $this->request = $request;
        $this->date = $date;
        $this->stockRegistry = $stockRegistry;
        $this->adminSession = $adminSession;
        $this->productMetadata = $productMetadata;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $posts = $this->request->getPost();
        $id = $this->request->getParam('id');
        try {
            $version = $this->productMetadata->getVersion();
            if ($version < '2.1.0') {
                $configurableMatrix = (string)$posts['configurable-matrix-serialized'];
            } elseif ($version >= '2.2.0') {
                $configurableMatrix = (string)$posts['configurable-matrix-serialized'];
                $configurableMatrix = ltrim($configurableMatrix, '[');
                $configurableMatrix = rtrim($configurableMatrix, ']');
            } else {
                $configurableMatrix = (string)$posts['product']['configurable-matrix-serialized'];
                $configurableMatrix = ltrim($configurableMatrix, '[');
                $configurableMatrix = rtrim($configurableMatrix, ']');
            }
            if ($configurableMatrix == '') {
                $productId = $product->getId();
                $productSku = $product->getSku();
                $stockItem  = $this->stockRegistry->getStockItem($productId);
                $stockStatus = $stockItem->getIsInStock();
                if ($stockStatus == false) {
                    $stockStatus = "Out of Stock";
                } else {
                    $stockStatus = "In Stock";
                }
                $this->report->updateProductId($productSku, $productId, $stockStatus, '2');
            } elseif (!$id) {
                $productType = $product->getTypeId();
                if ($productType == "simple" || $productType == "virtual") {
                    $currentTime = $this->date->gmtDate();
                    $productId = $product->getId();
                    $productName = $product->getName();
                    $productSku = $product->getSku();
                    $stockItem = $this->stockRegistry->getStockItem($productId);
                    $stockQty = $stockItem->getQty();
                    $stockStatus = $this->stockRegistry->getProductStockStatusBySku($productSku);
                    if ($stockStatus == 1) {
                        $stockStatus = "In Stock";
                    } else {
                        $stockStatus = "Out of Stock";
                    }
                    $qtyChange = '+ '.$stockQty;
                    $admin = $this->adminSession->getUser()->getUserName();
                    $admin = 'Admin/'.$admin;
                    $note = 'Admin Manual Change';
                    if ($stockQty > 0) {
                        $this->report->saveReport($currentTime, $productId, $productName,
                            $productSku, $stockQty, $qtyChange, $stockStatus,
                            $admin, $note, null, null, null, null, null, null, null);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
