<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\RequestInterface;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\ProductMetadataInterface;
use Psr\Log\LoggerInterface;

class SaveObserver implements ObserverInterface
{
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var Report
     */
    protected $report;
    /**
     * @var Session
     */
    protected $adminSession;
    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * SaveObserver constructor.
     * @param DateTime $date
     * @param StockRegistryInterface $stockRegistry
     * @param RequestInterface $request
     * @param Report $report
     * @param Session $adminSession
     * @param ProductMetadataInterface $productMetadata
     * @param LoggerInterface $logger
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        DateTime $date,
        StockRegistryInterface $stockRegistry,
        RequestInterface $request,
        Report $report,
        Session $adminSession,
        ProductMetadataInterface $productMetadata,
        LoggerInterface $logger,
        \Magento\Framework\Registry $registry
    ) {
        $this->date = $date;
        $this->stockRegistry = $stockRegistry;
        $this->request = $request;
        $this->report = $report;
        $this->adminSession = $adminSession;
        $this->productMetadata = $productMetadata;
        $this->logger = $logger;
        $this->registry = $registry;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $currentTime = $this->date->gmtDate();
        $productId = $product->getId();
        $productName = $product->getName();
        $productSku = $product->getSku();
        $stockItem = $this->stockRegistry->getStockItem($productId);
        $stockQty = $stockItem->getQty();
        $posts = $this->request->getPost();
        $qtyChange = "";
        $this->registry->unregister('admin_save_product');
        $this->registry->register('admin_save_product', '1');
        if (isset($posts['product']['quantity_and_stock_status']['qty'])) {
            $stockQtyAfter = $posts['product']['quantity_and_stock_status']['qty'];
            $stockQtyAfter = (int)$stockQtyAfter;
            $qtyChange = $stockQtyAfter - $stockQty;
        }
        $stockStatus = "In Stock";
        $admin = $this->adminSession->getUser()->getUserName();
        $admin = 'Admin/'.$admin;
        $note = 'Admin Manual Change';
        try {
            $version = $this->productMetadata->getVersion();
            if ($version < '2.1.0') {
                $configurableMatrix = (string)$posts['configurable-matrix-serialized'];
            } elseif ($version >= '2.2.0') {
                $configurableMatrix = (string)$posts['configurable-matrix-serialized'];
                $configurableMatrix = ltrim($configurableMatrix, '[');
                $configurableMatrix = rtrim($configurableMatrix, ']');
            } else {
                $configurableMatrix = (string)$posts['product']['configurable-matrix-serialized'];
                $configurableMatrix = ltrim($configurableMatrix, '[');
                $configurableMatrix = rtrim($configurableMatrix, ']');
            }
            if ($qtyChange && $configurableMatrix == '') {
                if ($qtyChange > 0) {
                    $qtyChange = '+ '.$qtyChange;
                } else {
                    $qtyChange = abs($qtyChange);   
                    $qtyChange = '- '.$qtyChange;
                }
                $this->report->saveReport($currentTime, $productId, $productName,
                    $productSku, $stockQtyAfter, $qtyChange, $stockStatus,
                    $admin, $note, null, null, null, null, null, null, '1');
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
