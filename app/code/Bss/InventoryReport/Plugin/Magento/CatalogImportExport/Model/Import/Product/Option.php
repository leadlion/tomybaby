<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Plugin\Magento\CatalogImportExport\Model\Import\Product;

use Magento\CatalogInventory\Api\StockRegistryInterface;

class Option
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;
    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;
    /**
     * @var \Bss\InventoryReport\Model\Report
     */
    private $report;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $productModel;
    /**
     * Option constructor.
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param StockRegistryInterface $stockRegistry
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Bss\InventoryReport\Model\Report $report
     */
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        StockRegistryInterface $stockRegistry,
        \Magento\Framework\App\RequestInterface $request,
        \Bss\InventoryReport\Model\Report $report,
        \Magento\Catalog\Model\Product $productModel
    ) {
        $this->productRepository = $productRepository;
        $this->stockRegistry = $stockRegistry;
        $this->report = $report;
        $this->request = $request;
        $this->productModel = $productModel;
    }

    /**
     * @param \Magento\CatalogImportExport\Model\Import\Product\Option $subject
     * @param \Closure $proceed
     * @param array $rowData
     * @param array $rowNumber
     * @return mixed
     */
    public function aroundValidateRow(
        \Magento\CatalogImportExport\Model\Import\Product\Option $subject,
        \Closure $proceed,
        array $rowData, 
        $rowNumber
    ) {
        try {
            $productSku = $rowData['sku'];
            if (isset($rowData['product_type'])) {
                $productType = $rowData['product_type'];
            } else {
                $product = $this->productRepository->get($productSku);
                $productType = $product->getTypeId();
            } 
            $productQtyImport = $rowData['qty'];
            $filter = $this->report->getCollection()
                    ->addFieldToFilter('product_sku', $productSku)
                    ->addFieldToFilter('import_check', 1);
            $note = 'Import Product';
            $actionName = $this->request->getActionName();
            if ($productType == "simple" || $productType == "virtual" && $actionName == 'validate') {
                if (!$this->productModel->getIdBySku($productSku)) {
                    if (count($filter) < 1) {
                        $data = [
                            'product_sku'=> $productSku,
                            'note'=> $note,
                            'import_check' => 1
                            ];
                        $this->report->setData($data)->save();
                    }
                } else {
                    if (count($filter) < 1) {
                        $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
                        $stockQty = $stockItem->getQty();
                        if ($productQtyImport != $stockQty) {
                            $data = [
                                'product_sku'=> $productSku,
                                'note'=> $note,
                                'qty_before'=> $stockQty,
                                'import_check' => 1
                                ];
                            $this->report->setData($data)->save();
                        }
                    }
                }
            }
            
        } catch (\Exception $e) {
            return $proceed($rowData, $rowNumber);
        }
    }
}