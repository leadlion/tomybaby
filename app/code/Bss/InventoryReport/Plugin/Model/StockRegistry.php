<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Plugin\Model;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

class StockRegistry
{
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;
    /**
     * @var \Bss\InventoryReport\Model\ResourceModel\Report
     */
    private $report;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * StockRegistry constructor.
     * @param DateTime $date
     * @param StockRegistryInterface $stockRegistry
     * @param Report $report
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        DateTime $date,
        StockRegistryInterface $stockRegistry,
        Report $report,
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Registry $registry
    ) {
        $this->date = $date;
        $this->stockRegistry = $stockRegistry;
        $this->report = $report;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->registry = $registry;
    }

    /**
     * @param \Magento\CatalogInventory\Model\StockRegistry $subject
     * @param \Closure $proceed
     * @param string $productSku
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
     * @return mixed
     */
    public function aroundUpdateStockItemBySku(
        \Magento\CatalogInventory\Model\StockRegistry $subject,
        \Closure $proceed,
        $productSku,
        \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
    ) {
        try {
            if (!$this->registry->registry('admin_save_product')) {
                $currentTime = $this->date->gmtDate();
                $product = $this->productRepository->get($productSku);
                $productId = $product->getId();
                $productName = $product->getName();
                $stockItems = $this->stockRegistry->getStockItem($productId);
                $stockQty = $stockItems->getQty();
                $stockQtyAfter = $stockItem->getQty();
                $qtyChange = (int)$stockQtyAfter - (int)$stockQty;
                $stockStatus = $stockItems->getIsInStock();
                $user = "Admin";
                if ($stockStatus == 1 && $stockQtyAfter > 0) {
                    $stockStatus = "In Stock";
                } else {
                    $stockStatus = "Out Stock";
                }
                $note = "Change stock by REST API";
                if ($qtyChange) {
                    if ($qtyChange > 0) {
                        $qtyChange = '+ '.$qtyChange;
                    } else {
                        $qtyChange = abs($qtyChange);
                        $qtyChange = '- '.$qtyChange;
                    }
                    $this->report->saveReport($currentTime, $productId, $productName,
                        $productSku, $stockQtyAfter, $qtyChange, $stockStatus,
                        $user, $note, null, null, null, null, null, null, null);
                }
            }
            return $proceed($productSku, $stockItem);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}