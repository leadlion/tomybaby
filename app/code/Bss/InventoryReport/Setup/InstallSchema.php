<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * install tables
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('bss_inventoryreport')) {
            $table = $installer->getConnection()
                ->newTable(
                    $installer->getTable('bss_inventoryreport')
                )
                ->addColumn(
                    'report_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Report ID'
                )
                ->addColumn(
                    'product_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Product ID'
                )
                ->addColumn(
                    'time',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, 10,
                    ['nullable' => false],
                    'Time'
                )
                ->addColumn(
                    'qty_before',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    50,
                    ['nullable' => false],
                    'Quantity Before'
                )
                ->addColumn(
                    'qty_after',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    50,
                    ['nullable' => false],
                    'Quantity After'
                )
                ->addColumn(
                    'stock_status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Stock Status'
                )
                ->addColumn(
                    'note',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Note'
                )
                ->addColumn(
                    'user',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'User'
                )
                ->addColumn(
                    'product_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Product Name'
                )
                ->addColumn(
                    'product_sku',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Product Sku'
                )
                ->addColumn(
                    'qty_change',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Quantity Change'
                )
                ->addColumn(
                    'customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Customer ID'
                )
                ->addColumn(
                    'type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Type'
                )
                ->addColumn(
                    'increment_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    32,
                    ['nullable' => true],
                    'Entity ID'
                )
                ->addColumn(
                    'import_check',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    50,
                    ['nullable' => false],
                    'Import Check'
                )
                ->addColumn(
                    'create_check',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    50,
                    ['nullable' => true],
                    'Order Create Check'
                )
                ->addColumn(
                    'cancel_check',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    50,
                    ['nullable' => true],
                    'Order Cancel Check'
                )
                ->addColumn(
                    'memo_check',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    50,
                    ['nullable' => true],
                    'Memo Check'
                )
                ->addColumn(
                    'save_check',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    50,
                    ['nullable' => true],
                    'Save Check'
                )
                ->addIndex(
                    $installer->getIdxName('bss_inventoryreport', ['report_id']),
                    ['report_id']
                )
                ->setComment(
                    'Bss Inventory Report'
                );
                $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
