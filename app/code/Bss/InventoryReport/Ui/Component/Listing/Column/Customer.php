<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Bss\InventoryReport\Model\ReportFactory;
use Magento\Backend\Helper\Data;

class Customer extends \Bss\InventoryReport\Ui\Component\Listing\Column\AbstractColumn
{
    /**
     * @var ReportFactory
     */
    private $reportFactory;
    /**
     * @var Data
     */
    private $adminhtmlData;

    /**
     * Customer constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param ReportFactory $reportFactory
     * @param array $components
     * @param Data $adminhtmlData
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ReportFactory $reportFactory,
        array $components = [],
        Data $adminhtmlData,
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->reportFactory = $reportFactory;
        $this->adminhtmlData = $adminhtmlData;
    }

    /**
     * @param array $item
     * @return string[]
     */
    protected function _prepareItem(array &$item)
    {
        if (isset($item['report_id'])) {
            $report = $this->reportFactory->create()->load($item['report_id']);
            $customerId = $report->getCustomerId();
            $customer = $item['user'];
            $customerUrl = $this->adminhtmlData->getUrl('customer/*/edit', ['id' => $customerId]);
            if (strpos($customer, 'Admin') === false) {
                if (strpos($customer, 'Guest') === false) {
                    $item[$this->getData('name')] = '<a href="'.$customerUrl.'" target="_blank">'.$customer.'</a>';
                }
            }
        }
        return $item;
    }
}
