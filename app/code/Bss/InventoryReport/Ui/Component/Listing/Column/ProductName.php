<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Backend\Helper\Data;

class ProductName extends \Bss\InventoryReport\Ui\Component\Listing\Column\AbstractColumn
{
    /**
     * @var Data
     */
    private $adminhtmlData;

    /**
     * ProductName constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Data $adminhtmlData
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Data $adminhtmlData,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->adminhtmlData = $adminhtmlData;
    }

    /**
     * @param array $item
     * @return string[]
     */
    protected function _prepareItem(array &$item)
    {
        if (isset($item['report_id'])) {
            $productId = $item['product_id'];
            $adminUrl = $this->adminhtmlData->getUrl('catalog/product/edit', ['id' => $productId]);
            $productName = $item['product_name'];
            $item[$this->getData('name')] = '<a href="'.$adminUrl.'" target="_blank">'.$productName.'</a>';
        }
        return $item;
    }
}
