<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Store\Model\StoreManagerInterface;
use Bss\InventoryReport\Model\ReportFactory;

class QtyChange extends \Bss\InventoryReport\Ui\Component\Listing\Column\AbstractColumn
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ReportFactory
     */
    private $reportFactory;

    /**
     * QtyChange constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     * @param ReportFactory $reportFactory
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = [],
        ReportFactory $reportFactory
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->storeManager = $storeManager;
        $this->reportFactory = $reportFactory;
    }

    /**
     * @param array $item
     * @return string[]
     */
    protected function _prepareItem(array &$item)
    {
        if (isset($item['report_id'])) {
            $reportid = $item['report_id'];
            $note = $item['note'];
            $report = $this->reportFactory->create()->load($reportid);
            if ($note == "Import Product") {
                $qtyBefore = $report->getData('qty_before');
                $qtyAfter = $report->getData('qty_after');
                $qtyChange = $qtyAfter - $qtyBefore;
                if ($qtyChange >0) {
                    $item[$this->getData('name')] = '+ '.$qtyChange;
                } else {
                    $qtyChange = abs($qtyChange);
                    $item[$this->getData('name')] = '- '.$qtyChange;
                }
            } else {
                $item[$this->getData('name')] = $item['qty_change'];
            }

        }
        return $item;
    }
}
