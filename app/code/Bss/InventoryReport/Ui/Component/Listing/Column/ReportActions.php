<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReport\Ui\Component\Listing\Column;

use Magento\Backend\Helper\Data;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Bss\InventoryReport\Model\ReportFactory;
use Magento\Framework\View\Element\UiComponentFactory;


class ReportActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var Data
     */
    private $adminhtmlData;
    /**
     * @var OrderInterface
     */
    private $orderIn;
    /**
     * @var ReportFactory
     */
    private $reportFactory;

    /**
     * ReportActions constructor.
     * @param Data $adminhtmlData
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     * @param OrderInterface $orderIn
     * @param ReportFactory $reportFactory
     */
    public function __construct(
        Data $adminhtmlData,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        OrderInterface $orderIn,
        ReportFactory $reportFactory
    ) {
        $this->adminhtmlData = $adminhtmlData;
        $this->orderIn = $orderIn;
        $this->reportFactory = $reportFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    /**
     * Prepare Data Source
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['report_id'])) {
                    $reportId = $item['report_id'];
                    $report = $this->getReport($reportId);
                    $orderCheck = $report->getCreateCheck();
                    $cancelCheck = $report->getCancelCheck();
                    $memoCheck = $report->getMemoCheck();
                    if ($orderCheck == '1' || $cancelCheck == '1') {
                        $orderIncrementId = $report->getIncrementId();
                        $orderEntity = $this->orderIn->loadByIncrementId($orderIncrementId)->getId();
                        $orderUrl = $this->adminhtmlData->getUrl('sales/order/view', ['order_id' => $orderEntity]);
                        $item[$this->getData('name')] = [
                            'view' => [
                                'href' => $orderUrl,
                                'label' => __('View Order')
                            ],
                        ];
                    } elseif ($memoCheck == '1') {
                        $memo = $report->getIncrementId();
                        $memoUrl = $this->adminhtmlData->getUrl('sales/creditmemo/view', ['creditmemo_id' => $memo]);
                        $item[$this->getData('name')] = [
                            'view' => [
                                'href' => $memoUrl,
                                'label' => __('View Memo')
                            ], 
                        ];
                    }
                }
            }
        }
        return $dataSource;
    }

    /**
     * Get Report
     * @param string $reportId
     * @return $this
     */
    public function getReport($reportId)
    {
        return $this->reportFactory->create()->load($reportId);
    }
}