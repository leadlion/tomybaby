<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReport
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReportCustomize\Plugin;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Request\Http;
use Magento\Backend\Model\Auth\Session;
use Magento\Catalog\Model\ProductFactory;

class AttributeSave
{
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;
    /**
     * @var \Bss\InventoryReport\Model\ResourceModel\Report
     */
    private $report;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var Http
     */
    private $request;
    /**
     * @var \Magento\Catalog\Helper\Product\Edit\Action\Attribute
     */
    private $attributeHelper;
    /**
     * @var Session
     */
    private $adminSession;
    /**
     * @var ProductFactory
     */
    private $productFactory;
    /**
     * @var \Bss\InventoryReport\Helper\ProductStock
     */
    private $productStockConfig;

    /**
     * AttributeSave constructor.
     * @param DateTime $date
     * @param StockRegistryInterface $stockRegistry
     * @param Report $report
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param Http $request
     * @param \Magento\Catalog\Helper\Product\Edit\Action\Attribute $attributeHelper
     * @param Session $adminSession
     * @param ProductFactory $productFactory
     * @param \Bss\InventoryReport\Helper\ProductStock $productStockConfig
     */
    public function __construct(
        DateTime $date,
        StockRegistryInterface $stockRegistry,
        Report $report,
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        Http $request,
        \Magento\Catalog\Helper\Product\Edit\Action\Attribute $attributeHelper,
        Session $adminSession,
        ProductFactory $productFactory,
        \Bss\InventoryReport\Helper\ProductStock $productStockConfig
    ) {
        $this->date = $date;
        $this->stockRegistry = $stockRegistry;
        $this->report = $report;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->request = $request;
        $this->attributeHelper = $attributeHelper;
        $this->adminSession = $adminSession;
        $this->productFactory = $productFactory;
        $this->productStockConfig = $productStockConfig;
    }

    /**
     * @param $subject
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundExecute(
        $subject,
        \Closure $proceed
    ) {
        $currentTime = $this->date->gmtDate();
        $inventoryData = $this->request->getParam('inventory', []);
        $admin = $this->adminSession->getUser()->getUserName();
        $admin = 'Admin/'.$admin;
        $backOrderValue = $this->productStockConfig->getBackOrderValue();
        if ($inventoryData && isset($inventoryData['qty'])) {
            foreach ($this->attributeHelper->getProductIds() as $productId) {
                try {
                    $product = $this->getProduct($productId);
                    $productType = $product->getTypeId();
                    if ($productType == "simple" || $productType == "downloadable" || $productType == "virtual") {
                        $productName = $product->getName();
                        $productSku = $product->getSku();
                        $stockItem = $this->stockRegistry->getStockItem($productId);
                        $stockQty = $stockItem->getQty();
                        $minQty = $stockItem->getMinQty();
                        $backOrderConfigProduct = $stockItem->getBackorders();
                        $useConfigBackOrder = $stockItem->getData('use_config_backorders');
                        $stockQtyAfter = $inventoryData['qty'];
                        $qtyChange = $stockQtyAfter - $stockQty;
                        $note = "Admin Manual Change By Product Manager";
                        if ($qtyChange > 0) {
                            $qtyChange = '+ '.$qtyChange;
                        } else {
                            $qtyChange = abs($qtyChange);
                            $qtyChange = '- '.$qtyChange;
                        }
                        $stockStatus = $stockItem->getIsInStock();
                        $stockStatus = $this->checkStatus($backOrderValue, $backOrderConfigProduct, $stockStatus,
                            $stockQtyAfter, $minQty, $useConfigBackOrder);
                        if ($qtyChange) {
                            $this->report->saveReport($currentTime, $productId, $productName,
                                $productSku, $stockQtyAfter, $qtyChange, $stockStatus,
                                $admin, $note, null, null, null, null, null, null, null);
                        }
                    }
                } catch (\Exception $e) {
                    $this->logger->debug($e->getMessage());
                    return $proceed($productSku, $stockItem);
                }
            }
        }
        return $proceed();
    }

    /**
     * @param string $backOrderValue
     * @param string $backOrderConfigProduct
     * @param string $stockStatus
     * @param string $stockQtyAfter
     * @param string $minQty
     * @param string $useConfigBackOrder
     * @return string
     */
    public function checkStatus(
        $backOrderValue,
        $backOrderConfigProduct,
        $stockStatus,
        $stockQtyAfter,
        $minQty,
        $useConfigBackOrder
    ) {
        $inventoryData = $this->request->getParam('inventory', []);
        if (isset($inventoryData['is_in_stock'])) {
            $stockStatus = "Out of Stock";
            if ($inventoryData['is_in_stock'] == 1) {
                $stockStatus = "In Stock";
            }
        } elseif ($backOrderValue || (!$useConfigBackOrder && $backOrderConfigProduct)) {
            if (!$stockStatus) {
                $stockStatus = "Out of Stock";
            } else {
                $stockStatus = "In Stock";
            }
        } elseif (!$stockStatus || !$stockQtyAfter) {
            $stockStatus = "Out of Stock";
        } else {
            $stockStatus = "In Stock";
            if ($stockQtyAfter <= $minQty) {
                $stockStatus = "Out of Stock";
            }
        }
        return $stockStatus;
    }

    /**
     * @param int $productId
     * @return $this
     */
    protected function getProduct($productId)
    {
        return $this->productFactory->create()->load($productId);
    }
}