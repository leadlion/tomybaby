<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_InventoryReportCustomize
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\InventoryReportCustomize\Plugin;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Bss\InventoryReport\Model\ResourceModel\Report;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Backend\Model\Auth\Session;

class StockRegistry
{
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var \Bss\InventoryReport\Model\ResourceModel\Report
     */
    protected $report;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;
    protected $resourceConnection;
    /**
     * @var Session
     */
    protected $adminSession;
    /**
     * StockRegistry constructor.
     * @param DateTime $date
     * @param Report $report
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        DateTime $date,
        Report $report,
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        Session $adminSession
    ) {
        $this->date = $date;
        $this->report = $report;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->registry = $registry;
        $this->resourceConnection = $resourceConnection;
        $this->adminSession = $adminSession;
    }

    /**
     * @param \Magento\CatalogInventory\Model\StockRegistry $subject
     * @param \Closure $proceed
     * @param string $productSku
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
     * @return mixed
     */
    public function aroundUpdateStockItemBySku(
        \Magento\CatalogInventory\Model\StockRegistry $subject,
        \Closure $proceed,
        $productSku,
        \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
    ) {
        try {
            if ($this->registry->registry('product_manager')) {
                $connection = $this->resourceConnection->getConnection();
                $tableName = $this->resourceConnection->getTableName('cataloginventory_stock_item');
                $currentTime = $this->date->gmtDate();
                $product = $this->productRepository->get($productSku);
                $productType = $product->getTypeId();
                if ($productType == "simple" || $productType == "downloadable" || $productType == "virtual") {
                    $productId = $product->getId();
                    $productName = $product->getName();
                    $sql = "SELECT qty FROM " . $tableName . " where product_id = " . $productId;
                    $stockQty = $connection->fetchAll($sql)[0]['qty'];
                    $stockQtyAfter = $stockItem->getQty();
                    $qtyChange = (int)$stockQtyAfter - (int)$stockQty;
                    $stockStatus = $stockItem->getIsInStock();
                    $user = $this->adminSession->getUser()->getUserName();
                    $user = 'Admin/'.$user;
                    
                    $stockStatus == 1 ? $stockStatus = "In Stock" : $stockStatus = "Out Stock";

                    $note = "Admin Manual Change By Product Manager";
                    if ($qtyChange) {
                        if ($qtyChange > 0) {
                            $qtyChange = '+ '. $qtyChange;
                        } else {
                            $qtyChange = abs($qtyChange);
                            $qtyChange = '- '. $qtyChange;
                        }
                        $this->report->saveReport($currentTime, $productId, $productName,
                            $productSku, $stockQtyAfter, $qtyChange, $stockStatus,
                            $user, $note, null, null, null, null, null, null, null);
                    }
                }
            }
            return $proceed($productSku, $stockItem);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->debug($e->getMessage());
            return $proceed($productSku, $stockItem);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
            return $proceed($productSku, $stockItem);
        }
    }
}