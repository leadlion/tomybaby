<?php

namespace Bss\MirasvitLayeredNavigation\Plugin;

use Magento\CatalogSearch\Model\Adapter\Mysql\Filter\Preprocessor;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Search\Request\FilterInterface;

class MultiselectCategoryPriceFilterQuery extends \Mirasvit\LayeredNavigation\Plugin\MultiselectCategoryPriceFilterQuery
{
    private $customerSession;
    
    private $resource;
    
    private $connection;
    
    public function __construct(
        CustomerSession $customerSession,
        ResourceConnection $resource
    ) {
        parent::__construct(
            $customerSession,
            $resource
        );
        $this->customerSession = $customerSession;
        $this->resource        = $resource;
        $this->connection      = $resource->getConnection();
    }
    
    public function aroundProcess(
        Preprocessor $subject,
        \Closure $proceed,
        FilterInterface $filter,
        $isNegation,
        $query
    ) {
        if ($filter->getField() === 'category_ids'
            && is_array($filter->getValue())
            && isset($filter->getValue()['in'])) {
            return $this->getCategoryQuery($filter->getValue());
        }
            
        if ($filter->getField() === 'price'
            && ((strpos($filter->getFrom(), ',') !== false)
                || (strpos($filter->getTo(), ',') !== false))
        ) {
            return $this->getPriceQuery($filter->getFrom(), $filter->getTo());
        }
        
        return $proceed($filter, $isNegation, $query);
    }
    
    private function getCategoryQuery($filterValue)
    {
        return 'category_ids_index.category_id IN (' . implode(',', $filterValue['in']) . ')';
    }
    
    /**
     * @param string $filterFrom
     * @param string $filterTo
     *
     * @return string
     */
    private function getPriceQuery($filterFrom, $filterTo)
    {
        $select = [];
        $from   = explode(',', $filterFrom);
        $to     = explode(',', $filterTo);
        $from   = $this->prepareFromFilter($from);
        
        foreach ($from as $key => $value) {
            $toPrepared = (isset($to[$key]) && $to[$key]) ? ' AND price_index.min_price <= ' . $to[$key] : '';
            $select[]   = '(price_index.min_price >= ' . $value . $toPrepared . ')';
        }
        
        $resultQuery = '(';
        
        $resultQuery .= implode(' OR ', $select);
        
        $resultQuery .= sprintf(
            ') AND %s = %s',
            $this->connection->quoteIdentifier('price_index.customer_group_id'),
            $this->customerSession->getCustomerGroupId()
            );
        
        return $resultQuery;
    }
    
    /**
     * @param array $from
     *
     * @return array
     */
    private function prepareFromFilter($from)
    {
        foreach ($from as $key => $value) {
            if ($value == '') {
                $from[$key] = 0;
            }
        }
        
        return $from;
    }
}
