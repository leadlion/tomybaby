<?php

namespace Bss\MirasvitSearchAutocomplete\Index\Magento\Catalog;

use Magento\Framework\App\ObjectManager;

class Product extends \Mirasvit\SearchAutocomplete\Index\Magento\Catalog\Product
{
    private $productBlock;
    
    private $cart;
    
    public function mapProduct(\Magento\Catalog\Model\Product $product, $storeId = 1)
    {
        $item = parent::mapProduct($product, $storeId);
        
        $item['cart'] = $this->getCart($product);
        return $item;
    }
    
    private function getCart(\Magento\Catalog\Model\Product $product)
    {
        if ($this->productBlock === null) {
            $this->productBlock = ObjectManager::getInstance()
            ->create('Magento\Catalog\Block\Product\ListProduct');
        }
        
        $cart = [
            'visible' => $this->config->isShowCartButton(),
            'label'   => __('Add to Cart')
        ];
        $cart['params'] = $this->productBlock->getAddToCartPostParams($product);
        
        return $cart;
    }
}
