<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Bss\RwIksanikaProductmanage\Block\Adminhtml\Product;

use Magento\Store\Model\Store;

class Grid extends \Iksanika\Productmanage\Block\Adminhtml\Product\Grid
{  
    private function prepareDefaults() 
    {
        $this->setDefaultLimit($this->_scopeConfig->getValue('iksanika_productmanage/columns/limit'));
        $this->setDefaultPage($this->_scopeConfig->getValue('iksanika_productmanage/columns/page'));
        $this->setDefaultSort($this->_scopeConfig->getValue('iksanika_productmanage/columns/sort'));
        $this->setDefaultDir($this->_scopeConfig->getValue('iksanika_productmanage/columns/dir'));
    }
    
    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
//        $collection = $this->getCollection();
//        $collection = !$collection ? $this->_productModel->getCollection() : $collection;
        
        $collection = $this->_productFactory->create()->getCollection();
        
        if($queryString = $this->getQuery())
        {
            $query = $this->queryFactory->get();
            $query->setStoreId($this->_helper->getStoreId());
            $collection = $query->getSearchCollection();
            $collection->addSearchFilter($this->getQuery());
            $collection->addBackendSearchFilter($this->getQuery());
            $collection->addAttributeToSelect('*');
        }
        
        $store = $this->_getStore();
//        $collection = $this->_productFactory->create()->getCollection()->addAttributeToSelect(
        $collection->addAttributeToSelect(
            'sku'
        )->addAttributeToSelect(
            'name'
        )->addAttributeToSelect(
            'attribute_set_id'
        )->addAttributeToSelect(
            'type_id'
        )->setStore(
            $store
        );
        
        if ($this->moduleManager->isEnabled('Magento_CatalogInventory')) {
            $collection->joinField(
                'qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
            $collection->joinField(
                'is_in_stock',
                'cataloginventory_stock_item',
                'is_in_stock',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
        }
        
        if($this->_helper->colIsVisible('category_ids')) {
            $collection->joinField(
                    'cat_ids',
                    'catalog_category_product',
                    'category_id',
                    'product_id=entity_id',
                    null,
                    'left');
        }
        if($this->_helper->colIsVisible('category')) {
            $collection->joinField(
                'category',
                'catalog_category_product',
                'category_id',
                'product_id=entity_id',
                null,
                'left');
        }
        if($this->_helper->colIsVisible('related_ids')) {
            $collection->joinField(
                'related_ids',
                'catalog_product_link',
                'linked_product_id',
                'product_id=entity_id',
                '{{table}}.link_type_id='.\Magento\Catalog\Model\Product\Link::LINK_TYPE_RELATED, // 1- relation, 4 - up_sell, 5 - cross_sell
                'left');
        }
        if($this->_helper->colIsVisible('cross_sell_ids')) {
            $collection->joinField(
                'cross_sell_ids',
                'catalog_product_link',
                'linked_product_id',
                'product_id=entity_id',
                '{{table}}.link_type_id='.\Magento\Catalog\Model\Product\Link::LINK_TYPE_CROSSSELL, // 1- relation, 4 - up_sell, 5 - cross_sell
                'left');
        }
        if($this->_helper->colIsVisible('up_sell_ids')) {
            $collection->joinField(
                'up_sell_ids',
                'catalog_product_link',
                'linked_product_id',
                'product_id=entity_id',
                '{{table}}.link_type_id='.\Magento\Catalog\Model\Product\Link::LINK_TYPE_UPSELL, // 1- relation, 4 - up_sell, 5 - cross_sell
                'left');
        }
                /*
            ->joinField(
                'associated_groupped_ids',
                'catalog/product_link',
                'linked_product_id',
                'product_id=entity_id',
                '{{table}}.link_type_id='.Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED, // 1- relation, 4 - up_sell, 5 - cross_sell
                'left')
                 */
        if($this->_helper->colIsVisible('associated_configurable_ids')) {
            $collection->joinField(
                'associated_configurable_ids',
                'catalog_product_super_link',
                'product_id',
                'parent_id=entity_id',
                null, // 1- relation, 4 - up_sell, 5 - cross_sell
                'left');
        }
        if($this->_helper->colIsVisible('tier_price')) {
            $collection->joinField(
                'tier_price',
                'catalog_product_entity_tier_price', //Mage::getConfig()->getTablePrefix().
                'value', 
                'entity_id=entity_id',
                null,//'{{table}}.website_id='.$store->getId(),
                'left');
        }             
        
        $collection->groupByAttribute('entity_id');
        
        if ($store->getId()) {
            //$collection->setStoreId($store->getId());
            $collection->addStoreFilter($store);
            $collection->joinAttribute(
                'name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                Store::DEFAULT_STORE_ID
            );
            $collection->joinAttribute(
                'custom_name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'status',
                'catalog_product/status',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'visibility',
                'catalog_product/visibility',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute('price', 'catalog_product/price', 'entity_id', null, 'left', $store->getId());
        } else {
            $collection->addAttributeToSelect('price');
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        }
        
        
        
        // EG: Select all needed columns.
        //id,name,type,attribute_set,sku,price,qty,visibility,status,websites,image
//        foreach(self::$columnSettings as $col => $true) 
        foreach($this->_helper->getColumnSettings() as $col => $true) 
        {
            if($col == 'category_ids')
            {
                //$filter = $this->getParam('filter');
//                echo $this->getVarNameFilter().'~';
                $filter = $this->getParam($this->getVarNameFilter());
                if($filter)
                {
                    $filter_data = $this->_backendHelper->prepareFilterString($filter);
                    if(isset($filter_data['category_ids']))
                    {
                        if(trim($filter_data['category_ids'])=='')
                            continue;
                        $categoryIds = explode(',', $filter_data['category_ids']);
                        $catIdsArray = array();
                        foreach($categoryIds as $categoryId)
                        {
                            //$collection->addCategoryFilter(Mage::getModel('catalog/category')->load($categoryId));
                            $catIdsArray[] = $categoryId;
                        }
                        $collection->addAttributeToFilter('cat_ids', array( 'in' => $catIdsArray));                        
                        //$collection->printLogQuery(true);
                    }
                }
            }
            if($col == 'related_ids' || $col == 'cross_sell_ids' || $col == 'up_sell_ids' || 
                    $col == 'associated_groupped_ids' || $col == 'associated_configurable_ids')
            { 
                $filter = $this->getParam($this->getVarNameFilter());
                if($filter)
                {
                    $filter_data = $this->_backendHelper->prepareFilterString($filter);
                    if(isset($filter_data[$col]))
                    {
                        if(trim($filter_data[$col])=='')
                            continue;
                        $relatedIds = explode(',', $filter_data[$col]);
                        $relatedIdsArray = array();
                        foreach($relatedIds as $relatedId)
                        {
                            //$collection->addCategoryFilter(Mage::getModel('catalog/category')->load($categoryId));
                            $relatedIdsArray[] = intval($relatedId);
                        }
                        $collection->addAttributeToFilter($col, array( 'in' => $relatedIdsArray));                        
                    }
                }
            }
            /*
            if($col == 'sku')
            {
                $filter = $this->getParam($this->getVarNameFilter());
                if($filter)
                {
                    $filter_data = Mage::helper('adminhtml')->prepareFilterString($filter);
                    if(isset($filter_data['sku']))
                    {
                        if(trim($filter_data['sku'])=='')
                            continue;
                        $skuIds = explode(',', $filter_data['sku']);
                        $skuIdsArray = array();
                        foreach($skuIds as $skuId)
                            $skuIdsArray[] = $skuId;
                        $collection->addAttributeToFilter('sku', array( 'inset' => $skuIdsArray));                        
                    }
                }
            }
           */
            if($col == 'qty' || $col == 'websites' || $col=='id' || $col=='category_ids' || $col=='related_ids' || 
                    $col=='cross_sell_ids' || $col=='up_sell_ids' || $col=='associated_groupped_ids' || 
                    $col=='associated_configurable_ids' || $col=='group_price') 
                continue;
            else
                $collection->addAttributeToSelect($col);
        }
        
        $collection->addWebsiteNamesToResult();
        $this->setCollection($collection);
        //die(get_class($this->getCollection()));
        //$collection->printLogQuery(true);
        \Magento\Backend\Block\Widget\Grid\Extended::_prepareCollection();
        //$this->getCollection()->addWebsiteNamesToResult();
        //$collection->printLogQuery(true);
        return $this;
    }
}
