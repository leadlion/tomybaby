<?php

namespace Bss\RwIksanikaProductmanage\Plugin\Helper;

class Data
{
    /**
     * @var \Magento\Backend\Block\Store\Switcher
     */
	protected $switcher;

    /**
     * Data constructor.
     * @param \Magento\Backend\Block\Store\Switcher $switcher
     */
	public function __construct(
        \Magento\Backend\Block\Store\Switcher $switcher
    ) {
        $this->switcher = $switcher;
    }

    /**
     * @param \Iksanika\Productmanage\Helper\Data $subject
     * @param $result
     * @return mixed
     */
	public function afterGetStoreId(\Iksanika\Productmanage\Helper\Data $subject, $result)
	{
		return $this->switcher->getStoreId();
	}

}
