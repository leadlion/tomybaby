<?php

namespace Bss\RwMirasvitSeoMarkup\Plugin\Rs;

use Magento\Store\Model\ScopeInterface;

class Product
{
	protected $scopeConfig;
 
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->scopeConfig = $scopeConfig;
    }

	public function afterGetJsonData(\Mirasvit\SeoMarkup\Block\Rs\Product $subject, $result)
	{
		$active = $this->scopeConfig->getValue('seo/seo_markup/product/enable_for_product', ScopeInterface::SCOPE_STORE);
		if ($active != 1) {
            return false;
        }

        return $result;
	}
}
