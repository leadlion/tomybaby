<?php
namespace Bss\RwOwlCarouselSlider\Block\Slider;

class Products extends \WeltPixel\OwlCarouselSlider\Block\Slider\Products
{

    public function getTemplate(){
        return 'WeltPixel_OwlCarouselSlider::sliders/products.phtml';
    }
    /**
     * Get new slider products.
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $_collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getNewProductCollection($_collection)
    {
        $limit  = $this->_getProductLimit('new_products');
        $random = $this->_getRandomSort('new_products');

        if (!$limit || $limit == 0) {
            return [];
        }

        $_collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());

        if ($random) {
            $allIds = $_collection->getAllIds();
            $randomIds = [];
            $maxKey = count($allIds) - 1;
            while (count($randomIds) <= count($allIds) - 1) {
                $randomKey = mt_rand(0, $maxKey);
                $randomIds[$randomKey] = $allIds[$randomKey];
            }

            $_collection->addIdFilter($randomIds);
        };

        $_collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());

        $_collection = $this->_addProductAttributesAndPrices($_collection)
            ->addAttributeToFilter(
                'news_from_date',
                ['date' => true, 'to' => $this->getEndOfDayDate()],
                'left')
            ->addAttributeToFilter(
                'news_to_date',
                [
                    'or' => [
                        0 => ['date' => true, 'from' => $this->getStartOfDayDate()],
                        1 => ['is' => new \Zend_Db_Expr('null')],
                    ]
                ],
                'left')
            ->addStoreFilter($this->getStoreId())->setCurPage(1);
        $_collection->setOrder('news_from_date', 'DESC');
        if ($limit && $limit > 0 ) {
            $_collection->setPageSize($limit);
        };
        return $_collection;
    }
}