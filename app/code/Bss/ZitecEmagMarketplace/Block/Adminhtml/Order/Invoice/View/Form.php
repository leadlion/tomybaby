<?php

namespace Bss\ZitecEmagMarketplace\Block\Adminhtml\Order\Invoice\View;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Sales\Helper\Admin;
use Bss\ZitecEmagMarketplace\Helper\Data as EmagData;

class Form extends \Magento\Sales\Block\Adminhtml\Order\Invoice\View\Form
{
	private $emagData;

	public function __construct(
        Context $context,
        Registry $registry,
        Admin $adminHelper,
        EmagData $emagData,
        array $data = []
    ) {
        parent::__construct(
        	$context,
        	$registry,
        	$adminHelper,
        	$data
        );
        $this->emagData = $emagData;
    }

	public function getTemplate()
	{
		return 'Bss_ZitecEmagMarketplace::order/invoice/view/form.phtml';
	}

	public function getEmagInformation()
	{
	    $order = $this->getOrder();
	    $customerData = $this->emagData->getEmagByMagento($order);
		return $customerData;
	}
}
