<?php

namespace Bss\ZitecEmagMarketplace\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Zitec\EmagMarketplace\Api\Data\OrderQueueItemInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Json\Helper\Data as JsonData;
use Zitec\EmagMarketplace\Model\ResourceModel\Queue\Order\Item\CollectionFactory;
use Bss\CheckoutCustomField\Helper\Data as CheckoutCustomFieldData;

class Data extends AbstractHelper
{
	private $jsonHelper;

	private $collectionFactory;
	
	private $customFieldHelper;

	public function __construct(
        Context $context,
        JsonData $jsonHelper,
        CollectionFactory $collectionFactory,
	    CheckoutCustomFieldData $customFieldHelper
    ) {
        parent::__construct(
            $context
        );
        $this->jsonHelper = $jsonHelper;
        $this->collectionFactory = $collectionFactory;
        $this->customFieldHelper = $customFieldHelper;
    }

    public function getEmagByMagento($order)
	{
	    $orderIncrement = $order->getIncrementId();
		$item = $this->collectionFactory->create()
			->addFieldToFilter(OrderQueueItemInterface::MAGENTO_ID, $orderIncrement)->getFirstItem();
		$customerData = [];
		if ($item && !empty($item->getData())) {
			if (isset($item['message'])) {
				$message = $this->jsonHelper->jsonDecode($item['message']);
				if (isset($message['results']) && isset($message['results'][0]) && isset($message['results'][0]['customer'])) {
					$customerData = $message['results'][0]['customer'];
				}
			}
		}
		
		if (empty($customerData) && $order->getBssCustomfield() && $this->customFieldHelper->moduleEnabled()) {
		    if ($this->customFieldHelper->hasDataCustomFieldOrder($order->getBssCustomfield())) {
		        $bssCustomfield = $this->jsonHelper->jsonDecode($order->getBssCustomfield());

		        foreach ($bssCustomfield as $key => $field) {
		            if ($field['show_in_order']) {
		                $customerData[$key] = $field['value'];
                    }
                }
		    }
		}
		return $customerData;
	}
}
