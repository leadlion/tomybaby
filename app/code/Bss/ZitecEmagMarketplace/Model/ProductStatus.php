<?php

namespace Bss\ZitecEmagMarketplace\Model;

use Zitec\EmagMarketplace\ApiWrapper\Requests\ProductOffer\CountRequest;
use Zitec\EmagMarketplace\Exception\ApiResponseErrorException;
use Zitec\EmagMarketplace\Model\ProductAttributes;

class ProductStatus extends \Zitec\EmagMarketplace\Model\ProductStatus
{
	public function importProductStatus()
    {
    	if (!$this->registry->registry('IMPORTING_STATUS')) {
        	$this->registry->register('IMPORTING_STATUS',1);
        }
        
        // Read offers
        $this->fetchData();

        // Save imported data
        $this->saveData();
    }

    public function saveData()
    {
        if (!$this->products) {
            return;
        }

        $productIds = array_column($this->products, 'id');
        $this->products = array_column($this->products, null, 'id');

        $collection = $this->collectionFactory->create();
        if (is_array($productIds) && empty($productIds)) {
            $productIds = null;
        }
        $collection->addFieldToFilter('entity_id', ['in' => $productIds]);
        $collection->addFieldToFilter('emkp_sent_to_emkp', ['eq' => 1]);
        $collection->addFieldToFilter('emkp_visible', ['eq' => 1]);

        if (!$collection->getSize()) {
            return;
        }

        foreach ($collection as $prod) {
            $prod->setData(
                ProductAttributes::STATUS,
                $this->products[$prod->getId()]['status'] ? 'active' : 'inactive'
            );

            $validationStatus = reset($this->products[$prod->getId()]['validation_status']);
            $validationStatusMessage = $validationStatus['description'];
            if ($validationStatus['errors']) {
                $validationStatusMessage .= PHP_EOL . __(' Errors: ');
                if (is_array($validationStatus['errors'])) {
                    foreach ($validationStatus['errors'] as $status) {
                        if (array_key_exists('message', $status)) {
                            $validationStatusMessage .= PHP_EOL . __('Message: %1', $status['message']);
                        }
                        
                        if (array_key_exists('user_message', $status)) {
                            foreach ($status['user_message'] as $userMessage) {
                                if (array_key_exists('error_description', $userMessage)) {
                                    $validationStatusMessage .= PHP_EOL . __('Error Description: %1',
                                            $userMessage['error_description']);
                                }

                                if (array_key_exists('user_message', $userMessage) && $userMessage['user_message']) {
                                    $validationStatusMessage .= PHP_EOL . __('User Message: %1',
                                            $userMessage['user_message']);
                                }

                                if (array_key_exists('reasons', $userMessage) && is_array($userMessage['reasons'])) {
                                    $validationStatusMessage .= PHP_EOL . __('Reasons: ');
                                    foreach ($userMessage['reasons'] as $reason) {
                                        if ( !empty($reason['seller_text']) && !empty($reason['seller_text']['en_GB']) ) {
                                            $validationStatusMessage .= PHP_EOL . ' - ' . $reason['seller_text']['en_GB'];
                                        }
                                        if ( !empty($reason['user_message']) && !empty($reason['user_message']['en_GB']) ) {
                                            $validationStatusMessage .= PHP_EOL . ' - ' . $reason['user_message']['en_GB'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $validationStatusMessage .= 'Errors: ' . print_r($validationStatus['errors'], true);
                }
            }

            $prod->setData(
                ProductAttributes::VALIDATION_STATUS,
                $validationStatusMessage
            );

            $offerValidationStatus = $this->products[$prod->getId()]['offer_validation_status'];

            $offerValidationStatusMessage = $offerValidationStatus['description'];
            if ($offerValidationStatus['errors']) {
                $offerValidationStatusMessage .= print_r($offerValidationStatus['errors'], true);
            }
            $prod->setData(
                ProductAttributes::OFFER_VALIDATION_STATUS,
                $offerValidationStatusMessage
            );
            if (isset($this->products[$prod->getId()]['part_number_key'])) {
                $partNumber = $this->products[$prod->getId()]['part_number_key'];
                $prod->setData(
                    ProductAttributes::PART_NUMBER_KEY,
                    $partNumber
                );
            }
        }

        $collection->save();
    }

    public function countData(): array
    {
        $request = new CountRequest([
            'itemsPerPage' => 100,
        ]);
        $this->apiClient->setArrayResponse(true);

        $response = $this->apiClient->sendRequest($request);

        if (!isset($response['isError'], $response['results']) || $response['isError']) {
            if (!isset($response['results'])) {
                throw new ApiResponseErrorException(__('Error occured during eMAG data import.'));
            } else {
                throw new ApiResponseErrorException(implode(', ', $response['messages']));
            }
        }

        return $response;
    }
}
