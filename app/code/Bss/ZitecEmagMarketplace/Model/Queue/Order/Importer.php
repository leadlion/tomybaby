<?php

namespace Bss\ZitecEmagMarketplace\Model\Queue\Order;

use Zitec\EmagMarketplace\Model\ApiClient;
use Zitec\EmagMarketplace\Api\QueueOrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Zitec\EmagMarketplace\Model\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Model\Quote\AddressFactory;
use Psr\Log\LoggerInterface;
use Zitec\EmagMarketplace\Model\AlertManager;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Registry;
use Magento\Quote\Model\Quote\ItemFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Tax\Api\TaxCalculationInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Model\Order;
use Magento\Quote\Model\Quote;

class Importer extends \Zitec\EmagMarketplace\Model\Queue\Order\Importer
{
    public function __construct(
        ApiClient $apiClient,
        QueueOrderRepositoryInterface $repository,
        OrderInterface $order,
        Config $config,
        StoreManagerInterface $storeManager,
        CartRepositoryInterface $cartRepositoryInterface,
        CartManagementInterface $cartManagementInterface,
        AddressFactory $addressFactory,
        LoggerInterface $logger,
        AlertManager $alertManager,
        ProductFactory $productFactory,
        Registry $registry,
        ItemFactory $itemFactory,
        ScopeConfigInterface $scopeConfig,
        TaxCalculationInterface $taxCalculation,
        PriceCurrencyInterface $priceCurrency
    ) {
        parent::__construct(
            $apiClient,
            $repository,
            $order,
            $config,
            $storeManager,
            $cartRepositoryInterface,
            $cartManagementInterface,
            $addressFactory,
            $logger,
            $alertManager,
            $productFactory,
            $registry,
            $itemFactory,
            $scopeConfig,
            $taxCalculation
        );
        $this->priceCurrency = $priceCurrency;
    }
    
    protected function forceShippingAmount(Order $order, array $data)
    {
        // Get eMag shipping tax
        $shippingPrice = $data['shipping_tax'];
        
        if ($shippingPrice != 0) {
            $shippingPriceExclTax = $shippingPrice / 1.19;
        } else {
            $shippingPriceExclTax = 0;
        }
        
        // Force eMag shipping tax value on order
        $originalShippingAmount = $order->getShippingInclTax() != 0
        ? $order->getShippingInclTax()
        : $order->getBaseShippingInclTax();
        
        $order->setShippingAmount($shippingPriceExclTax);
        $order->setShippingInclTax($shippingPrice);
        $order->setBaseShippingAmount($shippingPriceExclTax);
        $order->setBaseInclShippingAmount($shippingPrice);
        
        // Fix tax
        $shippingTaxAmount = $this->priceCurrency->round($shippingPrice - $shippingPriceExclTax);
        $order->setShippingTaxAmount($shippingTaxAmount);
        $order->setBaseShippingTaxAmount($shippingTaxAmount);
        $taxAmount = $shippingTaxAmount;
        if (!empty($data['products'])) {
            foreach($data['products'] as $item) {
                $taxAmount += $this->priceCurrency->round(
                    $this->priceCurrency->round($item['sale_price']) * $item['vat'] * $item['quantity']
                );
            }
        }
        $order->setTaxAmount($taxAmount);
        $order->setBaseTaxAmount($taxAmount);
        
        // Recalculate total
        $order->setGrandTotal($order->getGrandTotal() - $originalShippingAmount + $shippingPrice);
        $order->setBaseGrandTotal(
            $order->getBaseGrandTotal() - $originalShippingAmount + $shippingPrice
            );
    }
    
    protected function setProductsToQuote(array $data, Quote $quote)
    {
        $errorMessages = null;
        //add items in quote
        foreach ($data['products'] as $item) {
            if ($item['status'] == 0) {
                continue;
            }
            
            $product = $this->productFactory->create()->load($item['product_id']);
            
            if (!$product->getId()) {
                $errorMessages[] =
                __('Error importing order from eMAG Marketplace. eMAG order id: ') . $data['id'] .
                PHP_EOL .
                __('Product id not found. Id: ') . $item['product_id'];
            } else {
                
                $itemPrice = $this->priceCurrency->round(
                    $this->getTaxCalculatedPrice(
                        $this->priceCurrency->round($item['sale_price']),
                        (float)$item['vat'],
                        $product
                    )
                );
                
                $product->setPrice($itemPrice);
                
                if ($product->getSpecialPrice()) {
                    $product->setSpecialPrice($itemPrice);
                }
                
                $newItem = $this->itemFactory->create();
                $newItem->setProduct($product);
                $newItem->setQty((float)$item['quantity']);
                
                $newItem->setCustomPrice($itemPrice);
                $newItem->setOriginalCustomPrice($itemPrice);
                $newItem->getProduct()->setIsSuperMode(true);
                
                $quote->addItem($newItem);
                
            }
        }
        
        if ($errorMessages) {
            throw new \Exception(implode(PHP_EOL, $errorMessages));
        }
    }
}
