<?php

namespace Bss\ZitecEmagMarketplace\Model\Queue\Product;

use Magento\Catalog\Model\Product;
use Magento\Store\Model\ScopeInterface;
use Zitec\EmagMarketplace\Api\Data\CategoryInterface;
use Zitec\EmagMarketplace\Model\ProductAttributes;

use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Zitec\EmagMarketplace\Exception\MissingProductDataException;

class Handler extends \Zitec\EmagMarketplace\Model\Queue\Product\Handler
{
    /**
     * @param Product $product
     * @return array
     * @throws MissingProductDataException
     * @throws \Exception
     */
    protected function getProductData(Product $product): array
    {
        $status = (int)($product->getStatus() == Status::STATUS_ENABLED && $product->isInStock() && $product->isSalable());

        $productData = [
            'id' => $product->getId(),
            'status' => $status,
            'name' => $product->getName(),
            'sale_price' => $this->getProductPrice($product, 'sale'),
            'vat_id' => $this->config->getVatRate(),
            'handling_time' => [
                [
                    'warehouse_id' => 1,
                    'value' => 0,
                ],
            ],
            'stock' => [
                [
                    'warehouse_id' => 1,
                    'value' => $this->stockState->getStockQty($product->getId()),
                ],
            ],
        ];

        $mappedCategory = $this->mappingManager->getProductMappedCategory($product);
        if ($status === Status::STATUS_ENABLED && !$mappedCategory) {
            throw new MissingProductDataException(__('None of this product\'s categories are mapped to an eMag category.'));
        }

        if ($mappedCategory->isEanMandatory() && !$product->getData(ProductAttributes::EAN)) {
            throw new MissingProductDataException(__('The EAN is required for the category that this product is mapped to.'));
        }

        $additionalData = $this->getAdditionalData($product, $mappedCategory);

        $completeData = array_merge($productData, $additionalData);

        if ($product->getData(ProductAttributes::IS_SENT) && $sentData = $product->getData(ProductAttributes::SENT_DATA)) {
            $sentData = json_decode($sentData, true);
            $this->filterProductData($productData, $additionalData, $sentData);
        } else {
            $productData = array_merge($productData, $additionalData);
        }

        $productData['min_sale_price'] = $completeData['min_sale_price'];
        $productData['max_sale_price'] = $completeData['max_sale_price'];
        $productData['recommended_price'] = $completeData['recommended_price'];

        return [
            'completeData' => $completeData,
            'updatedData' => $productData,
        ];
    }

    public function getPriceExclTax(Product $product, string $priceType)
    {
        if ($taxAttribute = $product->getCustomAttribute('tax_class_id')) {
            // First get base price (=price excluding tax)
            $productRateId = $taxAttribute->getValue();
            
            $rate = $this->taxCalculation->getCalculatedRate($productRateId);
            
            if ($priceType == 'sale') {
                $productPrice = $product->getFinalPrice();
                $emkpPrice = (float) $product->getEmkpPrice();
                if ($emkpPrice) {
                    $productPrice = $emkpPrice;
                }
            } else {
                $emkpPrice = (float) $product->getEmkpPrice();
                if ($emkpPrice) {
                    $productPrice = $emkpPrice * 1.1;
                }else {
                    $productPrice = $product->getPrice();
                }
            }
            
            if ((int)$this->scopeConfig->getValue(
                'tax/calculation/price_includes_tax',
                ScopeInterface::SCOPE_STORE) === 1
                ) {
                    // Product price in catalog is including tax.
                    $priceExcludingTax = $productPrice / (1 + ($rate / 100));
                } else {
                    // Product price in catalog is excluding tax.
                    $priceExcludingTax = $productPrice;
                }
                
                return $priceExcludingTax;
        }
    }

    /**
     * @param Product $product
     * @param CategoryInterface $mappedCategory
     * @return array
     * @throws \Exception
     */
    protected function getAdditionalData(Product $product, CategoryInterface $mappedCategory): array
    {
        $data = [
            'part_number' => $product->getSku(),
            'url' => $product->getUrlInStore(),
            'recommended_price' => $this->getProductPrice($product, 'regular'),
            'min_sale_price' => $this->getProductPrice($product, 'min'),
            'max_sale_price' => $this->getProductPrice($product, 'max'),
            'warranty' => $product->getData(ProductAttributes::WARRANTY) ?: 0,
            'name' => $product->getName(),
            'images' => [],
            'category_id' => $mappedCategory->getEmagId(),
        ];

        // if ($pnk = $product->getData(ProductAttributes::PART_NUMBER_KEY)) {
        //     $data['part_number_key'] = $pnk;
        // }

        $data['description'] = $this->templateProcessor->filter($product->getDescription());
        $data['brand'] = $product->getData(ProductAttributes::BRAND);

        $ean = $product->getData(ProductAttributes::EAN);
        $data['ean'] = $ean ? array_map('trim', explode(',', $ean)) : [];

        $data['characteristics'] = $this->getProductCharacteristics($product);

        $images = $product->getMediaGalleryImages();

        foreach ($images as $image) {
            $data['images'][] = [
                'display_type' => 1,
                'url' => $image->getUrl(),
            ];
        }

        return $data;
    }
}
