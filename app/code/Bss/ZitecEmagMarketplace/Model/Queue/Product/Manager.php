<?php

namespace Bss\ZitecEmagMarketplace\Model\Queue\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\Message\ManagerInterface;
use Zitec\EmagMarketplace\Api\QueueProductRepositoryInterface;
use Zitec\EmagMarketplace\Exception\MissingProductDataException;
use Zitec\EmagMarketplace\Model\MappingManager;
use Zitec\EmagMarketplace\Model\ProductAttributes;
use Magento\CatalogInventory\Api\StockStateInterface;

class Manager extends \Zitec\EmagMarketplace\Model\Queue\Product\Manager
{
    /**
     * @var StockStateInterface
     */
    protected $stockState;

    /**
     * Manager constructor.
     * @param MappingManager $mappingManager
     * @param QueueProductRepositoryInterface $queueProductRepository
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        MappingManager $mappingManager,
        QueueProductRepositoryInterface $queueProductRepository,
        ManagerInterface $messageManager,
        StockStateInterface $stockState
    ) {
        parent::__construct($mappingManager, $queueProductRepository, $messageManager);
        $this->stockState = $stockState;
    }

    /**
     * @param Product $product
     * @return bool
     * @throws MissingProductDataException
     */
    protected function validateProduct(Product $product): bool
    {
        $visibleToEmag = $product->getData(ProductAttributes::IS_VISIBLE);
        $brand = $product->getData(ProductAttributes::BRAND);
        $ean = $product->getData(ProductAttributes::EAN);

        if (!$visibleToEmag) {
            return false;
        }

        try {
            if (!$brand) {
                throw new MissingProductDataException(__('The eMag brand is required for products that are visible in eMag Marketplace.'));
            }

            if (!($category = $this->mappingManager->getProductMappedCategory($product))) {
                throw new MissingProductDataException(__('None of this product\'s categories are mapped to an eMag category.'));
            }

            if (!$ean && $category->isEanMandatory()) {
                throw new MissingProductDataException(__('The EAN is required for the category that this product is mapped to.'));
            }

            $qty = $this->stockState->getStockQty($product->getId());

            if (!is_numeric($qty)) {
                throw new MissingProductDataException(__('The quantity is mandatory for eMag Marketplace products.'));
            }

            if (!$product->getMediaGalleryImages() || !$product->getMediaGalleryImages()->getSize()) {
                throw new MissingProductDataException(__('At least one product image is mandatory for eMag Marketplace products.'));
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
            $this->messageManager->addWarningMessage(__('The product can not be published in eMAG Marketplace until the error is resolved.'));
            $this->messageManager->addWarningMessage(__('The product has not been set as "Visible in eMAG Marketplace".'));
            throw new \Exception(__('Please correct the issue above, set "Visible in eMAG Marketplace" to "Yes" and save the product again in order to publish this product in eMAG Marketplace.'));
        }

        return true;
    }
}
