<?php

namespace Bss\ZitecEmagMarketplace\Model;

use Zitec\EmagMarketplace\ApiWrapper\Requests\ProductOffer\CreateRequest;
use Zitec\EmagMarketplace\Exception\ApiResponseErrorException;

class UpdateMarketplace extends \Bss\ZitecEmagMarketplace\Model\Queue\Product\Handler
{
    public function updateProductPrice($product)
    {
        try {
            $productData = $this->getProductData($product);

            $request = new CreateRequest([$productData['updatedData']]);
            $response = $this->apiClient->sendRequest($request);

            if (!isset($response->isError) || $response->isError) {
                throw new ApiResponseErrorException(implode(';', $response->messages ?? []));
            }
        } catch (\Throwable $exception) {
            $this->logger->critical($exception);
        }

    }
}
