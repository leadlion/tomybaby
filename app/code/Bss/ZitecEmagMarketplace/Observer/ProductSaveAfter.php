<?php

namespace Bss\ZitecEmagMarketplace\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Bss\ZitecEmagMarketplace\Model\UpdateMarketplace;

class ProductSaveAfter implements ObserverInterface
{
    private $updateMarketplace;

    private $logger;

    public function __construct(
        UpdateMarketplace $updateMarketplace,
        LoggerInterface $logger
    ) {
        $this->updateMarketplace = $updateMarketplace;
        $this->logger = $logger;
    }

    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $oldEmkpPrice = (float) $product->getOrigData('emkp_price');
        $newEmkpPrice = (float) $product->getEmkpPrice();
        try {
            if ($newEmkpPrice != $oldEmkpPrice) {
                $this->updateMarketplace->updateProductPrice($product);
            }
        } catch (\Throwable $exception) {
            $this->logger->critical($exception);
        }
    }
}
