<?php

namespace Bss\ZitecEmagMarketplace\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;

class ProductSaveBefore implements ObserverInterface
{
    private $logger;

    private $request;

    public function __construct(
        LoggerInterface $logger,
        RequestInterface $request
    ) {
        $this->logger = $logger;
        $this->request = $request;
    }

    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();

        if(!$product) {
            return;
        }

        $oldManufacturer = $product->getOrigData('manufacturer');
        $manufacturer = $product->getData('manufacturer');

        $oldCodEan = $product->getOrigData('cod_ean');
        $codEan = $product->getData('cod_ean');

        try {
            if ($manufacturer != $oldManufacturer) {
                $product->setEmkpBrand($this->getAttrLabel($product, 'manufacturer'));
            }
            if ($codEan != $oldCodEan) {
                $product->setEmkpEan($codEan);
            }
        } catch (\Exception $exception) {
            $this->logger->critical($exception);
        }
    }

    private function getAttrLabel($product, $attrCode)
    {
        return $product->getResource()->getAttribute($attrCode)->getFrontend()->getValue($product);
    }
}
