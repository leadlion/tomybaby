<?php

namespace Bss\ZitecEmagMarketplace\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class ProductUpdateAttributeBefore implements ObserverInterface
{
    private $logger;

    protected $eavConfig;

    public function __construct(
        LoggerInterface $logger,
        \Magento\Eav\Model\Config $eavConfig
    ) {
        $this->logger = $logger;
        $this->eavConfig = $eavConfig;

    }

    public function execute(EventObserver $observer)
    {
        try {
            $attributes_data = $observer->getEvent()->getAttributesData();
            if(isset($attributes_data['cod_ean'])) {
                $attributes_data['emkp_ean'] = $attributes_data['cod_ean'];
            };
            if(isset($attributes_data['manufacturer'])) {
                $attributes_data['emkp_brand'] = $this->getAttributeOptionLabel('manufacturer', $attributes_data['manufacturer']);
            };
            $observer->getEvent()->setAttributesData($attributes_data);
        } catch (\Exception $exception) {
            $this->logger->critical($exception);
        }
        
        return true;
    }

    private function getAttributeOptionLabel($attributeCode, $optionValue){
        $attribute = $this->eavConfig->getAttribute('catalog_product', $attributeCode);
        $options = $attribute->getSource()->getAllOptions();
        $key = array_search($optionValue, array_column($options, 'value'));
        return $options[$key]['label']; 
    }
}
