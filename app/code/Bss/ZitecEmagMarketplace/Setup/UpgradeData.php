<?php

namespace Bss\ZitecEmagMarketplace\Setup;

use Magento\Catalog\Model\Product;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
	const EMKP_PRICE = 'emkp_price';

	private $eavSetupFactory;

	public function __construct(
		EavSetupFactory $eavSetupFactory
	) {
		$this->eavSetupFactory = $eavSetupFactory;
	}

    public function upgrade(
    	ModuleDataSetupInterface $setup,
    	ModuleContextInterface $context
    ) {
    	if ($context->getVersion()
            && version_compare($context->getVersion(), '1.0.2') < 0
        ) {
        	$setup->startSetup();
    		$productSetup = $this->eavSetupFactory->create(['setup' => $setup]);
    		$attribute = [
	            'type' => 'decimal',
	            'label' => 'Price',
	            'input' => 'price',
	            'backend' => \Magento\Catalog\Model\Product\Attribute\Backend\Price::class,
	            'sort_order' => 99,
	            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
	            'visible' => true,
	            'required' => false,
	            'user_defined' => false,
	            'searchable' => false,
	            'filterable' => true,
	            'visible_on_front' => false,
	            'used_in_product_listing' => false,
	            'class' => '',
	            'source' => '',
	            'group' => 'eMag Marketplace',
	            'apply_to' => 'simple'
	        ];
    		$productSetup->removeAttribute(Product::ENTITY, self::EMKP_PRICE);
            $productSetup->addAttribute(
            	Product::ENTITY,
            	self::EMKP_PRICE,
            	$attribute
            );
            $setup->endSetup();
        }
    }
}
