<?php

namespace Bss\ZitecEmagMarketplace\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    	$setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
			$setup->getConnection()->changeColumn(
				$setup->getTable('zitec_emkp_vat_rate'),
				'vat_rate',
				'vat_rate',
				[
					'type' => \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
					'length' => '10,2',
					'nullable' => false,
					'comment' => 'Is default Vat Rate'
				]
			);
        }

        $setup->endSetup();
    }
}
