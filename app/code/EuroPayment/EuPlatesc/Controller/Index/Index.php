<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace EuroPayment\EuPlatesc\Controller\Index;

/**
 * Responsible for loading page content.
 *
 * This is a basic controller that only loads the corresponding layout file. It may duplicate other such
 * controllers, and thus it is considered tech debt. This code duplication will be resolved in future releases.
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /** @var \Magento\Framework\View\Result\PageFactory  */
    protected $resultPageFactory;
	protected $request;
	protected $formKey;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Data\Form\FormKey $formKey,
		\Magento\Framework\App\Request\Http $request
    ) {
        $this->request = $request;
		$this->formKey = $formKey;
		$this->request->setParam('form_key', $this->formKey->getFormKey());
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    /**
     * Load the page defined in view/frontend/layout/samplenewpage_index_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {   //load model
        /* @var $paymentMethod \Magento\Authorizenet\Model\DirectPost */
        $paymentMethod = $this->_objectManager->create('EuroPayment\EuPlatesc\Model\EpPay');

        //get request data
        $data = $this->request->getPostValue();

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/eppay.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(json_encode($data));

        $paymentMethod->process($data);
        //return $this->resultPageFactory->create();
    }

}
