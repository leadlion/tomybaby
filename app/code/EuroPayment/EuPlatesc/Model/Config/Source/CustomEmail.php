<?php

namespace EuroPayment\EuPlatesc\Model\Config\Source;
 
class CustomEmail implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
 
        return [
            ['value' => 0, 'label' => __('No email')],
            ['value' => 1, 'label' => __('Payment Success')],
            ['value' => 2, 'label' => __('Payment Fail')],
            ['value' => 3, 'label' => __('Payment Success/Fail')],
        ];
    }
}
?>