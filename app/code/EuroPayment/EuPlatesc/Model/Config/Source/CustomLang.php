<?php

namespace EuroPayment\EuPlatesc\Model\Config\Source;
 
class CustomLang implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
 
        return [
            ['value' => 0, 'label' => __('Auto')],
            ['value' => 1, 'label' => __('RO')],
            ['value' => 2, 'label' => __('EN')],
            ['value' => 3, 'label' => __('FR')],
            ['value' => 4, 'label' => __('HU')],
            ['value' => 5, 'label' => __('IT')],
            ['value' => 6, 'label' => __('ES')],
            ['value' => 7, 'label' => __('DE')],
        ];
    }
}
?>