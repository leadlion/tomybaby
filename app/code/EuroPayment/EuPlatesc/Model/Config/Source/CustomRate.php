<?php

namespace EuroPayment\EuPlatesc\Model\Config\Source;

class CustomRate implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
         return array(
			array('value'=>'apb', 'label'=>'Alpha Bank'),
			array('value'=>'bcr', 'label'=>'Banca Comerciala Romana'),
			array('value'=>'btrl', 'label'=>'Banca Transilvania'),            
			array('value'=>'brdf', 'label'=>'BRD Finance'),         
			array('value'=>'rzb', 'label'=>'Raiffeisen Bank')                     
		);
    }
}