<?php

namespace EuroPayment\EuPlatesc\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;

class ConfigProvider implements ConfigProviderInterface
{
    /**
     * @var string[]
     */
    protected $rate = ['apb'=>'Alpha Bank','bcr'=>'Banca Comerciala Romana','btrl'=>'Banca Transilvania','brdf'=>'BRD Finance','rzb'=>'Raiffeisen Bank'];
    /**
     * @var Escaper
     */
    protected $escaper;
    protected $euplatesc;
    /**
     * @param Escaper $escaper
     */
    public function __construct(PaymentHelper $paymentHelper,Escaper $escaper) {
        $this->escaper = $escaper;
		$this->euplatesc=$paymentHelper->getMethodInstance("eppay");
    }
	
    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [];
		$config['payment']['euplatesc']['ratestatus']=$this->euplatesc->getRateStatus();
		if($this->euplatesc->getRateStatus()){
			
			$config['payment']['euplatesc']['ratedisplay']=$this->euplatesc->rateDisplay();
			
			$rateactive=explode(",",$this->euplatesc->getRate());
			
			$i=0;
			foreach ($rateactive as $code) {
				$config['payment']['euplatesc']['rate'][$i]['code']=$code;
				$config['payment']['euplatesc']['rate'][$i]['name']=$this->rate[$code];
				$config['payment']['euplatesc']['rate'][$i]['nr']=explode(",",$this->euplatesc->getNrRate($code));
				$i++;
			}
			
			
		}
		
        return $config;
    }
}