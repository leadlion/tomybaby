<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace EuroPayment\EuPlatesc\Model;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;



/**
 * Pay In Store payment method model
 */
class EpPay extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'eppay';
    protected $_isOffline = true;
	
    protected $_isInitializeNeeded = true;
   
    //protected $_formBlockType = 'EuroPayment\EuPlatesc\Block\Form\Euplatesc';

    protected $_gateUrl = "https://secure.euplatesc.ro/tdsprocess/tranzactd.php";

    protected $_orderSender;
    protected $_storeManager;
    protected $orderFactory;
	protected $connection;

	public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Sales\Model\OrderFactory $orderFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []){
		$this->_orderSender = $orderSender;
		$this->_storeManager = $storeManager;
        $this->orderFactory = $orderFactory;
		
        parent::__construct($context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data);
    }
	
	public function getAmount($orderId)
    {   
        return $this->getOrder($orderId)->getGrandTotal();
    }
	
	public function getCurrency()
    {   
        return $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
    }
	
	public function getCustomerName($orderId)
    {   
        return $this->getOrder($orderId)->getCustomerName();
    }
	
	public function getCustomerIsGuest($orderId)
    {   
        return $this->getOrder($orderId)->getCustomerIsGuest();
    }
	
	public function getCustomerEmail($orderId)
    {   
        return $this->getOrder($orderId)->getCustomerEmail();
    }	
	
	public function getPaymentMethod($orderId)
    {   
        return $this->getOrder($orderId)->getPayment()->getMethod()==$this->_code;
    }
	
	public function getAditionalData($orderId){
		 return $this->getOrder($orderId)->getPayment()->getAdditionalInformation();
	}

    protected function getOrder($orderId)
    {
        $orderFactory=$this->orderFactory;
        return $orderFactory->create()->loadByIncrementId($orderId);
    }

 
    public function initialize($paymentAction, $stateObject)
    {
        $state = $this->getConfigData('order_status');
        $this->_gateUrl=$this->getConfigData('cgi_url');
        $stateObject->setState($state);
        $stateObject->setStatus($state);
        $stateObject->setIsNotified(false);
    }
	

    public function getGateUrl(){
        return $this->_gateUrl;
    }
	
	public function getRedirect(){
        return $this->getConfigData('redirect');
    }
	
	public function getRateStatus(){
        if($this->getConfigData('ractiv')=="1")
			return true;
		return false;
    }
	
	public function getRate(){
        if($this->getConfigData('ractiv')=="1"){
			return $this->getConfigData('rate');
		}
		return "";
    }	
	
	public function getNrRate($code){
        return $this->getConfigData('rate_'.$code);
    }
	
	public function rateDisplay(){
        if($this->getConfigData('rateshow')=="0")
			return false;
		return true;
    }
	
	public function payText(){
        $my_lang=$this->getConfigData('lang');
		$mlang="Plateste";
		switch($my_lang){
			case 0: $mlang="Plateste";break;
			case 1: $mlang="Plateste";break;
			case 2: $mlang="Pay";break;
			case 3: $mlang="Paie";break;
			case 4: $mlang="Fizet";break;
			case 5: $mlang="Paga";break;
			case 6: $mlang="Pague";break;
			case 7: $mlang="Bezahlen";break;
		}
		return $mlang;
    }
	
	private function hmacsha1($key,$data) {
		$blocksize = 64;
		$hashfunc  = 'md5';
		if(strlen($key) > $blocksize)
			$key = pack('H*', $hashfunc($key));
		$key  = str_pad($key, $blocksize, chr(0x00));
		$ipad = str_repeat(chr(0x36), $blocksize);
		$opad = str_repeat(chr(0x5c), $blocksize);
		$hmac = pack('H*', $hashfunc(($key ^ $opad) . pack('H*', $hashfunc(($key ^ $ipad) . $data))));
		return bin2hex($hmac);
	}

	private function euplatesc_mac($data, $key){
		$str = NULL;

		foreach($data as $d){
			if($d === NULL || strlen($d) == 0)
				$str .= '-';
			else
				$str .= strlen($d) . $d;
		}
		$key = pack('H*', $key);
		return $this->hmacsha1($key, $str);
	}
	
	public function getPostData($orderId)
    {   
		$mid=$this->getConfigData('merch_id');
		if(strpos($_SERVER['SERVER_NAME'],"tomybaby")!==false){
			$mid="44840978477";
		}
		
		$dataAll = array(
			'amount'      => $this->getAmount($orderId),
			'curr'        => $this->getCurrency(),
			'invoice_id'  => $orderId,
			'order_desc'  => "Plata online ".$_SERVER['SERVER_NAME'],
			'merch_id'    => $mid,
			'timestamp'   => gmdate("YmdHis"),
 			'nonce'       => md5(microtime() . mt_rand()),
		); 
  
		$dataAll['fp_hash'] = strtoupper($this->euplatesc_mac($dataAll,$this->getConfigData('merch_key')));
		if($this->getCustomerIsGuest($orderId)==false){
			$dataAll['fname'] = preg_split('/ /',$this->getCustomerName($orderId))[0];
			$dataAll['lname'] = preg_split('/ /',$this->getCustomerName($orderId),2)[1];
		}
		$dataAll['email'] = $this->getCustomerEmail($orderId);
		if(strlen($this->getAditionalData($orderId)['bankop'])>3){
			$dataAll['ExtraData[rate]'] = $this->getAditionalData($orderId)['bankop'];
		}
		
		$my_lang=$this->getConfigData('lang');
		if($my_lang>0){
			$mlang="ro";
			switch($my_lang){
				case 1: $mlang="ro";break;
				case 2: $mlang="en";break;
				case 3: $mlang="fr";break;
				case 4: $mlang="hu";break;
				case 5: $mlang="it";break;
				case 6: $mlang="es";break;
				case 7: $mlang="de";break;
			}
			$dataAll['lang']=$mlang;
		}
 
        return $dataAll;
    }

	//ipn check start
	
	protected function getConnection()
    {
        if (!$this->connection) {
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
            $this->connection = $connection;
        }
        return $this->connection;
    }
	
	public function ep_insert_epid($ep_id,$invoice_id)
    {
        $this->getConnection()->query("Insert INTO euplatesc_ipn VALUES (null,'".$ep_id."','".$invoice_id."')");
    }
	
	public function ep_select_epid($cart_id)
    {
        $sku = $this->getConnection()->fetchOne('SELECT invoice_id FROM euplatesc_ipn WHERE ep_id = "'. $cart_id .'"');
        return $sku;
    }
	
    public function process($responseData){

		if(isset($responseData['sec_status']) and isset($responseData['invoice_id'])) {
			$zcrsp =  array (
				'amount'     => addslashes(trim(@$responseData['amount'])),
				'curr'       => addslashes(trim(@$responseData['curr'])), 
				'invoice_id' => addslashes(trim(@$responseData['invoice_id'])),
				'ep_id'      => addslashes(trim(@$responseData['ep_id'])),
				'merch_id'   => addslashes(trim(@$responseData['merch_id'])),
				'action'     => addslashes(trim(@$responseData['action'])),
				'message'    => addslashes(trim(@$responseData['message'])),
				'approval'   => addslashes(trim(@$responseData['approval'])),
				'timestamp'  => addslashes(trim(@$responseData['timestamp'])),
				'nonce'      => addslashes(trim(@$responseData['nonce'])),
				'sec_status' => addslashes(trim(@$responseData['sec_status'])),
			);
						 
			$zcrsp['fp_hash'] = strtoupper($this->euplatesc_mac($zcrsp, $this->getConfigData('merch_key')));

			$fp_hash=addslashes(trim(@$responseData['fp_hash']));
			if($zcrsp['fp_hash']===$fp_hash)  {

				$id = $responseData['invoice_id'];
				$order = $this->getOrder($id);
				if ($order) {
					echo $this->_processOrder2($order,$responseData);
				}else{
					echo "err inv";
				}

			} else {
				echo "Invalid signature";
			}
		} 
		else if(isset($responseData['sec_status']) and isset($responseData['cart_id'])){
							
			$id = $this->ep_select_epid($responseData['cart_id']);
			$order = $this->getOrder($id);
			if ($order) {
				echo $this->_processOrder3($order,$responseData);
			}else{
				echo "err inv";
			}
							
		}
		
		else if(isset($responseData['invoice_id'])) {
			$zcrsp =  array (
				'amount'     => addslashes(trim(@$responseData['amount'])),
				'curr'       => addslashes(trim(@$responseData['curr'])),
				'invoice_id' => addslashes(trim(@$responseData['invoice_id'])),
				'ep_id'      => addslashes(trim(@$responseData['ep_id'])),
				'merch_id'   => addslashes(trim(@$responseData['merch_id'])),
				'action'     => addslashes(trim(@$responseData['action'])),
				'message'    => addslashes(trim(@$responseData['message'])),
				'approval'   => addslashes(trim(@$responseData['approval'])),
				'timestamp'  => addslashes(trim(@$responseData['timestamp'])),
				'nonce'      => addslashes(trim(@$responseData['nonce'])),
			);
				 
			$zcrsp['fp_hash'] = strtoupper($this->euplatesc_mac($zcrsp, $this->getConfigData('merch_key')));

			$fp_hash=addslashes(trim(@$responseData['fp_hash']));
			if($zcrsp['fp_hash']==$fp_hash) {
				
				$id = $responseData['invoice_id'];
				
				$order = $this->getOrder($id);

				if ($order) {
					echo $this->_processOrder($order,$responseData);
				}else{
					echo "err inv";
				}
					
			}else {
				echo "Invalid signature";
			}
		} else {
			echo"e0";
		}  
         
    }

    protected function _processOrder(\Magento\Sales\Model\Order $order , $response)
    {

        if ($response['action']==0) {
			echo "Success";
						
			$payment = $order->getPayment();
			if (true === isset($payment) && false === empty($payment)) {
				$payment->registerCaptureNotification($response['amount']);
				$order->setPayment($payment);
				$order->addStatusToHistory(Order::STATE_PROCESSING,
						sprintf('[INFO] IPN updated order state to "%s"', Order::STATE_PROCESSING));
				$order->save();
				
				//if($this->getConfigData('emails_send')==1 || $this->getConfigData('emails_send')==3)
				if($this->getConfigData('emails_send')%2==1)
					$this->_orderSender->send($order);
			}
			
        }else{
			
			echo "Fail";
			$order->setStatus(Order::STATE_CANCELED);
			$order->addStatusToHistory(
				Order::STATE_CANCELED,
				sprintf('[INFO] IPN updated order state to "%s"', Order::STATE_CANCELED)
			);
			$order->save();
			
			//if($this->getConfigData('emails_send')==2 || $this->getConfigData('emails_send')==3)
			if($this->getConfigData('emails_send')>=2)
				$this->_orderSender->send($order);
			
		}
          
    }

  
	protected function _processOrder2(\Magento\Sales\Model\Order $order , $response)
    {
		
		if($response['action']=="0") {

			if($response['sec_status']==8 or $response['sec_status']==9){
				$payment = $order->getPayment();
				if (true === isset($payment) && false === empty($payment)) {
					$payment->registerCaptureNotification($response['amount']);
					$order->setPayment($payment);
					$order->addStatusToHistory(Order::STATE_PROCESSING,
							sprintf('[INFO] IPN updated order state to "%s"', Order::STATE_PROCESSING));
					$order->save();
					
					//if($this->getConfigData('emails_send')==1 || $this->getConfigData('emails_send')==3)
					if($this->getConfigData('emails_send')%2==1)
						$this->_orderSender->send($order);
				}
			}else{
				$this->ep_insert_epid($response['ep_id'],$response['invoice_id']);
			}			
							
		} else {
			$order->setStatus(Order::STATE_CANCELED);
			$order->addStatusToHistory(
				Order::STATE_CANCELED,
				sprintf('[INFO] IPN updated order state to "%s"', Order::STATE_CANCELED)
			);
			$order->save();
			
			//if($this->getConfigData('emails_send')==2 || $this->getConfigData('emails_send')==3)
			if($this->getConfigData('emails_send')>=2)
				$this->_orderSender->send($order);
		}
		
          
    }	
	
	
	protected function _processOrder3(\Magento\Sales\Model\Order $order , $response)
    {
		
		if($responseData['sec_status']==8 or $responseData['sec_status']==9){
				echo "Successfully completed";
				$payment = $order->getPayment();
				if (true === isset($payment) && false === empty($payment)) {
					$payment->registerCaptureNotification($response['amount']);
					$order->setPayment($payment);
					$order->addStatusToHistory(Order::STATE_PROCESSING,
							sprintf('[INFO] IPN updated order state to "%s"', Order::STATE_PROCESSING));
					$order->save();
					
					//if($this->getConfigData('emails_send')==1 || $this->getConfigData('emails_send')==3)
					if($this->getConfigData('emails_send')%2==1)
						$this->_orderSender->send($order);
				}
		}if($responseData['sec_status']==5 or $responseData['sec_status']==6){
			echo "Tranzaction failed";
			$order->setStatus(Order::STATE_CANCELED);
			$order->addStatusToHistory(
				Order::STATE_CANCELED,
				sprintf('[INFO] IPN updated order state to "%s"', Order::STATE_CANCELED)
			);
			$order->save();
			
			//if($this->getConfigData('emails_send')==2 || $this->getConfigData('emails_send')==3)
			if($this->getConfigData('emails_send')>=2)
				$this->_orderSender->send($order);
		}
		         
    }

  

}
