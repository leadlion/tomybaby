<?php
namespace EuroPayment\EuPlatesc\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\DataObject;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;

class DataAssignObserver extends AbstractDataAssignObserver
{
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
		$data = $this->readDataArgument($observer);
		$additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }
		
		$additionalData = new DataObject($additionalData);
        $paymentMethod = $this->readMethodArgument($observer);
		$paymentInfo = $paymentMethod->getInfoInstance();
		
		
        if ($additionalData->getData('bankop') !== null) {
            $paymentInfo->setAdditionalInformation(
                'bankop',
                $additionalData->getData('bankop')
            );
        }
		
    }
}