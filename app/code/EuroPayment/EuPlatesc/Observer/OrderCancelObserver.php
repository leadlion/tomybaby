<?php
namespace EuroPayment\EuPlatesc\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;

class OrderCancelObserver extends AbstractDataAssignObserver
{
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
	   $order = $observer->getEvent()->getOrder();
       $paymentMethod = $order->getPayment()->getMethod();
       if ($paymentMethod == 'eppay') {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/eppay.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('------event-------');
            $logger->info('Order: '. $order->getId());
            $e = new \Exception();
            $logger->info($e->getTraceAsString());
            $logger->info('----------------');
       }
    }
}