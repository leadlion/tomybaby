/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'eppay',
                component: 'EuroPayment_EuPlatesc/js/view/payment/method-renderer/eppay-method'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({
            getData: function() {
				return {
					'method': this.item.method,
					'additional_data': {
						'bankop': $('input[name=ep_ext_op]:checked').val()
					}
				};
			}
        });
    }
);