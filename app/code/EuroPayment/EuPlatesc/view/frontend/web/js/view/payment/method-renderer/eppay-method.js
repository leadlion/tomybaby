/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'Magento_Checkout/js/view/payment/default',
		'jquery'
    ],
    function (Component,$) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'EuroPayment_EuPlatesc/payment/eppay'
            },
			getRateStatus: function() {
                 return window.checkoutConfig.payment.euplatesc['ratestatus'];
            },
			getRateShow: function() {
				return window.checkoutConfig.payment.euplatesc['ratedisplay'];
			},
			getRateData: function() {
                return window.checkoutConfig.payment.euplatesc['rate'];
            },
			getRateExtTxt: function() {
				var rate=window.checkoutConfig.payment.euplatesc['rate'];
				var ret=[],k=0;
				for(var i=0;i<rate.length;i++){
					for(var j=0;j<rate[i]['nr'].length;j++){
						ret[k]=[];
						ret[k]['code']=rate[i]['code']+"-"+rate[i]['nr'][j];
						ret[k]['text']=rate[i]['nr'][j]+" rate "+rate[i]['name'];
						k++;
					}
				}
				return ret;
			},
			getData: function() {
				var inrate=$('input[name=euplatesc_pty]:checked').val();
				var rateop="";
				if(inrate=="1"){
					if(this.getRateShow()){
						//compact
						rateop=$("#euplatesc_sb").val()+"-"+$("#euplatesc_sr").val();
					}else{
						//extins
						rateop=$('input[name=ep_ext_op]:checked').val();
						if(rateop==undefined){
							rateop="";
						}
					}
				}
				return {
					'method': this.item.method,
					'additional_data': {
						'bankop': rateop
					}
				};
			},
			euplatesc_handle_radio: function(){
				if($("#euplatesc_ptyi").is(":checked")){
					$("#euplatesc_rcontainer").css("display","none");
				}else{
					$("#euplatesc_rcontainer").css("display","block");
					this.euplatesc_handle_select();
				}
			},
			euplatesc_handle_select: function(){
				var rate=window.checkoutConfig.payment.euplatesc['rate'];
				
				if($("#euplatesc_sb").html()==""){
					var bancihtml="";
					for(var i=0;i<rate.length;i++){
						bancihtml+="<option value='"+rate[i]['code']+"'>"+rate[i]['name']+"</option>";
					}
					$("#euplatesc_sb").html(bancihtml);
				}
				
				if($("#euplatesc_sb").val()!=null && $("#euplatesc_sb").val()!=undefined){
					var bop=$("#euplatesc_sb").val();
					
					var nrratehtml="";
					for(var i=0;i<rate.length;i++){
						if(rate[i]['code']==bop){
							for(var j=0;j<rate[i]['nr'].length;j++){
								nrratehtml+="<option value='"+rate[i]['nr'][j]+"'>"+rate[i]['nr'][j]+" rate</option>";
							}
						}
					}
					
					$("#euplatesc_sr").html(nrratehtml);
					
				}
			}
        });
    }
);
