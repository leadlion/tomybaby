<?php
namespace Iksanika\Orderattach\Block\Account\Order;

class Attachments extends \Magento\Framework\View\Element\Template
{
    protected $_template = 'account/order/attachments.phtml';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Iksanika\Orderattach\Helper\Attachment
     */
    public $_helper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Iksanika\Orderattach\Helper\Attachment $helper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Iksanika\Orderattach\Helper\Attachment $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $registry;
        $this->_helper = $helper;
    }

    public function getOrder()
    {
        return $this->coreRegistry->registry('current_order');
    }

    public function getAttachmentConfig()
    {
        return $this->_helper->getAttachmentConfig($this);
    }

    public function getOrderAttachments()
    {
        return $this->_helper->getOrderAttachments($this->getOrder()->getId());
    }

    public function isEnabledFileUpload()
    {
        return $this->_helper->isEnabledFileUpload();
    }

    public function getUploadUrl()
    {
        return $this->getUrl('orderattachment/attachment/upload', ['order_id' => $this->getOrder()->getId()]);
    }

    public function getUpdateUrl()
    {
        return $this->getUrl('orderattachment/attachment/update', ['order_id' => $this->getOrder()->getId()]);
    }

    public function getRemoveUrl()
    {
        return $this->getUrl('orderattachment/attachment/delete', ['order_id' => $this->getOrder()->getId()]);
    }
}
