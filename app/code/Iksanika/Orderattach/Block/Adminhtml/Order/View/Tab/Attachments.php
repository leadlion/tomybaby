<?php
namespace Iksanika\Orderattach\Block\Adminhtml\Order\View\Tab;

use Magento\Sales\Block\Adminhtml\Order\AbstractOrder;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Attachments extends AbstractOrder implements TabInterface
{
    /**
     * @var \Iksanika\Orderattach\Helper\Attachment
     */
    protected $_helper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param \Iksanika\Orderattach\Helper\Attachment $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Iksanika\Orderattach\Helper\Attachment $helper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $adminHelper, $data);
        $this->_helper = $helper;
    }

    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    public function getAttachmentConfig()
    {
        $config = $this->_helper->getAttachmentConfig($this);

        return $config;
    }

    public function getOrderAttachments()
    {
        $orderId = $this->getOrder()->getId();

        return $this->_helper->getOrderAttachments($orderId);
    }

    public function getUploadUrl()
    {
        return $this->getUrl(
            'orderattachment/attachment/upload',
            ['order_id' => $this->getOrder()->getId()]
        );
    }

    public function getUpdateUrl()
    {
        return $this->getUrl(
            'orderattachment/attachment/update',
            ['order_id' => $this->getOrder()->getId()]
        );
    }

    public function getRemoveUrl()
    {
        return $this->getUrl(
            'orderattachment/attachment/delete',
            ['order_id' => $this->getOrder()->getId()]
        );
    }

    public function getTabLabel()
    {
        return __('Order Attachments');
    }

    public function getTabTitle()
    {
        return __('Order Attachments');
    }

    public function canShowTab()
    {
        return $this->_helper->isAttachmentEnabled();
    }

    public function isHidden()
    {
        return false;
    }
}
