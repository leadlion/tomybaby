<?php
namespace Iksanika\Orderattach\Block\Email;

class Attachments extends \Magento\Framework\View\Element\Template
{
    protected $_template = 'email/order/attachments.phtml';

    /**
     * @var \Iksanika\Orderattach\Helper\Attachment
     */
    protected $_helper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Iksanika\Orderattach\Helper\Attachment $_helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Iksanika\Orderattach\Helper\Attachment $helper,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->_helper = $helper;
    }

    public function getOrderId()
    {
        $order = $this->getData('order');
        return $order ? $order->getId() : false;
    }

    public function getOrderAttachments()
    {
        $order = $this->getData('order');
        if(!$order)
        {
            return [];
        }

        return $this->_helper->getOrderAttachments($order->getQuoteId(), false);
    }
}
