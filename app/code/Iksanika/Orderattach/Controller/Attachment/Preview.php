<?php
namespace Iksanika\Orderattach\Controller\Attachment;

class Preview extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Iksanika\Orderattach\Helper\Attachment
     */
    protected $attachmentHelper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Iksanika\Orderattach\Helper\Attachment $attachmentHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Iksanika\Orderattach\Helper\Attachment $attachmentHelper
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;
        $this->attachmentHelper = $attachmentHelper;
    }

    public function execute()
    {
        $response = $this->resultRawFactory->create();
        $result = $this->attachmentHelper
            ->previewAttachment($this->getRequest(), $response);

        return $result;
    }
}
