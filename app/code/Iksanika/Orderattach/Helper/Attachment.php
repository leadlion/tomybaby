<?php
namespace Iksanika\Orderattach\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\UrlInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class Attachment extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Iksanika\Orderattach\Model\Upload
     */
    protected $uploadModel;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $random;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var \Iksanika\Orderattach\Model\AttachmentFactory
     */
    protected $attachmentFactory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Iksanika\Orderattach\Model\Upload $uploadModel
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Math\Random $random
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Iksanika\Orderattach\Model\AttachmentFactory $attachmentFactory
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Iksanika\Orderattach\Model\Upload $uploadModel,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Math\Random $random,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Iksanika\Orderattach\Model\AttachmentFactory $attachmentFactory,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Escaper $escaper,
        UrlInterface $urlBuilder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->_customerSession = $customerSession;
        $this->uploadModel = $uploadModel;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->random = $random;
        $this->dateTime = $dateTime;
        $this->attachmentFactory = $attachmentFactory;
        $this->fileSystem = $fileSystem;
        $this->escaper = $escaper;
        $this->jsonEncoder = $jsonEncoder;
        $this->urlBuilder = $urlBuilder;

        $this->_logger = $logger;
    }

    /**
     * Upload file and save attachment
     * @param \Magento\Framework\App\Request\Http $request
     * @return array
     */
    public function saveAttachment($request)
    {
        try {
            $uploadData = $request->getFiles()->get('order-attachment')[0];
            $result = $this->uploadModel->uploadFileAndGetInfo($uploadData);

            unset($result['tmp_name']);
            unset($result['path']);
            $result['success'] = true;
            $result['url'] = $this->storeManager->getStore()
                ->getBaseUrl() . "var/orderattachment/" . $result['file'];

            $hash = $this->random->getRandomString(32);
            $date = $this->dateTime->gmtDate('Y-m-d H:i:s');

            $attachment = $this->attachmentFactory
                ->create()
                ->setPath($result['file'])
                ->setHash($hash)
                ->setComment('')
                ->setType($result['type'])
                ->setUploadedAt($date)
                ->setModifiedAt($date);

            if ($orderId = $request->getParam('order_id')) {
                $attachment->setOrderId($orderId);
            } else {
                $quote = $this->checkoutSession->getQuote();
                $attachment->setQuoteId($quote->getId());
            }

            $attachment->save();

            $defaultStore = $this->storeManager
                ->getStore(
                    $this->storeManager->getDefaultStoreView()->getId()
                );

            $preview = $defaultStore->getUrl(
                'orderattachment/attachment/preview',
                [
                    'attachment' => $attachment->getId(),
                    'hash' => $attachment->getHash()
                ]
            );
            $download = $defaultStore->getUrl(
                'orderattachment/attachment/preview',
                [
                    'attachment' => $attachment->getId(),
                    'hash' => $attachment->getHash(),
                    'download' => 1
                ]
            );
            $result['preview'] = $preview;
            $result['download'] = $download;
            $result['attachment_id'] = $attachment->getId();
            $result['hash'] = $attachment->getHash();
            $result['comment'] = '';
        } catch (\Exception $e) {
            $result = [
                'success' => false,
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode()
            ];
        }

        return $result;
    }

    /**
     * Delete order attachment
     * @param \Magento\Framework\App\Request\Http $request
     * @return array
     */
    public function deleteAttachment($request)
    {
        $result = [];
        $isAjax = $request->isAjax();
        $isPost = $request->isPost();
        $requestParams = $request->getParams();
        $attachmentId = $requestParams['attachment'];
        $hash = $requestParams['hash'];
        $orderId = isset($requestParams['order_id']) ? $requestParams['order_id'] : null;

        if (!$isAjax || !$isPost || !$attachmentId || !$hash) {
            return ['success' => false, 'error' => __('Invalid Request Params')];
        }

        try {
            $attachment = $this->attachmentFactory->create()->load($attachmentId);

            if (!$attachment->getId() || ($orderId && $orderId !== $attachment->getOrderId())) {
                return ['success' => false, 'error' => __('Can\'t find a attachment to delete.')];
            }

            if ($hash !== $attachment->getHash()) {
                return ['success' => false, 'error' => __('Invalid Hash Params')];
            }

            $varDirectory = $this->fileSystem
                ->getDirectoryRead(DirectoryList::VAR_DIR)
                ->getAbsolutePath("orderattachment");

            $attachFile = $varDirectory . "/" . $attachment->getPath();
            if (file_exists($attachFile)) {
                unlink($attachFile);
            }
            $attachment->delete();

            $result = ['success' => true];
        } catch (\Exception $e) {
            $result = [
                'success' => false,
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode()
            ];
        }

        return $result;
    }

    /**
     * Save attachment comment
     * @param \Magento\Framework\App\Request\Http $request
     * @return array
     */
    public function updateAttachment($request)
    {
        $result = [];
        $isAjax = $request->isAjax();
        $isPost = $request->isPost();
        $requestParams = $request->getParams();
        $attachmentId = $requestParams['attachment'];
        $hash = $requestParams['hash'];
        $comment = $this->escaper->escapeHtml($requestParams['comment']);
        $orderId = isset($requestParams['order_id']) ? $requestParams['order_id'] : null;

        if (!$isAjax || !$isPost || !$attachmentId || !$hash) {
            return ['success' => false, 'error' => __('Invalid Request Params')];
        }

        try {
            $attachment = $this->attachmentFactory->create()->load($attachmentId);

            if (!$attachment->getId() || ($orderId && $orderId !== $attachment->getOrderId())) {
                return ['success' => false, 'error' => __('Can\'t find a attachment to update.')];
            }

            if ($hash !== $attachment->getHash()) {
                return ['success' => false, 'error' => __('Invalid Hash Params')];
            }

            $attachment->setComment($comment);
            $attachment->save();
            $result = ['success' => true];
        } catch (\Exception $e) {
            $result = [
                'success' => false,
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode()
            ];
        }

        return $result;
    }

    /**
     * Preview attachment
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Framework\Controller\Result\Raw $response
     */
    public function previewAttachment($request, $response)
    {
        $result = [];
        $attachmentId = $request->getParam('attachment');
        $hash = $request->getParam('hash');
        $download = $request->getParam('download');

        if (!$attachmentId || !$hash) {
            $result = ['success' => false, 'error' => __('Invalid Request Params')];
            $response->setHeader('Content-type', 'text/plain')
                ->setContents(json_encode($result));

            return $response;
        }

        try {
            $attachment = $this->attachmentFactory->create()->load($attachmentId);

            if (!$attachment->getId()) {
                $result = ['success' => false, 'error' => __('Can\'t find a attachment to preview.')];
                $response->setHeader('Content-type', 'text/plain')
                    ->setContents(json_encode($result));

                return $response;
            }

            if ($hash !== $attachment->getHash()) {
                $result = ['success' => false, 'error' => __('Invalid Hash Params')];
                $response->setHeader('Content-type', 'text/plain')
                    ->setContents(json_encode($result));

                return $response;
            }

            $varDirectory = $this->fileSystem
                ->getDirectoryRead(DirectoryList::VAR_DIR)
                ->getAbsolutePath("orderattachment");
            $attachmentFile = $varDirectory . "/" . $attachment->getPath();

            $attachmentType = explode('/', $attachment->getType());
            $handle = fopen($attachmentFile, "r");
            if ($download) {
                $response
                    ->setHeader('Content-Type', 'application/octet-stream', true)
                    ->setHeader(
                        'Content-Disposition',
                        'attachment; filename="' . basename($attachmentFile) . '"',
                        true
                    );
            } else {
                $response->setHeader('Content-Type', $attachment->getType(), true);
            }
            $response->setContents(fread($handle, filesize($attachmentFile)));
            fclose($handle);
        } catch (\Exception $e) {
            $result = ['success' => false, 'error' => $e->getMessage(), 'errorcode' => $e->getCode()];
            $response->setHeader('Content-type', 'text/plain');
            $response->setContents(json_encode($result));
        }

        return $response;
    }

    /**
     * Get attachment config json
     * @param mixed $block
     * @return string
     */
    public function getAttachmentConfig($block)
    {
        $config = [
            'attachments' => $block->getOrderAttachments(),
            'limit' => $this->scopeConfig->getValue(
                \Iksanika\Orderattach\Model\Attachment::XML_PATH_ATTACHMENT_FILE_LIMIT,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'size' => $this->scopeConfig->getValue(
                \Iksanika\Orderattach\Model\Attachment::XML_PATH_ATTACHMENT_FILE_SIZE,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'ext' => $this->scopeConfig->getValue(
                \Iksanika\Orderattach\Model\Attachment::XML_PATH_ATTACHMENT_FILE_EXT,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'uploadUrl' => $block->getUploadUrl(),
            'updateUrl' => $block->getUpdateUrl(),
            'removeUrl' => $block->getRemoveUrl()
        ];

        return $this->jsonEncoder->encode($config);
    }

    /**
     * Load order attachments by order id or by quote id
     * @param  int $entityId
     * @param  bool $byOrder load by order or by quote
     * @return array
     */
    public function getOrderAttachments($entityId, $byOrder = true)
    {
        $attachmentModel = $this->attachmentFactory->create();
        if ($byOrder) {
            $attachments = $attachmentModel->getOrderAttachments($entityId);
            $baseUrl = $this->storeManager->getStore()
                ->getBaseUrl() . DirectoryList::VAR_DIR . '/orderattachment/';
        } else {
            $attachments = $attachmentModel->getAttachmentsByQuote($entityId);
        }

        if (count($attachments) > 0) {
            foreach ($attachments as &$attachment) {
                $download = $this->_urlBuilder->getUrl(
                    'orderattachment/attachment/preview',
                    [
                        'attachment' => $attachment['attachment_id'],
                        'hash' => $attachment['hash'],
                        'download' => 1
                    ]
                );
                $attachment['path'] = basename($attachment['path']);
                $attachment['download'] = $download;
                $attachment['comment'] = $this->escaper->escapeHtml($attachment['comment']);

                if ($byOrder) {
                    $preview = $this->_urlBuilder->getUrl(
                        'orderattachment/attachment/preview',
                        [
                            'attachment' => $attachment['attachment_id'],
                            'hash' => $attachment['hash']
                        ]
                    );
                    $attachment['preview'] = $preview;
                    $attachment['url'] = $baseUrl . $attachment['path'];
                }
            }

            return $attachments;
        }

        return false;
    }


    /**
     * Load order attachments by order id or by quote id
     * @param  bool $checkFrontEnd conditions stoak if we checking frontend part of the module
     * @param  bool $checkCheckout conditions stack if we checking on checkout page
     * @return bool
     */

    public function isAttachmentEnabled($checkFrontEnd = false, $checkCheckout = false)
    {
        $onCheckout             =   true;
        $customerGroupEnabled   =   true;

        $moduleEnabled = $this->scopeConfig->getValue(
            \Iksanika\Orderattach\Model\Attachment::XML_PATH_ENABLE_ATTACHMENT,
            ScopeInterface::SCOPE_STORE
        );

        if($checkFrontEnd)
        {
            if($checkCheckout)
            {
                $onCheckout = $this->scopeConfig->getValue(
                    \Iksanika\Orderattach\Model\Attachment::XML_PATH_ATTACHMENT_ON_CHECKOUT,
                    ScopeInterface::SCOPE_STORE
                );
            }

            $customerGroupSelected = $this->scopeConfig->getValue(
                \Iksanika\Orderattach\Model\Attachment::XML_PATH_ENABLE_CUSTOMER_GROUP,
                ScopeInterface::SCOPE_STORE
            );

            /**
             * Enabled for currect customer group
             */
            $customerGroupEnabled = false;

            $tempArr        =   explode(',', $customerGroupSelected);
            $customerGroup  =   array();
            foreach($tempArr as $customerGroupId)
            {
                $customerGroup[trim($customerGroupId)] = true;
            }

            if(!empty($customerGroup) && isset($customerGroup[\Iksanika\Orderattach\Model\Config\Source\Groups::CUSTOMER_GROP_ALL]))
            {
                $customerGroupEnabled = true;
            }else
            {
                if($this->_customerSession->isLoggedIn())
                {
                    $customerGroupId = $this->_customerSession->getCustomer()->getGroupId();

                    if(!empty($customerGroup) && isset($customerGroup[$customerGroupId]))
                    {
                        $customerGroupEnabled = true;
                    }
                }else
                if(isset($customerGroup[\Magento\Customer\Model\GroupManagement::NOT_LOGGED_IN_ID]))
                {
                    $customerGroupEnabled = true;
                }
            }
        }
        return ($moduleEnabled && $onCheckout && $customerGroupEnabled);
    }


    /**
     * Get config for order view file upload enabled
     * @return boolean
     */
    public function isEnabledFileUpload()
    {
        return (bool)$this->scopeConfig->getValue(
            \Iksanika\Orderattach\Model\Attachment::XML_PATH_ATTACHMENT_ON_ORDER_VIEW,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getConfigFileLimit()
    {
        return $this->scopeConfig->getValue(
            \Iksanika\Orderattach\Model\Attachment::XML_PATH_ATTACHMENT_FILE_LIMIT,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getConfigFileSize()
    {
        return $this->scopeConfig->getValue(
            \Iksanika\Orderattach\Model\Attachment::XML_PATH_ATTACHMENT_FILE_SIZE,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getConfigFileExt()
    {
        return $this->scopeConfig->getValue(
            \Iksanika\Orderattach\Model\Attachment::XML_PATH_ATTACHMENT_FILE_EXT,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getUrlUpload()
    {
        return $this->urlBuilder->getUrl('orderattachment/attachment/upload');
    }

    public function getUrlUpdate()
    {
        return $this->urlBuilder->getUrl('orderattachment/attachment/update');
    }

    public function getUrlRemove()
    {
        return $this->urlBuilder->getUrl('orderattachment/attachment/delete');
    }

}
