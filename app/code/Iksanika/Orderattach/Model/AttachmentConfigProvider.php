<?php
namespace Iksanika\Orderattach\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\UrlInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Iksanika\Orderattach\Model\Attachment;

class AttachmentConfigProvider implements ConfigProviderInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Iksanika\Orderattach\Model\ResourceModel\Attachment\Collection
     */
    protected $attachmentCollection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param UrlInterface $urlBuilder
     * @param CheckoutSession $checkoutSession
     * @param \Iksanika\Orderattach\Model\ResourceModel\Attachment\Collection $attachmentCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CheckoutSession $checkoutSession,
        \Iksanika\Orderattach\Model\ResourceModel\Attachment\Collection $attachmentCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Iksanika\Orderattach\Helper\Attachment $helper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
        $this->attachmentCollection = $attachmentCollection;
        $this->storeManager = $storeManager;
        $this->_helper = $helper;
    }

    public function getConfig()
    {
        return [
            'iksanikaAttachmentEnabled'     =>  $this->_helper->isAttachmentEnabled(true, true),
            'attachments'                   =>  $this->getUploadedAttachments(),
            'iksanikaAttachmentLimit'       =>  $this->_helper->getConfigFileLimit(),
            'iksanikaAttachmentSize'        =>  $this->_helper->getConfigFileSize(),
            'iksanikaAttachmentExt'         =>  $this->_helper->getConfigFileExt(),
            'iksanikaAttachmentUpload'      =>  $this->_helper->getUrlUpload(),
            'iksanikaAttachmentUpdate'      =>  $this->_helper->getUrlUpdate(),
            'iksanikaAttachmentRemove'      =>  $this->_helper->getUrlRemove(),
            'removeItem'                    =>  __('Remove Item'),
            'iksanikaAttachmentInvalidExt'  =>  __('Invalid File Type'),
            'iksanikaAttachmentComment'     =>  __('Write comment here'),
            'iksanikaAttachmentInvalidSize' =>  __('Size of the file is greather than allowed') . '(' . $this->_helper->getConfigFileSize() . ' KB)',
            'iksanikaAttachmentInvalidLimit'=>  __('You have reached the limit of files'),
        ];
    }

    private function getUploadedAttachments()
    {
        if ($quoteId = $this->checkoutSession->getQuote()->getId())
        {
            $attachments = $this->attachmentCollection
                ->addFieldToFilter('quote_id', $quoteId)
                ->addFieldToFilter('order_id', ['is' => new \Zend_Db_Expr('null')]);

            $defaultStoreId = $this->storeManager->getDefaultStoreView()->getStoreId();
            foreach ($attachments as $attachment)
            {
                $url = $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . "orderattachment/" . $attachment['path'];
                $attachment->setUrl($url);
                $preview = $this->storeManager->getStore($defaultStoreId)->getUrl(
                    'orderattachment/attachment/preview',
                    [
                        'attachment' => $attachment['attachment_id'],
                        'hash' => $attachment['hash']
                    ]
                );
                $attachment->setPreview($preview);
                $attachment->setPath(basename($attachment->getPath()));
            }
            $result = $attachments->toArray();
            $result = $result['items'];
            return $result;
        }

        return false;
    }

}
