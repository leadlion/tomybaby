<?php

namespace Iksanika\Orderattach\Model\Config\Source;

class Groups
{

    const CUSTOMER_GROP_ALL = -1;


    public function __construct(\Magento\Customer\Model\ResourceModel\Group\Collection $customerGroup)
    {
        $this->_customerGroup = $customerGroup;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $customerGroups = $this->_customerGroup->toOptionArray();
        array_unshift($customerGroups, array('value'=> self::CUSTOMER_GROP_ALL, 'label'=>'Any'));
        return $customerGroups;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $returnArray = [];
        foreach($this->toOptionArray() as $item)
        {
            $returnArray[$item['value']] = $item['label'];
        }
        return $returnArray;
    }
}
