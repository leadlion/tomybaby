<?php

namespace Iksanika\Orderattach\Model\ResourceModel\Attachment;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Iksanika\Orderattach\Model\Attachment', 'Iksanika\Orderattach\Model\ResourceModel\Attachment');
    }
}
