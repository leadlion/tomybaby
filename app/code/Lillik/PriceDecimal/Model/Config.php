<?php
/**
 *
 * @package Lillik\PriceDecimal\Model
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */

namespace Lillik\PriceDecimal\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config implements ConfigInterface
{

    const XML_PATH_PRICE_PRECISION_FRONTEND
        = 'catalog_price_decimal/general/price_precision_frontend';

    const XML_PATH_PRICE_PRECISION_BACKEND
        = 'catalog_price_decimal/general/price_precision_backend';

    const XML_PATH_GENERAL_ENABLE
        = 'catalog_price_decimal/general/enable';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getScopeConfig()
    {
        return $this->scopeConfig;
    }

    /**
     * Return Config Value by XML Config Path
     * @param $path
     *
     * @return mixed
     */
    public function getValueByPath($path)
    {
        return $this->getScopeConfig()->getValue($path);
    }

    /**
     * @return mixed
     */
    public function isEnable()
    {
        return $this->getValueByPath(self::XML_PATH_GENERAL_ENABLE);
    }

    /**
     * Return Price precision for frontend from store config
     *
     * @return mixed
     */
    public function getPricePrecisionFrontend()
    {
        return $this->getValueByPath(self::XML_PATH_PRICE_PRECISION_FRONTEND);
    }

    /**
     * Return Price precision for backend from store config
     *
     * @return mixed
     */
    public function getPricePrecisionBackend()
    {
        return $this->getValueByPath(self::XML_PATH_PRICE_PRECISION_BACKEND);
    }
}
