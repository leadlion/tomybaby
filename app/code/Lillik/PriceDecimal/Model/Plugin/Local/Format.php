<?php
/**
 *
 * @package package Lillik\PriceDecimal\Model\Plugin\Local
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */

namespace Lillik\PriceDecimal\Model\Plugin\Local;

use Lillik\PriceDecimal\Model\Plugin\PriceFormatPluginAbstract;

class Format extends PriceFormatPluginAbstract
{

    /**
     * {@inheritdoc}
     *
     * @param $subject
     * @param $result
     *
     * @return mixed
     */
    public function afterGetPriceFormat($subject, $result)
    {
        if ($this->getConfig()->isEnable()) {
            // Precision argument
            if ($this->state->getAreaCode() === 'adminhtml') {
                $result['precision'] = $this->getPricePrecisionBackend();
                $result['requiredPrecision'] = $this->getPricePrecisionBackend();
            } elseif ($this->state->getAreaCode() === 'frontend') {
                $result['precision'] = $this->getPricePrecisionFrontend();
                $result['requiredPrecision'] = $this->getPricePrecisionFrontend();
            }
        }

        return $result;
    }
}
