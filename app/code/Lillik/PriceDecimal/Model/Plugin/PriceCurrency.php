<?php
/**
 *
 * @package Lillik\PriceDecimal\Model\Plugin
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */

namespace Lillik\PriceDecimal\Model\Plugin;

class PriceCurrency extends PriceFormatPluginAbstract
{
    /**
     * {@inheritdoc}
     */
    public function beforeFormat(
        \Magento\Directory\Model\PriceCurrency $subject,
        ...$args
    ) {
        if ($this->getConfig()->isEnable()) {
            // add the optional arg
            if (!isset($args[1])) {
                $args[1] = true;
            }

            // Precision argument
            if ($this->state->getAreaCode() === 'adminhtml') {
                $args[2] = $this->getPricePrecisionBackend();
            } elseif ($this->state->getAreaCode() === 'frontend') {
                $args[2] = $this->getPricePrecisionFrontend();
            }
        }

        return $args;
    }

    /**
     * @param \Magento\Directory\Model\PriceCurrency $subject
     * @param callable $proceed
     * @param $price
     * @param array ...$args
     * @return float
     */
    public function aroundRound(
        \Magento\Directory\Model\PriceCurrency $subject,
        callable $proceed,
        $price,
        ...$args
    ) {
        if ($this->getConfig()->isEnable()) {
            // Precision argument
            if ($this->state->getAreaCode() === 'adminhtml') {
                return round($price, $this->getPricePrecisionBackend());
            } elseif ($this->state->getAreaCode() === 'frontend') {
                return round($price, $this->getPricePrecisionFrontend());
            } else {
                return $proceed($price);
            }
        } else {
            return $proceed($price);
        }
    }

    /**
     * @param \Magento\Directory\Model\PriceCurrency $subject
     * @param array ...$args
     * @return array
     */
    public function beforeConvertAndFormat(
        \Magento\Directory\Model\PriceCurrency $subject,
        ...$args
    ) {
        if ($this->getConfig()->isEnable()) {
            // add the optional args
            $args[1] = isset($args[1])? $args[1] : null;
            $args[2] = isset($args[2])? $args[2] : null;

            // Precision argument
            if ($this->state->getAreaCode() === 'adminhtml') {
                $args[3] = $this->getPricePrecisionBackend();
            } elseif ($this->state->getAreaCode() === 'frontend') {
                $args[3] = $this->getPricePrecisionFrontend();
            }
        }

        return $args;
    }

    /**
     * @param \Magento\Directory\Model\PriceCurrency $subject
     * @param array ...$args
     * @return array
     */
    public function beforeConvertAndRound(
        \Magento\Directory\Model\PriceCurrency $subject,
        ...$args
    ) {
        if ($this->getConfig()->isEnable()) {
            //add optional args
            $args[1] = isset($args[1])? $args[1] : null;
            $args[2] = isset($args[2])? $args[2] : null;

            // Precision argument
            if ($this->state->getAreaCode() === 'adminhtml') {
                $args[3] = $this->getPricePrecisionBackend();
            } elseif ($this->state->getAreaCode() === 'frontend') {
                $args[3] = $this->getPricePrecisionFrontend();
            }
        }

        return $args;
    }
}
