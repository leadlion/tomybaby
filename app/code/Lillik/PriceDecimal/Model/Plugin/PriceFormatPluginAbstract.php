<?php
/**
 *
 * @package Lillik\PriceDecimal\Model\Plugin
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */

namespace Lillik\PriceDecimal\Model\Plugin;

use Lillik\PriceDecimal\Model\ConfigInterface;
use Lillik\PriceDecimal\Model\PricePrecisionConfigTrait;
use Magento\Framework\App\State;

abstract class PriceFormatPluginAbstract
{
    use PricePrecisionConfigTrait;

    /** @var ConfigInterface  */
    protected $moduleConfig;

    /** @var State  */
    protected $state;

    /**
     * @param \Lillik\PriceDecimal\Model\ConfigInterface $moduleConfig
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        ConfigInterface $moduleConfig,
        State $state
    ) {
        $this->moduleConfig  = $moduleConfig;
        $this->state = $state;
    }
}
