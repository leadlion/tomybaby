<?php
/**
 *
 * @package Lillik\PriceDecimal
 *
 * @author  Lilian Codreanu <lilian.codreanu@gmail.com>
 */

namespace Lillik\PriceDecimal\Model;

trait PricePrecisionConfigTrait
{


    /**
     * @return \Lillik\PriceDecimal\Model\Config
     */
    public function getConfig()
    {
        return $this->moduleConfig;
    }

    /**
     * @return int|mixed
     */
    public function getPricePrecisionFrontend()
    {
        if ($this->getConfig()->isEnable()) {
            return $this->getConfig()->getPricePrecisionFrontend();
        }

        return 0;
    }

    /**
     * @return int|mixed
     */
    public function getPricePrecisionBackend()
    {
        if ($this->getConfig()->isEnable()) {
            return $this->getConfig()->getPricePrecisionBackend();
        }

        return 0;
    }
}
