<?php
/**
 * Copyright © 2018 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageXtensions\TntExpressConnect\Controller\Licence;

use Magento\Store\Model\ScopeInterface;

class Disable extends \Magento\Framework\App\Action\Action
{
    /**
     * @var string
     */
    protected $_code = 'tntexpressconnect';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $_writer;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * Disable constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $writer
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $writer,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_writer = $writer;
        $this->_resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }

    /**
     * Execute the disable licence function
     *
     * @return $this|\Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = array();

        $licenceKey = $this->getRequest()->getParam('licence_key');
        $response['licence_key'] = $licenceKey;
        $response['success'] = false;

        $storeLicenceKey = $this->_scopeConfig
            ->getValue(
                'carriers/' . $this->_code . '/licence_key',
                ScopeInterface::SCOPE_STORE
            );

        if ($storeLicenceKey == $licenceKey) {
            $this->_writer->save('carriers/' . $this->_code . '/active', 0);
            $response['status'] = $this->_scopeConfig
                ->getValue(
                    'carriers/' . $this->_code . '/active',
                    ScopeInterface::SCOPE_STORE
                );
            $response['success'] = true;
        }

        $result = $this->_resultJsonFactory->create();

        $result = $result->setData($response);

        return $result;
    }
}