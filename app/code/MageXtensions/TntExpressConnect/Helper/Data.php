<?php
/**
 * Copyright © 2018 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageXtensions\TntExpressConnect\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function getConfigData($config_path)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}