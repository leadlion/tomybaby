<?php
/**
 * Copyright © 2017 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageXtensions\TntExpressConnect\Model;


use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Item;
use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;

abstract class AbstractTnt extends AbstractCarrierOnline
{
    /**
     * TNT ExpressConnect Pricing URL
     */
    const GATEWAY_URL = 'https://express.tnt.com/expressconnect/pricing/getprice';

    /**
     * Date format for shipping date in request
     */
    const SHIPPING_DATE_FORMAT = 'Y-m-d';

    /**
     * Date format for current date in request
     */
    const CURRENT_DATE_FORMAT = 'Y-m-d\TH:i:s';

    const ERROR_TYPE_INFORMATION = 'I';
    const ERROR_TYPE_WARNING = 'W';
    const ERROR_TYPE_ERROR = 'E';

    /**
     * Check if the street address contains po box or rsd
     *
     * @param string $street
     * @return bool
     */
    protected function _validateStreet($street)
    {
        $street = str_replace("\n", ', ', $street);
        if (
            preg_match(
                "/^\s*((.)*(?:P(?:OST)?.?\s*(?:O(?:FF(?:ICE)?)?)?.?\s*(?:B(?:IN|OX)?)?)+|(?:B(?:IN|OX)+\s+)+)\s*\d+/i",
                $street
            )
            ||
            strpos(strtolower($street), "rsd") !== false
            ||
            preg_match("/locked *bag/i", $street)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Check whether the date is a public holiday in the given State
     * or whether the date is a weekend
     *
     * @param $date
     * @return bool
     */
    protected function _isHoliday($date)
    {
        $publicHolidays = explode(',', str_replace(' ', '', $this->getConfigData('holidays')));

        if (date('D', $date) == 'Sat'
            || date('D', $date) == 'Sun'
            || in_array($date, $publicHolidays)) {
            return true;
        }

        return false;
    }

    /**
     * Calculate the available shipping date based on the configuration
     *
     * @return false|string
     */
    protected function _getShippingDate()
    {
        $days = (int)$this->getConfigData('days_collection');

        while ($this->_isHoliday(date(strtotime("+{$days} days")))) {
            $days++;
        }

        return date(self::SHIPPING_DATE_FORMAT, strtotime("+{$days} days"));
    }

    /**
     * Convert the package weight to kilograms
     *
     * @return float
     */
    protected function _getWeightInKg($weight)
    {
        $unit = $this->_scopeConfig->getValue(
            \Magento\Directory\Helper\Data::XML_PATH_WEIGHT_UNIT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if ($unit == 'lbs') {
            $weight = $weight * 0.453592;
        }

        return round($weight, 2);
    }

    /**
     * Calculate volume of items in the cart
     *
     * @param RateRequest $request
     * @return float
     */
    protected function _getPackageVolume($request)
    {
        $totalVolume = 0;

        /** @var Item $item */
        foreach ($request->getAllItems() as $item) {
            $product = $item->getProduct();
            if ($product->hasLength() && $product->hasWidth() && $product->hasHeight()) {
                $totalVolume += $product->getLength() * $product->getWidth() * $product->getHeight() * $item->getQty();
            } else {
                $totalVolume += $this->getConfigData('default_length') * $this->getConfigData('default_width')
                    * $this->getConfigData('default_height') * $item->getQty();
            }
        }

        return round($totalVolume / 1000000, 3);
    }

    /**
     * Get the Description of Goods from configuration
     *
     * @return string
     */
    protected function _isDangerousGoods()
    {
        return (int)$this->getConfigData('dangerous_goods') ? 'true' : 'false';
    }

    /**
     * Get package type (document/non-document) from configuration
     *
     * @return string
     */
    protected function _getPackageType()
    {
        return $this->getConfigData('is_document') ? 'D' : 'N';
    }

    /**
     * Get default package length from configuration
     *
     * @return int
     */
    protected function _getPackageLength()
    {
        return (int)$this->getConfigData('default_length');
    }

    /**
     * Get default package width from configuration
     *
     * @return int
     */
    protected function _getPackageWidth()
    {
        return (int)$this->getConfigData('default_width');
    }

    /**
     * Get default package height from configuration
     *
     * @return int
     */
    protected function _getPackageHeight()
    {
        return (int)$this->getConfigData('default_height');
    }

    /**
     * Calculate the insurance value based on the percentage set in the configuration
     *
     * @param float $goodsValue
     * @return float
     */
    protected function _getInsuranceValue($goodsValue)
    {
        return round($goodsValue * $this->getConfigData('insurance_percentage') / 100, 2);
    }

    /**
     * Get item's data from product info (length, width, height)
     *
     * @param Item $item
     * @param 'length'|'width'|'height' $data
     * @return float
     */
    protected function _getItemData($item, $data)
    {
        if ($item->getProduct()->hasData($data)) {
            $value = $item->getProduct()->getData($data);
        } else {
            $value = $this->getConfigData('default_' . $data);
        }

        return round($value / 100, 2);
    }
    
    /**
     * Replace accented characters with non accented
     *
     * @param $str
     * @return mixed
     * @link http://myshadowself.com/coding/php-function-to-convert-accented-characters-to-their-non-accented-equivalant/
     */
    function removeAccents($str) {
      $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
      $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
      return str_replace($a, $b, $str);
    }
}