<?php
/**
 * Copyright (C) 2018 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageXtensions\TntExpressConnect\Model;


use Magento\Framework\DataObject;
use Magento\Framework\Xml\Security;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\Error;
use Magento\Sales\Model\Order\Shipment;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * TNT Express Connect Australia
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Carrier extends AbstractTnt implements CarrierInterface
{
    /**
     * Code of the carrier
     */
    const CODE = 'tntexpressconnect';

    /**
     * @var bool
     */
    protected $_isFixed = false;

    /**
     * Instance of the RateRequest
     *
     * @var RateRequest
     */
    protected $_request;

    /**
     * Rate result data
     *
     * @var Result|null
     */
    protected $_result;

    /**
     * Errors placeholder
     *
     * @var string|false
     */
    protected $_errors = false;

    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * Flag that shows if shipping is domestic
     *
     * @var bool
     */
    protected $_isDomestic = true;

    /**
     * Core string
     *
     * @var \Magento\Framework\Stdlib\StringUtils
     */
    protected $string;

    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_httpClientFactory;

    /**
     * @inheritdoc
     */
    protected $_debugReplacePrivateDataKeys = [
        'senderAccount'
    ];
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * TNT Express constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param Security $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Directory\Helper\Data $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->string = $string;
        $this->_dateTime = $dateTime;
        $this->_httpClientFactory = $httpClientFactory;
        $this->storeManager = $storeManager;
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $data
        );
    }

    /**
     * Returns value of given variable
     *
     * @param string|int $origValue
     * @param string $pathToValue
     * @return string|int|null
     */
    protected function _getDefaultValue($origValue, $pathToValue)
    {
        if (!$origValue) {
            $origValue = $this->_scopeConfig->getValue(
                $pathToValue,
                ScopeInterface::SCOPE_STORE,
                $this->getStore()
            );
        }

        return $origValue;
    }

    /**
     * Collect and get rates
     *
     * @param RateRequest $request
     * @return bool|Result|Error
     */
    public function collectRates(RateRequest $request)
    {
        if (!$request->getDestCity() || !$request->getDestPostcode() || !$request->getDestCountryId()) {
            return false;
        }
        
        if (!$this->canCollectRates()) {
            return $this->getErrorMessage();
        }

        if ($request->getDestStreet() && !$this->_validateStreet($request->getDestStreet())) {
            $this->_errors = __('TNT cannot deliver to PO Boxes or RSD address. Please provide another address.');
            $error = $this->getErrorMessage();
            if ($error) {
                $result = $this->_rateFactory->create();
                $result->append($error);
                return $result;
            }
            return false;
        }

        $requestTnt = clone $request;
        $this->setStore($requestTnt->getStoreId());

        $origCountryId = $this->_getDefaultValue($requestTnt->getOrigCountryId(), Shipment::XML_PATH_STORE_COUNTRY_ID);
        $origState = $this->_getDefaultValue($requestTnt->getOrigState(), Shipment::XML_PATH_STORE_REGION_ID);
        $origCity = $this->_getDefaultValue($requestTnt->getOrigCity(), Shipment::XML_PATH_STORE_CITY);
        $origPostcode = $this->_getDefaultValue($requestTnt->getOrigPostcode(), Shipment::XML_PATH_STORE_ZIP);

        $requestTnt->setOrigCountryId($origCountryId)
            ->setOrigState($origState)
            ->setOrigCity($origCity)
            ->setOrigPostcode($origPostcode);
        $this->setRequest($requestTnt);

        $this->_result = $this->_getQuotes();

        /*$this->_rates[] = [
            'code' => 'exp',
            'title' => $this->getConfigData('method'),
            'price' => (float)$this->getConfigData('fixed_rate'),
        ];

        /*$result = $this->_rateFactory->create();
        if ($this->_rates) {
            foreach ($this->_rates as $rate) {
                $result->append($this->createResultMethod($rate));
            }
        } else {
            if (!empty($this->_errors)) {
                $this->debugErrors($this->_errors);
            }
            $result->append($this->getErrorMessage());
        }

        $this->_result = $result;*/

        $this->_updateFreeMethodQuote($request);

        return $this->_result;
    }

    /**
     * Set Free Method Request
     *
     * @param string $freeMethod
     * @return void
     */
    protected function _setFreeMethodRequest($freeMethod)
    {
        $this->_rawRequest->setFreeMethodRequest(true);
        $freeWeight = $this->getTotalNumOfBoxes($this->_request->getFreeMethodWeight());
        $this->_rawRequest->setWeight($freeWeight);
        $this->_rawRequest->setService($freeMethod);
    }

    /**
     * Prepare and set request in property of current instance
     *
     * @param RateRequest $request
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function setRequest($request)
    {
        $this->_request = $request;
        $this->setStore($request->getStoreId());

        $requestObject = new DataObject();

        $requestObject->setStoreId($request->getStoreId());

        if ($request->getDestPostcode()) {
            $requestObject->setDestPostcode($request->getDestPostcode());
        }

        $shippingWeight = $request->getPackageWeight();

        $requestObject->setValue(round($request->getPackageValue(), 2))
            ->setValueWithDiscount($request->getPackageValueWithDiscount())
            ->setCustomsValue($request->getPackageCustomsValue())
            ->setDestStreet($this->string->substr(str_replace("\n", '', $request->getDestStreet()), 0, 35))
            ->setDestStreetLine2($request->getDestStreetLine2())
            ->setDestCity($request->getDestCity())
            ->setOrigCompanyName($request->getOrigCompanyName())
            ->setOrigCountryId($request->getOrigCountryId())
            ->setOrigCity($request->getOrigCity())
            ->setOrigPhoneNumber($request->getOrigPhoneNumber())
            ->setOrigPersonName($request->getOrigPersonName())
            ->setOrigEmail(
                $this->_scopeConfig->getValue(
                    'trans_email/ident_general/email',
                    ScopeInterface::SCOPE_STORE,
                    $requestObject->getStoreId()
                )
            )
            ->setOrigCity($request->getOrigCity())
            ->setOrigPostcode($request->getOrigPostcode())
            ->setOrigStreetLine2($request->getOrigStreetLine2())
            ->setDestPhoneNumber($request->getDestPhoneNumber())
            ->setDestPersonName($request->getDestPersonName())
            ->setDestCompanyName($request->getDestCompanyName());

        $originStreet2 = $this->_scopeConfig->getValue(
            Shipment::XML_PATH_STORE_ADDRESS2,
            ScopeInterface::SCOPE_STORE,
            $requestObject->getStoreId()
        );

        $requestObject->setOrigStreet($request->getOrigStreet() ? $request->getOrigStreet() : $originStreet2);

        if (is_numeric($request->getOrigState())) {
            $requestObject->setOrigState($this->_regionFactory->create()->load($request->getOrigState())->getCode());
        } else {
            $requestObject->setOrigState($request->getOrigState());
        }

        if ($request->getDestCountryId()) {
            $destCountry = $request->getDestCountryId();
        } else {
            $destCountry = self::AUSTRALIA_COUNTRY_ID;
        }

        $requestObject->setDestCountryId($destCountry)
            ->setDestState($request->getDestRegionCode())
            ->setWeight($shippingWeight)
            ->setFreeMethodWeight($request->getFreeMethodWeight())
            ->setOrderShipment($request->getOrderShipment());

        if ($request->getPackageId()) {
            $requestObject->setPackageId($request->getPackageId());
        }

        $requestObject->setBaseSubtotalInclTax($request->getBaseSubtotalInclTax());

        $this->setRawRequest($requestObject);

        return $this;
    }

    /**
     * Get allowed shipping methods
     *
     * @return string[]
     */
    public function getAllowedMethods()
    {
        $allowed = explode(',', $this->getConfigData('services_allowed'));
        $methods = [];
        foreach ($allowed as $method => $title) {
            $methods[$method] = $title;
        }

        return $methods;
    }

    /**
     * Get shipping quotes
     *
     * @return \Magento\Framework\Model\AbstractModel|Result
     */
    protected function _getQuotes()
    {
        $responseBody = '';
        try {
            $debugPoint = [];

            $requestXml = $this->_buildQuotesRequestXml();

            $request = $requestXml->asXML();

            $this->_debug($request);

            $debugPoint['request'] = $this->filterDebugData($request);
            $responseBody = $this->_getCachedQuotes($request);
            $debugPoint['from_cache'] = $responseBody === null;

            if ($debugPoint['from_cache']) {
                $responseBody = $this->_getQuotesFromServer($request);
            }

            $debugPoint['response'] = $this->filterDebugData($responseBody);

            $this->_setCachedQuotes($request, $responseBody);
            $this->_debug($debugPoint);
        } catch (\Exception $e) {
            $this->_errors[$e->getCode()] = $e->getMessage();
        }

        return $this->_parseResponse($responseBody);
    }

    /**
     * Get shipping quotes from TNT service
     *
     * @param string $data
     * @return string
     * @throws \Zend_Http_Client_Exception
     */
    protected function _getQuotesFromServer($data)
    {
        $headers = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Basic '. base64_encode($this->getConfigData('tnt_username') . ':' . $this->getConfigData('tnt_password'))
        );

        $client = $this->_httpClientFactory->create();
        $client->setUri(self::GATEWAY_URL);
        $client->setHeaders($headers);
        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
        $client->setRawData(utf8_encode($data));

        return $client->request(\Zend_Http_Client::POST)->getBody();
    }

    /**
     * Build quotes request XML object
     *
     * @return \SimpleXMLElement
     */
    protected function _buildQuotesRequestXml()
    {
        /** @var RateRequest $request */
        $request = $this->_request;

        /** @var \SimpleXMLElement $xml */
        $xml = $this->_xmlElFactory->create(
            ['data' => '<?xml version="1.0" encoding="UTF-8" standalone="no"?><priceRequest/>']
        );

        $xml->addChild('appId', 'PC');
        $xml->addChild('appVersion', '3.0');
        $priceCheck = $xml->addChild('priceCheck');

        $priceCheck->addChild('rateId', 'rate1');

        $sender = $priceCheck->addChild('sender');
        $sender->addChild('country', $this->removeAccents($request->getOrigCountryId()));
        $sender->addChild('town', $this->removeAccents($request->getOrigCity()));
        $sender->addChild('postcode', $this->removeAccents($request->getOrigPostcode()));

        $delivery = $priceCheck->addChild('delivery');
        $delivery->addChild('country', $this->removeAccents($request->getDestCountryId()));
        $delivery->addChild('town', $this->removeAccents($request->getDestCity()));
        $delivery->addChild('postcode', $this->removeAccents($request->getDestPostcode()));

        $priceCheck->addChild('collectionDateTime', $this->_getShippingDate());

        $product = $priceCheck->addChild('product');
        $product->addChild('type', $this->_getPackageType());

        $account = $priceCheck->addChild('account');
        $account->addChild('accountNumber', $this->getConfigData('tnt_account'));
        $account->addChild('accountCountry', $this->getConfigData('tnt_country'));

        if ($this->getConfigData('insurance_enable')) {
            $insurance = $priceCheck->addChild('insurance');
            $insurance->addChild('insuranceValue', $this->_getInsuranceValue($request->getPackageValue()));
            $insurance->addChild('goodsValue', $request->getPackageValue());
        }

        $priceCheck->addChild('currency', $request->getPackageCurrency()->getCode());
        $priceCheck->addChild('priceBreakDown', 'false');

        /*$consignmentDetails = $priceCheck->addChild('consignmentDetails');
        $consignmentDetails->addChild('totalWeight', $this->_getWeightInKg($request->getPackageWeight()));
        $consignmentDetails->addChild('totalVolume', $this->_getPackageVolume($request));
        $consignmentDetails->addChild('totalNumberOfPieces', $request->getPackageQty());*/

        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($request->getAllItems() as $item) {
            $pieceLine = $priceCheck->addChild('pieceLine');
            $pieceLine->addChild('numberOfPieces', $item->getQty());
            $pieceMeasurements = $pieceLine->addChild('pieceMeasurements');
            $pieceMeasurements->addChild('length', $this->_getItemData($item, 'length'));
            $pieceMeasurements->addChild('width', $this->_getItemData($item, 'width'));
            $pieceMeasurements->addChild('height', $this->_getItemData($item, 'height'));
            $pieceMeasurements->addChild('weight', $this->_getWeightInKg($item->getWeight()));
            $pieceLine->addChild('pallet', 0);
        }

        return $xml;
    }

    /**
     * Parse and extract shipping rates from response
     *
     * @param $response
     * @return Result
     */
    private function _parseResponse($response)
    {
        if (strpos($response, 'html') !== false) {
            $this->_errors = strip_tags($response);
        } else {
            $this->parseXml($response);
        }

        /** @var Result $result */
        $result = $this->_rateFactory->create();
        if ($this->_rates) {
            foreach ($this->_rates as $rate) {
                $result->append($this->createResultMethod($rate));
            }
        } else {
            if (!empty($this->_errors)) {
                $this->debugErrors($this->_errors);
            }
            $result->append($this->getErrorMessage());
        }

        return $result;
    }

    /**
     * Parse XML string and return XML document object or false
     *
     * @param $xmlContent
     * @param string $customSimpleXml
     * @return bool|\SimpleXMLElement
     */
    public function parseXml($xmlContent, $customSimpleXml = 'SimpleXMLElement')
    {
        $document = false;

        try {
            $document = simplexml_load_string($xmlContent, $customSimpleXml);
        } catch (\Exception $e) {
            $this->_errors = $e->getCode() . ' ' . $e->getMessage();
        }

        if ($document) {
            if (isset($document->priceResponse->ratedServices)) {
                foreach($document->priceResponse->ratedServices->ratedService as $rate) {
                    if (in_array($rate->product->id, $this->getAllowedMethods()) && isset($rate->totalPrice)) {
                        $this->_addRate($rate);
                    }
                }
            } elseif (isset($document->errors)) {
                if (isset($document->errors->parseError)) {
                    $this->_errors = $document->errors->parseError->errorReason->__toString();
                } elseif (isset($document->errors->brokenRule)
                    && $document->errors->brokenRule->messageType == self::ERROR_TYPE_ERROR
                ) {
                    $this->_errors = $document->errors->brokenRule->description->__toString();
                } elseif (isset($document->errors->runtimeError)) {
                    $this->_errors = $document->errors->runtimeError->errorReason->__toString();
                }
            } else {
                $this->_errors = __('An undetermined error has occurred. Please check TNT module configuration.');
            }
        } else {
            $this->_errors = __('Failed loading XML');
            foreach(libxml_get_errors() as $xml_error) {
                $this->_errors .= $xml_error->message;
            }
        }

        return $document;
    }

    /**
     * Get relevant data for each available services
     *
     * @param \SimpleXMLElement $service
     * @return $this
     */
    private function _addRate($service) {
        $this->_rates[] = [
            'code' => $service->product->id->__toString(),
            //'title' => $service->product->productDesc->__toString(),
            'title' => $this->getConfigData('method'),
            //'price' => $service->totalPrice->__toString(),
            'price' => $this->getConfigData('fixed_rate') !== '' ? (float)$this->getConfigData('fixed_rate') : $service->totalPrice->__toString(),
        ];

        return $this;
    }

    /**
     * Create the rate result method for a service
     *
     * @param array $rate
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Method
     */
    private function createResultMethod($rate)
    {
        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
        $method = $this->_rateMethodFactory->create();

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod($this->_code . '_' . $rate['code']);
        $method->setMethodTitle($rate['title']);

        $method->setCost($rate['price']);
        $method->setPrice($this->getFinalPriceWithHandlingFee($rate['price']));

        return $method;
    }

    /**
     * Get an appropriate error description
     *
     * @return string
     */
    private function _getErrorDescription()
    {
        if (isset($this->_errors) && $this->_errors) {
            if ($this->_errors = 'Destination address town/postcode combination invalid') {
                return 'Adresa de livrare oras/cod postal nu este valida';
            }
            return $this->_errors;
        }

        return $this->getConfigData('specificerrmsg');
    }

    /**
     * Get error messages
     *
     * @return bool|Error
     */
    protected function getErrorMessage()
    {
        if ($this->getConfigData('showmethod')) {
            /* @var $error Error */
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->_getErrorDescription());
            return $error;
        } else {
            return false;
        }
    }

    /**
     * Do shipment request to carrier web service, obtain Print Shipping Labels and process errors in response
     *
     * @param DataObject $request
     * @return DataObject
     */
    protected function _doShipmentRequest(DataObject $request)
    {
        return new DataObject;
    }

    /**
     * Processing additional validation to check is carrier applicable.
     *
     * @param DataObject $request
     * @return $this|DataObject|boolean
     */
    public function proccessAdditionalValidation(DataObject $request)
    {
        return $this;
    }
    
    public function processAdditionalValidation(DataObject $request)
    {
        return $this;
    }

    public function isTrackingAvailable()
    {
        return true;
    }

    public function isShippingLabelsAvailable()
    {
        return false;
    }

    public function getTrackingInfo($trackingNumber)
    {
        $tracking = $this->_trackStatusFactory->create();

        $url = $this->getConfigData('tracking_url') . $trackingNumber;

        $tracking->setData([
            'carrier' => $this->_code,
            'carrier_title' => $this->getConfigData('title'),
            'tracking' => $trackingNumber,
            'url' => $url,
        ]);
        return $tracking;
    }

}