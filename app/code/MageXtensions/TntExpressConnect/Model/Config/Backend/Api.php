<?php
/**
 * Copyright © 2018 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageXtensions\TntExpressConnect\Model\Config\Backend;

use Magento\Framework\App\Config\Value as ConfigValue;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;

class Api extends ConfigValue
{
    /**
     * Check licence data before saving the configuration
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeSave()
    {
        $data = array(
            'licence_key'   => $this->getValue(),
            'extension_id'  => 'TECM2',
            'domain'        => $this->_getDomain()
        );

        $response = $this->_validate($data);

        if (isset($response['error']) && $response['error']) {
            throw new LocalizedException(
                __('TNT Express: ' . $response['error'])
            );
        }

        parent::beforeSave();
    }

    /**
     * Get the domain of the magento installation
     *
     * @return string
     */
    private function _getDomain()
    {
        $_objectManager = ObjectManager::getInstance();
        $storeManager = $_objectManager
            ->get('Magento\Store\Model\StoreManagerInterface');
        $currentStore = $storeManager->getStore();
        $domain = $currentStore->getBaseUrl();

        return strtolower(str_replace('index.php/', '', $domain));
    }

    /**
     * Send request to validate licence
     *
     * @param array $data
     * @return array|mixed
     */
    private function _validate($data)
    {
        $curl = curl_init();

        $headers = array(
            "Content-Type: application/json",
            'Content-Length: ' . strlen(json_encode($data))
        );

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $curl,
            CURLOPT_URL,
            'https://magextensions.com.au/rest/default/V1/licence/check'
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

        $response = json_decode(curl_exec($curl), true);

        if (curl_errno($curl)) {
            $error = 'Error ' . curl_errno($curl) . ': ' . curl_error($curl);
        } else {
            $error = false;
        }

        curl_close($curl);

        if ($error) {
            return array('error' => $error);
        }

        return json_decode($response, true);
    }
}