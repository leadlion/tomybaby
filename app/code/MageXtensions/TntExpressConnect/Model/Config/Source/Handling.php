<?php
/**
 * Copyright © 2018 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageXtensions\TntExpressConnect\Model\Config\Source;

/**
 * Class Handling
 * @package MageXtensions\TntExpressConnect\Model\Config\Source
 */
class Handling implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * List of Handling Types
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'F', 'label' => __('Fixed Amount')],
            ['value' => 'P', 'label' => __('Percentage')],
        ];
    }
}