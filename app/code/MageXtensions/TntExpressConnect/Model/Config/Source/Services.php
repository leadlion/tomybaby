<?php
/**
 * Copyright © 2018 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace MageXtensions\TntExpressConnect\Model\Config\Source;

/**
 * Class Services
 * @package MageXtensions\TntExpressConnect\Model\Config\Source
 */
class Services implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * List of TNT Express services
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '09N', 'label' => __('09:00 Express')],
            ['value' => '10N', 'label' => __('10:00 Express')],
            ['value' => '12N', 'label' => __('12:00 Express')],
            ['value' => '412', 'label' => __('12:00 Economy Express')],
            ['value' => '15N', 'label' => __('Express')],
            ['value' => '15', 'label' => __('Domestic Express')],
            ['value' => '48N', 'label' => __('Economy Express')],
            ['value' => '29', 'label' => __('IDE Express')],
            ['value' => '30', 'label' => __('IDE Economy')],
        ];
    }
}