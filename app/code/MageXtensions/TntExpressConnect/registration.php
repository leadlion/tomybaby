<?php
/**
 * Copyright © 2018 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MageXtensions_TntExpressConnect',
    __DIR__
);