/**
 * Copyright © 2018 MageXtensions. All rights reserved.
 * See LICENSE.txt for license details.
 */
var config = {
    map: {
        '*': {
            'Magento_Checkout/js/model/cart/totals-processor/default': 'MageXtensions_TntExpressConnect/js/model/cart/totals-processor/default'
        }
    }
};