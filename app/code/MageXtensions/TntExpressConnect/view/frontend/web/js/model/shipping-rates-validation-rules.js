define(
    [],
    function () {
        'use strict';
        return {
            getRules: function() {
                return {
                    'street': {
                        'required': true
                    },
                    'postcode': {
                        'required': true
                    },
                    'city': {
                        'required': true
                    },
                    'country_id': {
                        'required': true
                    }
                };
            }
        };
    }
)