# MageXtensions Tracking Link

Add Tracking URL in Shipment Email.

Native shipment email doesn’t include a clickable tracking number link, just plain text one. The customer has to copy and then go to the courier site to get the tracking information. The extension make the tracking number a clickable link that will bring them to the carriers site and display the tracking information.

The extension supports all couriers which support shipping tracking.

## Compatibility

Magento CE(EE) 2.0.x, 2.1.x, 2.2.x, 2.3.x

## Install

#### Manual Installation

1. Create a folder {Magento root}/app/code/MageXtensions/TrackingLink

2. Download the latest version from your My Downloads on MageXtensions website

3. Copy the unzip content to the folder ({Magento root}/app/code/MageXtensions/TrackingLink)

### Completion of installation

1. Go to Magento2 root folder

2. Enter following commands:

    ```bash
    php bin/magento setup:upgrade
    php bin/magento setup:di:compile
    php bin/magento setup:static-content:deploy  (optional)
    ```

### Configuration

In the Magento Admin Panel go to *Stores > Configuration > Sales > Tracking Settings > Tracking Service Url*.

## Uninstall
This works only with modules defined as Composer packages.

#### Remove database data

1. Go to Magento2 root folder

2. Enter following commands to remove database data:

    ```bash
    php bin/magento module:uninstall -r MageXtensions_TrackingLink
    ```

#### Remove Extension

1. Go to Magento2 root folder

2. Enter following commands:

    ```bash
    php bin/magento setup:upgrade
    php bin/magento setup:di:compile
    php bin/magento setup:static-content:deploy  (optional)
    ```
