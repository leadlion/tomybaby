<?php

namespace Meetanshi\MultiProductUpdate\Api;

interface ProductUpdateInterface
{

    /**
     * GET for Post api
     * @param string $param
     * @return string
     */

    public function productUpdate();
}