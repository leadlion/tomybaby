<?php

namespace Meetanshi\MultiProductUpdate\Cron;

use Magento\Framework\HTTP\Client\Curl;
use Psr\Log\LoggerInterface;

class ProductUpdate
{
    protected $logger;
    public function __construct(Curl $curl,
     LoggerInterface $logger
    )
    {
        $this->curl = $curl;
    }

    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/product_update.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $url = "https://temanovelart.ro/rest/all/V1/product-update/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
        $api_response_data = curl_exec($ch);
        curl_close($ch);
        $logger->info(print_r($api_response_data,true));

    }
}