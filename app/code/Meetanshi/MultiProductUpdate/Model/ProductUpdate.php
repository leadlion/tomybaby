<?php

namespace Meetanshi\MultiProductUpdate\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Meetanshi\MultiProductUpdate\Api\ProductUpdateInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Filesystem;

class ProductUpdate implements ProductUpdateInterface
{
    public $request;
    protected $productRepository;
    protected $stockRegistry;
    protected $collectionFactory;
    protected $json;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        Json $json,
        Http $request,
        Filesystem $filesystem,
        StockRegistryInterface $stockRegistry,
        CollectionFactory $collectionFactory)
    {
        $this->stockRegistry = $stockRegistry;
        $this->productRepository = $productRepository;
        $this->request = $request;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->json = $json;
        $this->collectionFactory = $collectionFactory;
    }

    public function productUpdate()
    {

        $filepath = 'export/productupdate.csv';
        $this->directory->create('export');
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();
        $header = ['sku', 'qty'];
        $stream->writeCsv($header);
        $url = "https://ro.expertaccounts.com//?api=public&t1=b618c3210e934362ac261db280128c22&t2=845561665f33c461be03db0a1aeddf76&json=true";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
        $api_response_data = curl_exec($ch);
        curl_close($ch);
        $api_allvalue = $this->json->unserialize($api_response_data);
        for ($i = 0; $i < sizeof($api_allvalue); $i++) {
            $sku_name = $api_allvalue[$i]['info3'];
            $product_qty = $api_allvalue[$i]['stoc'];
            $data = [];
            $data[] = $sku_name;
            $data[] = $product_qty;
            $stream->writeCsv($data);
        }
        $file = fopen('var/export/productupdate.csv', 'r', '"');
        if ($file !== false) {
            $error = array();
            $k = 0;
            $product_updatesku = array();
            $l = 0;
            $product_update_count = 0;
            $product_notupdate_count = 0;
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productRepository = $objectManager->get('Magento\Catalog\Model\ProductRepository');
            $stockRegistry = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');
            $required_data_fields = 2;
            $header = fgetcsv($file);
            while ($row = fgetcsv($file, 4000, ",")) {
                $data_count = count($row);
                if ($data_count < 1) {
                    continue;
                }
                $data = array();
                $data = array_combine($header, $row);
                $sku = $data['sku'];
                if ($data_count < $required_data_fields) {
                    continue;
                }
                $qty = trim($data['qty']);
                try {
                    $product = $productRepository->get($sku);
                } catch (\Exception $e) {
                    //$logger->info("Invalid product SKU: ".$sku);
                    $error[$k] = $sku;
                    $product_notupdate_count++;
                    $k++;
                    continue;
                }
                try {
                    $stockItem = $stockRegistry->getStockItemBySku($sku);
                } catch (\Exception $e) {
                    $error[$k] = $sku;
                    $product_notupdate_count++;
                    $k++;
                    continue;
                }
                if ($stockItem->getQty() == $qty) {
                    $product_updatesku[$l] = $sku;
                    $product_update_count++;
                    $l++;
                }
                if ($stockItem->getQty() != $qty) {
                    $product_updatesku[$l] = $sku;
                    $l++;
                    $product_update_count++;
                    $stockItem->setQty($qty);
                    if ($qty > 0) {
                        $stockItem->setIsInStock(1);
                    }
                    $stockRegistry->updateStockItemBySku($sku, $stockItem);
                }
            }
            fclose($file);
        }
        $response = [
            'product_not_update_count' => $product_notupdate_count,
            'product_update_count' => $product_update_count,
            'product_not_update_sku' => $error
        ];
        return json_encode($response);
    }

}