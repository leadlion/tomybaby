<?php

namespace Temanovelart\Amastyxnotif\Block\Html\Link;

class Current extends \Magento\Framework\View\Element\Html\Link\Current
{
    /**
     * @var \Amasty\Xnotif\Helper\Data
     */
    private $helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\DefaultPathInterface $defaultPath,
        \Amasty\Xnotif\Helper\Config $helper,
        array $data = []
    ) {
        parent::__construct($context, $defaultPath, $data);
        $this->helper = $helper;
    }

    protected function _toHtml()
    {
        if(!$this->helper->allowForCurrentCustomerGroup($this->getData('internalIndex'))) {
            return '';
        }
        return parent::_toHtml();
    }
}
