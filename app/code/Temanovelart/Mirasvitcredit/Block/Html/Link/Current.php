<?php

namespace Temanovelart\Mirasvitcredit\Block\Html\Link;

class Current extends \Magento\Framework\View\Element\Html\Link\Current
{
    /**
     * @var \Temanovelart\Mirasvitcredit\Helper\Data
     */
    private $helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\DefaultPathInterface $defaultPath,
        \Temanovelart\Mirasvitcredit\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $defaultPath, $data);
        $this->helper = $helper;
    }

    protected function _toHtml()
    {
        return (!$this->helper->isCurrentCustomerGroupAllowed()) ? '' : parent::_toHtml();
    }
}
