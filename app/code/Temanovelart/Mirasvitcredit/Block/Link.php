<?php

namespace Temanovelart\Mirasvitcredit\Block;

class Link extends \Mirasvit\Credit\Block\Link
{
    /**
     * @var \Temanovelart\Mirasvitcredit\Helper\Data
     */
    private $helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Temanovelart\Mirasvitcredit\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    protected function _toHtml()
    {
        return (!$this->helper->isCurrentCustomerGroupAllowed()) ? '' : parent::_toHtml();
    }

}
