<?php

namespace Temanovelart\Mirasvitcredit\Helper;

use Temanovelart\Mirasvitcredit\Model\Config\Source\Groups;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Customer\Model\SessionFactory
     */
    private $sessionFactory;

    public function __construct(
        \Magento\Customer\Model\SessionFactory $sessionFactory,
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
        $this->sessionFactory = $sessionFactory;
        $this->customerSession = $this->sessionFactory->create();
    }

    public function isCurrentCustomerGroupAllowed()
    {
        $allowedGroups = $this->getAllowedCustomerGroups();
        return in_array(Groups::CUSTOMER_GROP_ALL, $allowedGroups)
                ? true
                : in_array($this->sessionFactory->create()->getCustomerGroupId(), $allowedGroups);
    }

    public function getAllowedCustomerGroups()
    {
        $allowedGroups = $this->scopeConfig->getValue('credit/access/customer_groups');
        $allowedGroups = str_replace(' ', '', $allowedGroups);
        $allowedGroups = explode(',', $allowedGroups);
        return $allowedGroups;
    }

}
