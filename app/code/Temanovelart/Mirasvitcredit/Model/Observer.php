<?php

namespace Temanovelart\Mirasvitcredit\Model;

use Temanovelart\Mirasvitcredit\Helper\Data;
use Temanovelart\Mirasvitcredit\Model\Config\Source\Groups;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\App\Area as Area;

class Observer
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Model\Context
     */
    protected $context;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Model\Context           $context
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Model\Context $context,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        PricingHelper $pricingHelper,

        \Mirasvit\Credit\Model\BalanceFactory $balanceFactory,
        \Temanovelart\Mirasvitcredit\Helper\Data $helper,

        \Psr\Log\LoggerInterface $logger
    ) {
        $this->storeManager = $storeManager;
        $this->context = $context;
        $this->_log = $logger;
        $this->_helper = $helper;
        $this->_customerCollectionFactory = $customerCollectionFactory;
        $this->_balanceFactory = $balanceFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->_helperPricing = $pricingHelper;

    }

    public function process()
    {
        $allowedCustomersGroups = $this->_helper->getAllowedCustomerGroups();

        // get list of customers who feates alloed customers group
        $customersCollection = $this->_customerCollectionFactory->create();
        if(!in_array(Groups::CUSTOMER_GROP_ALL, $allowedCustomersGroups))
        {
            if(count($allowedCustomersGroups))
            {
                $customersCollection->addAttributeToFilter('group_id', array( 'in' => implode($allowedCustomersGroups, ',')));
                //$this->_log->error('Only specified groupos of customers are selected');
            }
        }else{
            //$this->_log->error('There is ALL CUSTOMERS option selected in settings - so full list of customers show be selected.');
        }

        //$this->_log->error('mirasvit credit enhancement: cron job triggered: START');
        foreach($customersCollection as $customer)
        {

            // get balance for $customer
            $customerBalance = $this->_balanceFactory->create()->loadByCustomer($customer);

            // generate $email is balance at lowest level
            //
            /**
             * @param int $storeId
             * @return string
             */
            $recipientEmail = $customer->getEmail();
            $recipientName  = $customer->getName();
            $storeId        = $customer->getStore()->getId();

            $balanceDueTemplate = $this->_scopeConfig->getValue('credit/email/balance_due_template', ScopeInterface::SCOPE_STORE, $storeId);
            $balanceDueValue = $this->_scopeConfig->getValue('credit/email/balance_due_amount', ScopeInterface::SCOPE_STORE, $storeId);

            if($customerBalance->getAmount() <= $balanceDueValue) // get minimum balance from enhancements settings
            {
                // generate email


                $emailSender    = $this->_scopeConfig->getValue('credit/email/email_identity', ScopeInterface::SCOPE_STORE, $storeId);

                $variables = [
                    'customer'            => $customer,
                    'store'               => $customer->getStore(),
                    'balance'             => $customerBalance,
                    'balance_amount'      => $this->_helperPricing->currency($customerBalance->getAmount(), true, false),
                ];

                $transport = $this->_transportBuilder
                    ->setTemplateIdentifier($balanceDueTemplate)
                    ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => $storeId])
                    ->setTemplateVars($variables)
                    ->setFrom($emailSender)
                    ->addTo($recipientEmail, $recipientName)
                    ->getTransport();
                    
echo $recipientEmail.'<br/>';                    
echo $transport->getTemplate();


                $transport->sendMessage();
            }

            $this->_log->error($customer->getId());
            $this->_log->error($customerBalance->getAmount());
        }
        //$this->_log->error('mirasvit credit enhancement: cron job triggered: END');
    }

}
