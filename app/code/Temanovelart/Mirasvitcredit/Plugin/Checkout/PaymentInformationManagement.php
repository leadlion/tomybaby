<?php

namespace Temanovelart\Mirasvitcredit\Plugin\Checkout;

use \Magento\Framework\Exception\CouldNotSaveException;
use \Magento\Framework\Phrase;

class PaymentInformationManagement
{
    public function __construct(
        \Temanovelart\Mirasvitcredit\Helper\Data $helper,
        \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalRepository
    ) {
        $this->_helper = $helper;
        $this->_cartTotalRepository = $cartTotalRepository;
    }

    public function aroundSavePaymentInformationAndPlaceOrder(\Magento\Checkout\Model\PaymentInformationManagement $subject, $proceed, $cartId, \Magento\Quote\Api\Data\PaymentInterface $paymentMethod, \Magento\Quote\Api\Data\AddressInterface $billingAddress = null)
    {
        if($this->_helper->isCurrentCustomerGroupAllowed())
        {
            $creditValue        = NULL;
            $grandTotalValue    = NULL;
            $cartTotals         = $this->_cartTotalRepository->get($cartId);
            $cartTotalsSegments = $cartTotals->getTotalSegments();
            foreach($cartTotalsSegments as $segment)
            {
                if($segment->getCode() == 'credit')
                {
                    $creditValue = $segment->getValue();
                }
                if($segment->getCode() == 'grand_total')
                {
                    $grandTotalValue = $segment->getValue();
                }
            }

            if($creditValue == NULL)
            {
                throw new CouldNotSaveException(
                    new Phrase(__('You have to apply credit before placing order.'))
                );
            }else
            {
                if($grandTotalValue > 0)
                {
                    throw new CouldNotSaveException(
                        new Phrase(__('Unable to proceed your order. You are out of credit balance. Please, top up your balance to proceed your order further.'))
                    );
                }
            }
/*
            // ValidatorException
            throw new CouldNotSaveException(
                new Phrase(__('Unable to proceed your order. You are out of credit balance. Please, top up your balance to proceed your order further. ' . $tmp))
            );
            return false;
*/
        }
        return $proceed($cartId, $paymentMethod, $billingAddress);
    }

}