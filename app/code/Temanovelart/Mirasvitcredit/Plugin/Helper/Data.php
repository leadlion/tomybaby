<?php

namespace Temanovelart\Mirasvitcredit\Plugin\Helper;

class Data
{
    public function __construct(
        \Temanovelart\Mirasvitcredit\Helper\Data $helper
    ) {
        $this->_helper = $helper;
    }

    public function aroundIsAllowedForQuote(\Mirasvit\Credit\Helper\Data $subject, $proceed)
    {
        return ($proceed() && $this->_helper->isCurrentCustomerGroupAllowed()) ? true : false;
    }

}