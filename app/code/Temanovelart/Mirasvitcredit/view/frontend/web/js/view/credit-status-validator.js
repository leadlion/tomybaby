define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Temanovelart_Mirasvitcredit/js/model/credit-validator'
    ],
    function (Component, additionalValidators, yourValidator) {
        'use strict';
        additionalValidators.registerValidator(yourValidator);
        return Component.extend({});
    }
);