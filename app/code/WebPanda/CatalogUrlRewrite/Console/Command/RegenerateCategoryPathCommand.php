<?php
namespace WebPanda\CatalogUrlRewrite\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Store\Model\Store;
use Magento\Framework\App\State;

/**
 * Class RegenerateCategoryPathCommand
 * @package WebPanda\CatalogUrlRewrite\Console\Command
 */
class RegenerateCategoryPathCommand extends Command
{
    /**
     * @var CategoryUrlPathGenerator
     */
    protected $categoryUrlPathGenerator;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * RegenerateCategoryPathCommand constructor.
     * @param State $state
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param CategoryUrlPathGenerator $categoryUrlPathGenerator
     */
    public function __construct(
        State $state,
        CategoryCollectionFactory $categoryCollectionFactory,
        CategoryUrlPathGenerator $categoryUrlPathGenerator
    ) {
        $this->state = $state;
        $this->categoryUrlPathGenerator = $categoryUrlPathGenerator;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('webpanda:category-path:regenerate')
            ->setDescription('Regenerate path for given categories')
            ->addArgument(
                'cids',
                InputArgument::IS_ARRAY,
                'Categories to regenerate'
            )
            ->addOption(
                'store',
                's',
                InputOption::VALUE_REQUIRED,
                'Use the specific Store View',
                Store::DEFAULT_STORE_ID
            )
        ;
        return parent::configure();
    }

    public function execute(InputInterface $inp, OutputInterface $out)
    {
        try {
            $this->state->getAreaCode();
        } catch ( \Magento\Framework\Exception\LocalizedException $e){
            $this->state->setAreaCode('adminhtml');
        }

        $storeId = $inp->getOption('store');

        $categories = $this->categoryCollectionFactory->create()
            ->setStore($storeId)
            ->addAttributeToSelect(['name', 'url_path', 'url_key']);

        $cIds = $inp->getArgument('cids');
        if(!empty($cIds)) {
            $categories->addAttributeToFilter('entity_id', ['in' => $cIds]);
        }

        $regenerated = 0;

        foreach($categories as $category)
        {
            $out->writeln('Regenerating urls for ' . $category->getName() . ' (' . $category->getId() . ')');

            $category->setStoreId($storeId);
            $resultUrlKey = $this->categoryUrlPathGenerator->getUrlKey($category);
            $this->updateUrlKey($category, $resultUrlKey);

            // do the reset for store 1 automatically if no store is specified
            if ($storeId == 0) {
                $category->setStoreId(1);
                $resultUrlKey = $this->categoryUrlPathGenerator->getUrlKey($category);
                $this->updateUrlKey($category, $resultUrlKey);
            }

            $regenerated++;
        }

        $out->writeln('Done regenerating. Regenerated url paths for ' . $regenerated . ' categories');
    }

    /**
     * Update Url Key
     *
     * @param Category $category
     * @param string $urlKey
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function updateUrlKey($category, $urlKey)
    {
        if (empty($urlKey)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid URL key'));
        }
        $category->setUrlKey($urlKey)
            ->setUrlPath($this->categoryUrlPathGenerator->getUrlPath($category));

        $category->getResource()->saveAttribute($category, 'url_key');
        $category->getResource()->saveAttribute($category, 'url_path');
    }
}
