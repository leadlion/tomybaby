<?php
namespace WebPanda\CatalogUrlRewrite\Console\Command;

use Magento\Framework\App\Area;
use Magento\Store\Model\App\Emulation;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Store\Model\Store;
use Magento\Framework\App\State;
use WebPanda\CatalogUrlRewrite\Helper\Data as HelperData;

/**
 * Class RegenerateCategoryUrlCommand
 * @package WebPanda\CatalogUrlRewrite\Console\Command
 */
class RegenerateCategoryUrlCommand extends Command
{
    /**
     * @var CategoryUrlRewriteGenerator
     */
    protected $categoryUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $collection;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * RegenerateCategoryUrlCommand constructor.
     * @param State $state
     * @param Collection $collection
     * @param CategoryUrlRewriteGenerator $categoryUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param Emulation $emulation
     */
    public function __construct(
        State $state,
        Collection $collection,
        CategoryUrlRewriteGenerator $categoryUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        CategoryCollectionFactory $categoryCollectionFactory,
        Emulation $emulation,
        HelperData $helper
    ) {
        parent::__construct();
        $this->state = $state;
        $this->collection = $collection;
        $this->categoryUrlRewriteGenerator = $categoryUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->emulation = $emulation;
        $this->helper = $helper;
    }

    protected function configure()
    {
        $this->setName('webpanda:category-url:regenerate')
            ->setDescription('Regenerate url for given categories')
            ->addArgument(
                'cids',
                InputArgument::IS_ARRAY,
                'Categories to regenerate'
            )
            ->addOption(
                'store', 's',
                InputOption::VALUE_REQUIRED,
                'Use the specific Store View',
                Store::DEFAULT_STORE_ID
            )
        ;
        return parent::configure();
    }

    public function execute(InputInterface $inp, OutputInterface $out)
    {
        try {
            $this->state->getAreaCode();
        } catch ( \Magento\Framework\Exception\LocalizedException $e){
            $this->state->setAreaCode('adminhtml');
        }

        $storeId = $inp->getOption('store');
        $this->emulation->startEnvironmentEmulation($storeId, Area::AREA_FRONTEND, true);

        $categories = $this->categoryCollectionFactory->create()
            ->setStore($storeId)
            ->addAttributeToSelect(['name', 'url_path', 'url_key']);

        $cIds = $inp->getArgument('cids');
        if(!empty($cIds)) {
            $categories->addAttributeToFilter('entity_id', ['in' => $cIds]);
        }

        $regenerated = 0;
        $saveRewrites = $this->helper->getConfigValue('save_rewrites_history', $storeId);

        foreach($categories as $category)
        {
            $out->writeln('Regenerating urls for ' . $category->getName() . ' (' . $category->getId() . ')');
            if (!$saveRewrites) {
                $this->urlPersist->deleteByData([
                    UrlRewrite::ENTITY_ID => $category->getId(),
                    UrlRewrite::ENTITY_TYPE => CategoryUrlRewriteGenerator::ENTITY_TYPE,
                    UrlRewrite::REDIRECT_TYPE => 0,
                    UrlRewrite::STORE_ID => $storeId
                ]);
            }

            $newUrls = $this->categoryUrlRewriteGenerator->generate($category);
            try {
                $newUrls = $this->filterEmptyRequestPaths($newUrls);
                $this->urlPersist->replace($newUrls);
                $regenerated += count($newUrls);
            } catch (\Exception $e) {
//                $out->writeln(sprintf('<error>Duplicated url for store ID %d, category %d (%s) - %s Generated URLs:' . PHP_EOL . '%s</error>' . PHP_EOL, $storeId, $category->getId(), $category->getName(), $e->getMessage(), implode(PHP_EOL, array_keys($newUrls))));
                $out->writeln('<error>' . $e->getMessage() . '</error>' . PHP_EOL);
            }
        }
        $this->emulation->stopEnvironmentEmulation();
        $out->writeln('Done regenerating. Regenerated ' . $regenerated . ' urls');
    }

    /**
     * Remove entries with request_path='' to prevent error 404 for "http://site.com/" address.
     *
     * @param \Magento\UrlRewrite\Service\V1\Data\UrlRewrite[] $newUrls
     * @return \Magento\UrlRewrite\Service\V1\Data\UrlRewrite[]
     */
    private function filterEmptyRequestPaths($newUrls)
    {
        $result = [];
        foreach ($newUrls as $key => $url) {
            $requestPath = $url->getRequestPath();
            if (!empty($requestPath)) {
                $result[$key] = $url;
            }
        }
        return $result;
    }
}
