<?php
namespace WebPanda\CatalogUrlRewrite\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Store\Model\Store;
use Magento\Framework\App\State;
use WebPanda\CatalogUrlRewrite\Helper\Data as HelperData;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ResourceModel\Product\Action as ProductAction;
use Magento\Framework\Model\ResourceModel\Iterator;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class RegenerateImageAltCommand
 * @package WebPanda\CatalogUrlRewrite\Console\Command
 */
class RegenerateImageAltCommand extends Command
{
    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * @var ProductAction
     */
    protected $action;

    /**
     * @var Iterator
     */
    protected $iterator;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    protected $progress;

    protected $output;

    protected $saveRewrites;

    protected $storeId;

    /**
     * RegenerateImageAltCommand constructor.
     * @param State $state
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $collectionFactory
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param HelperData $helper
     * @param ProductAction $action
     * @param Iterator $iterator
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        State $state,
        StoreRepositoryInterface $storeRepository,
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        HelperData $helper,
        ProductAction $action,
        Iterator $iterator,
        ProductRepositoryInterface $productRepository
    ) {
        $this->state = $state;
        $this->storeRepository = $storeRepository;
        $this->storeManager = $storeManager;
        $this->collectionFactory = $collectionFactory;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->helper = $helper;
        $this->action = $action;
        $this->iterator = $iterator;
        $this->productRepository = $productRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('webpanda:image-alt:regenerate')
            ->setDescription('Regenerate meta fields for given products')
            ->addArgument(
                'pids',
                InputArgument::IS_ARRAY,
                'Products to regenerate'
            )
            ->addOption(
                'store',
                's',
                InputOption::VALUE_REQUIRED,
                'Use the specific Store View',
                Store::DEFAULT_STORE_ID
            )
            ->addOption(
                'startPid',
                'spid',
                InputOption::VALUE_OPTIONAL,
                'Starting Product Id'
            )
            ->addOption(
                'endPid',
                'epid',
                InputOption::VALUE_OPTIONAL,
                'Ending Product Id'
            )
        ;
        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch ( \Magento\Framework\Exception\LocalizedException $e){
            $this->state->setAreaCode('adminhtml');
        }

        $this->output = $output;
        $this->output->setDecorated(true);
        $this->storeId = $input->getOption('store');
        // try to process data for store ID 1 by default
        // if store with ID 1 does not exists for some reason use store 0
        try {
            $storeObject = $this->storeManager->getStore($this->storeId);
        } catch (\Exception $e) {
            $this->storeId = 0;
        }

        $this->storeManager->setCurrentStore($this->storeId);

        $collection = $this->collectionFactory->create();
        $collection->addStoreFilter($this->storeId)->setStoreId($this->storeId);
        $collection->addAttributeToSelect(['name', 'store_id']);

        $startPid = $input->getOption('startPid');
        $endPid = $input->getOption('endPid');
        if ($startPid && $endPid) {
            $collection->addAttributeToFilter('entity_id', [
                'from' => $startPid,
                'to' => $endPid
            ]);
        }

        if ($this->helper->getConfigValue('product_url_key_auto_generate', $this->storeId)) {
            // add all url_key attributes to select
            $urlKey = $this->helper->getConfigValue('product_url_key_format', $this->storeId);
            $check = preg_match_all('/{{(.*?)}}/', $urlKey, $matches);
            if ($check) {
                $collection->addAttributeToSelect($matches[1]);
            }
        }
        $this->saveRewrites = $this->helper->getConfigValue('save_rewrites_history', $this->storeId);

        $pIds = $input->getArgument('pids');
        if (!empty($pIds)) {
            $collection->addIdFilter($pIds);
        }

        $this->progress = new \Symfony\Component\Console\Helper\ProgressBar($this->output, $collection->getSize());

        $this->iterator
            ->walk(
                $collection->getSelect(),
                array(array($this, 'callback'))
            );

        $this->output->writeln("");
        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

    public function callback($args)
    {
        $product = $this->productRepository->getById($args['row']['entity_id'], true, $this->storeId);
        $product->setStoreId($this->storeId);
        if ($gallery = $product->getMediaGallery()) {
            foreach ($gallery['images'] as $key => $entry) {
                $gallery['images'][$key]['label'] = $this->helper->getAltText($product);
            }
        }

        try {
            $product->setMediaGallery($gallery);
            $product->save();
        } catch (\Exception $e) {
//            $this->output->writeln('<error>' . __('Duplicated url for product with ID ') . $product->getId() .'</error>');
            $this->output->writeln('<error>' . $e->getMessage() . '</error>');
        }
        $this->progress->advance();
    }
}
