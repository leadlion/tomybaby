<?php
namespace WebPanda\CatalogUrlRewrite\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Store\Model\Store;
use Magento\Framework\App\State;
use WebPanda\CatalogUrlRewrite\Helper\Data as HelperData;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Model\ResourceModel\Iterator;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class RegenerateProductUrlRewrites
 * @package WebPanda\CatalogUrlRewrite\Console\Command
 */
class RegenerateProductUrlRewrites extends Command
{
    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * @var Iterator
     */
    protected $iterator;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    protected $progress;

    protected $output;

    protected $saveRewrites;
    
    protected $storeId;

    /**
     * RegenerateProductUrlRewrites constructor.
     * @param State $state
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $collectionFactory
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     * @param HelperData $helper
     * @param Iterator $iterator
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        State $state,
        StoreRepositoryInterface $storeRepository,
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        HelperData $helper,
        Iterator $iterator,
        ProductRepositoryInterface $productRepository
    ) {
        $this->state = $state;
        $this->storeRepository = $storeRepository;
        $this->storeManager = $storeManager;
        $this->collectionFactory = $collectionFactory;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        $this->helper = $helper;
        $this->iterator = $iterator;
        $this->productRepository = $productRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('webpanda:product-url:regenerate')
            ->setDescription('Regenerate url for given products')
            ->addArgument(
                'pids',
                InputArgument::IS_ARRAY,
                'Products to regenerate'
            )
            ->addOption(
                'store',
                's',
                InputOption::VALUE_REQUIRED,
                'Use the specific Store View',
                1
            )
            ->addOption(
                'startPid',
                'spid',
                InputOption::VALUE_OPTIONAL,
                'Starting Product Id'
            )
            ->addOption(
                'endPid',
                'epid',
                InputOption::VALUE_OPTIONAL,
                'Ending Product Id'
            )
        ;
        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch ( \Magento\Framework\Exception\LocalizedException $e){
            $this->state->setAreaCode('adminhtml');
        }

        $this->output = $output;
        $this->output->setDecorated(true);
        $this->storeId = $input->getOption('store');
        // try to process data for store ID 1 by default
        // if store with ID 1 does not exists for some reason use store 0
        try {
            $storeObject = $this->storeManager->getStore($this->storeId);
        } catch (\Exception $e) {
            $this->storeId = 0;
        }

        $this->storeManager->setCurrentStore($this->storeId);

        $collection = $this->collectionFactory->create();
        $collection->addStoreFilter($this->storeId)->setStoreId($this->storeId);
        $collection->addAttributeToSelect(['name', 'url_path', 'url_key', 'store_id']);

        $startPid = $input->getOption('startPid');
        $endPid = $input->getOption('endPid');
        if ($startPid && $endPid) {
            $collection->addAttributeToFilter('entity_id', [
                'from' => $startPid,
                'to' => $endPid
            ]);
        }

        if ($this->helper->getConfigValue('product_url_key_auto_generate', $this->storeId)) {
            // add all url_key attributes to select
            $urlKey = $this->helper->getConfigValue('product_url_key_format', $this->storeId);
            $check = preg_match_all('/{{(.*?)}}/', $urlKey, $matches);
            if ($check) {
                $collection->addAttributeToSelect($matches[1]);
            }
        }
        $this->saveRewrites = $this->helper->getConfigValue('save_rewrites_history', $this->storeId);

        $pIds = $input->getArgument('pids');
        if (!empty($pIds)) {
            $collection->addIdFilter($pIds);
        }

        $this->progress = new \Symfony\Component\Console\Helper\ProgressBar($this->output, $collection->getSize());

        // if tying to process over 20k products use the iterator
        // else process the collection directly because it's faster this way
        if ($collection->getSize() > 20000) {
            $this->iterator
                ->walk(
                    $collection->getSelect(),
                    array(array($this, 'callback'))
                );
        } else {
            $collection->setPageSize(false);
            foreach ($collection->getItems() as $product) {
                $product->setStoreId($this->storeId);
                $product->setData('save_rewrites_history', $this->saveRewrites);

                if (!$this->saveRewrites) {
                    $this->urlPersist->deleteByData([
                        UrlRewrite::ENTITY_ID => $product->getId(),
                        UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                        UrlRewrite::REDIRECT_TYPE => 0,
                        UrlRewrite::STORE_ID => $this->storeId
                    ]);
                }
                try {
                    $this->urlPersist->replace(
                        $this->productUrlRewriteGenerator->generate($product)
                    );
                } catch (\Exception $e) {
//                    $this->output->writeln('<error>' . __('Duplicated url for product with ID ') . $product->getId() .'</error>');
                    $this->output->writeln('<error>' . $e->getMessage() .'</error>');
                }
                $this->progress->advance();
            }
        }

        $this->output->writeln("");
        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

    public function callback($args)
    {
        $product = $this->productRepository->getById($args['row']['entity_id'], true, $this->storeId);
        $product->setStoreId($this->storeId);
        $product->setData('save_rewrites_history', $this->saveRewrites);

        if (!$this->saveRewrites) {
            $this->urlPersist->deleteByData([
                UrlRewrite::ENTITY_ID => $product->getId(),
                UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                UrlRewrite::REDIRECT_TYPE => 0,
                UrlRewrite::STORE_ID => $this->storeId
            ]);
        }
        try {
            $this->urlPersist->replace(
                $this->productUrlRewriteGenerator->generate($product)
            );
        } catch (\Exception $e) {
//            $this->output->writeln('<error>' . __('Duplicated url for product with ID ') . $product->getId() .'</error>');
            $this->output->writeln('<error>' . $e->getMessage() .'</error>');
        }
        $this->progress->advance();
    }
}
