<?php
namespace WebPanda\CatalogUrlRewrite\Controller\Adminhtml\Product;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Controller\Adminhtml\Product\Builder;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use WebPanda\CatalogUrlRewrite\Helper\Data as HelperData;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\UrlRewrite\Model\UrlPersistInterface;

/**
 * Class MassReset
 * @package WebPanda\CatalogUrlRewrite\Controller\Adminhtml\Product
 */
class MassReset extends \Magento\Catalog\Controller\Adminhtml\Product
{
    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * MassReset constructor.
     * @param Context $context
     * @param Builder $productBuilder
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     * @param HelperData $helper
     */
    public function __construct(
        Context $context,
        Builder $productBuilder,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        HelperData $helper
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        $this->helper = $helper;
        parent::__construct($context, $productBuilder);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $storeId = $this->getRequest()->getParam('store_id');

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $selectedItemsNr = $collection->getSize();

        $collection->addStoreFilter($storeId)->setStoreId($storeId);
        $collection->addAttributeToSelect(['name', 'url_path', 'url_key', 'store_id']);
        $collection->setPageSize(false);

        if ($this->helper->getConfigValue('product_url_key_auto_generate', $storeId)) {
            // add all url_key attributes to select
            $urlKey = $this->helper->getConfigValue('product_url_key_format', $storeId);
            $check = preg_match_all('/{{(.*?)}}/', $urlKey, $matches);
            if ($check) {
                $collection->addAttributeToSelect($matches[1]);
            }
        }
        $saveRewrites = $this->helper->getConfigValue('save_rewrites_history', $storeId);

        $counter = 0;
        foreach($collection->getItems() as $product) {
            $product->setStoreId($storeId);
            $product->setData('save_rewrites_history', $saveRewrites);

            if (!$saveRewrites) {
                $this->urlPersist->deleteByData([
                    UrlRewrite::ENTITY_ID => $product->getId(),
                    UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                    UrlRewrite::REDIRECT_TYPE => 0,
                    UrlRewrite::STORE_ID => $storeId
                ]);
            }
            try {
                $this->urlPersist->replace(
                    $this->productUrlRewriteGenerator->generate($product)
                );
                $counter++;
            } catch(\Exception $e) {
                $this->messageManager->addError(
                    __('Duplicated url for product with %1 and SKU %2', [$product->getId(), $product->getSku()])
                );
            }
        }

        if ($selectedItemsNr == $counter) {
            $this->messageManager->addSuccess(
                __('%1 products were selected, %2 products were reset', $selectedItemsNr, $counter)
            );
        } else {
            $this->messageManager->addSuccess(
                __('%1 products were selected, %2 products were reset(because only %2 of them were in the selected store)', $selectedItemsNr, $counter)
            );
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('catalog/*/index');
    }
}
