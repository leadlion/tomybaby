<?php
namespace WebPanda\CatalogUrlRewrite\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\CatalogImportExport\Model\Import\Product as ProductImport;
use Magento\Store\Model\Store;
use Magento\Catalog\Model\Product\Url as ProductUrl;
use Magento\Eav\Model\AttributeRepository;
use Magento\Catalog\Model\Product\Url as ProductUrlFormatter;

/***
 * Class Data
 * @package WebPanda\CatalogUrlRewrite\Helper
 */
class Data extends AbstractHelper
{
    private $baseConfig = 'catalog/seo';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ProductUrl
     */
    protected $productUrl;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * @var ProductUrlFormatter
     */
    protected $urlModel;

    /**
     * Data constructor.
     * @param Context $context
     * @param ProductUrlFormatter $productUrl
     * @param AttributeRepository $attributeRepository
     * @param ProductUrlFormatter $urlModel
     */
    public function __construct(
        Context $context,
        ProductUrl $productUrl,
        AttributeRepository $attributeRepository,
        ProductUrlFormatter $urlModel
    ) {
        parent::__construct($context);
        $this->scopeConfig = $context->getScopeConfig();
        $this->productUrl = $productUrl;
        $this->attributeRepository = $attributeRepository;
        $this->urlModel = $urlModel;
    }

    /**
     * Get config data based on product store id
     *
     * @param $configCode
     * @param $storeId
     * @return mixed
     */
    public function getConfigValue($configCode, $storeId)
    {
        $configCode = $this->baseConfig . '/' . $configCode;

        if ($storeId) {
            return $this->scopeConfig->getValue($configCode, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        }
        return $this->scopeConfig->getValue($configCode, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Generate url_key based on config data
     *
     * @param $product
     * @return mixed
     */
    public function getConfigUrlKey($product)
    {
        $urlKey = $this->getConfigValue('product_url_key_format', $product->getStoreId());

        $check = preg_match_all('/{{(.*?)}}/', $urlKey, $matches);
        if (!$check) {
            return $product->getUrlKey();
        }

        $attributeCodes = $matches[1];
        foreach ($attributeCodes as $code) {
            if (in_array($code, ['entity_id', 'id'])) {
                $value = $product->getId();
            } else {
                $attribute = $this->attributeRepository->get('catalog_product', $code);
                switch ($attribute->getFrontendInput()) {
                    case 'multiselect':
                    case 'image':
                    case 'boolean':
                    case 'wee':
                    case 'gallery':
                        $value = '';
                        break;
                    case 'select':
                        if ($product->getData($code)) {
                            $value = $attribute->getFrontend()->getValue($product);
                        } else {
                            $value = '';
                        }
                        break;
                    case 'price':
                    case 'weight':
                        $value = number_format($attribute->getFrontend()->getValue($product), 2);
                        break;
                    default:
                        $value = $product->getData($code);
                }
            }

            $urlKey = str_replace('{{' . $code . '}}', $value, $urlKey);
        }

        return $urlKey;
    }

    public function getAltText($product)
    {
        $urlKey = $this->getConfigValue('product_image_alt_format', $product->getStoreId());

        $check = preg_match_all('/{{(.*?)}}/', $urlKey, $matches);
        if (!$check) {
            return $product->getName();
        }

        $attributeCodes = $matches[1];
        foreach ($attributeCodes as $code) {
            if (in_array($code, ['entity_id', 'id'])) {
                $value = $product->getId();
            } else {
                $attribute = $this->attributeRepository->get('catalog_product', $code);
                switch ($attribute->getFrontendInput()) {
                    case 'multiselect':
                    case 'image':
                    case 'boolean':
                    case 'wee':
                    case 'gallery':
                        $value = '';
                        break;
                    case 'select':
                        if ($product->getData($code)) {
                            $value = $attribute->getFrontend()->getValue($product);
                        } else {
                            $value = '';
                        }
                        break;
                    case 'price':
                    case 'weight':
                        $value = number_format($attribute->getFrontend()->getValue($product), 2);
                        break;
                    default:
                        $value = $product->getData($code);
                }
            }

            $urlKey = str_replace('{{' . $code . '}}', $value, $urlKey);
        }

        return $urlKey;
    }

    /**
     * Generate url_key based on config data for product import
     *
     * @param $rowData
     * @return mixed
     */
    public function getConfigUrlKeyForImport($rowData)
    {
        $urlKey = $this->getConfigValue('product_url_key_format', Store::DEFAULT_STORE_ID);

        $check = preg_match_all('/{{(.*?)}}/', $urlKey, $matches);
        if (!$check) {
            if (!empty($rowData[ProductImport::URL_KEY])) {
                return strtolower($rowData[ProductImport::URL_KEY]);
            }

            if (!empty($rowData[ProductImport::COL_NAME])) {
                return $this->productUrl->formatUrlKey($rowData[ProductImport::COL_NAME]);
            }
        }

        $attributeCodes = $matches[1];
        foreach ($attributeCodes as $code) {
            if (isset($rowData[$code])) {
                $value = $rowData[$code];
            } else {
                $value = '';
            }


            $urlKey = str_replace('{{' . $code . '}}', $value, $urlKey);
        }

        return $this->urlModel->formatUrlKey($urlKey);
    }
}
