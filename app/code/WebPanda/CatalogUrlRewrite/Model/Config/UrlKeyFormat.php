<?php
namespace WebPanda\CatalogUrlRewrite\Model\Config;

use Magento\Eav\Model\AttributeRepository;

/**
 * Class UrlKeyFormat
 * @package WebPanda\CatalogUrlRewrite\Model\Config
 */
class UrlKeyFormat extends \Magento\Framework\App\Config\Value
{
    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * UrlKeyFormat constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param AttributeRepository $attributeRepository
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        AttributeRepository $attributeRepository,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->attributeRepository = $attributeRepository;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * Validate a base URL field value
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        $check = preg_match_all('/{{(.*?)}}/', $value, $matches);
        if (!$check) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid URL Key Format'));
        }

        try {
            $attributeCodes = $matches[1];
            foreach ($attributeCodes as $code) {
                if (in_array($code, ['entity_id', 'id'])) {
                    continue;
                } else {
                    $attribute = $this->attributeRepository->get('catalog_product', $code);
                }
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid URL Key Format. ' . $e->getMessage()), $e);
        }
    }
}
