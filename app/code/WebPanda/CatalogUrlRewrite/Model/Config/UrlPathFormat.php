<?php
namespace WebPanda\CatalogUrlRewrite\Model\Config;

/**
 * Class UrlPathFormat
 * @package WebPanda\CatalogUrlRewrite\Model\Config
 */
class UrlPathFormat extends \Magento\Framework\App\Config\Value
{
    /**
     * Validate a URL Path field value
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeSave()
    {
        $value = $this->getValue();

        if (strpos($value, '{{url_key}}') === false) {
            throw new \Magento\Framework\Exception\LocalizedException(__('URL Special Words needs to contain {{url_key}}'));
        }
    }
}
