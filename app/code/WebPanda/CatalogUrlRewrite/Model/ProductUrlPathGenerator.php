<?php
namespace WebPanda\CatalogUrlRewrite\Model;

/**
 * Class ProductUrlPathGenerator
 * @package WebPanda\CatalogUrlRewrite\Model
 */
class ProductUrlPathGenerator extends \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
{
    /**
     * @var \WebPanda\CatalogUrlRewrite\Helper\Data
     */
    protected $helper;

    /**
     * ProductUrlPathGenerator constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $categoryUrlPathGenerator
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \WebPanda\CatalogUrlRewrite\Helper\Data $helper
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $categoryUrlPathGenerator,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \WebPanda\CatalogUrlRewrite\Helper\Data $helper
    ) {
        parent::__construct($storeManager, $scopeConfig, $categoryUrlPathGenerator, $productRepository);
        $this->helper = $helper;
    }

    /**
     * Retrieve Product Url path (with category if exists)
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Catalog\Model\Category $category
     *
     * @return string
     */
    public function getUrlPath($product, $category = null)
    {
        // if auto generation is not enabled don't do anything
        if (!$this->helper->getConfigValue('product_url_key_auto_generate', $product->getStoreId())) {
            return parent::getUrlPath($product, $category);
        }

        $urlStoreId = ($product->hasData('special_store')) ? $product->getData('special_store') : $product->getStoreId();
        $urlPathFormat = $this->helper->getConfigValue('product_url_path_format', $urlStoreId);

        $path = $product->getUrlKey()
            ? $this->prepareProductUrlKey($product)
            : $this->prepareProductDefaultUrlKey($product);

        // only reformat path if the config path format contains url_key
        if (strpos($urlPathFormat, '{{url_key}}') !== false) {
            $path = str_replace('{{url_key}}', $path, $urlPathFormat);
        }

        return $category === null
            ? $path
            : $this->categoryUrlPathGenerator->getUrlPath($category) . '/' . $path;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    protected function prepareProductUrlKey(\Magento\Catalog\Model\Product $product)
    {
        // if auto generation is not enabled don't do anything
        if (!$this->helper->getConfigValue('product_url_key_auto_generate', $product->getStoreId())) {
            return parent::prepareProductUrlKey($product);
        }
        return $product->formatUrlKey($this->helper->getConfigUrlKey($product));
    }

    /**
     * Prepare URL Key with stored product data (fallback for "Use Default Value" logic)
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    protected function prepareProductDefaultUrlKey(\Magento\Catalog\Model\Product $product)
    {
        // if auto generation is not enabled don't do anything
        if (!$this->helper->getConfigValue('product_url_key_auto_generate', $product->getStoreId())) {
            return parent::prepareProductDefaultUrlKey($product);
        }
        $storedProduct = $this->productRepository->getById($product->getId());
        $storedUrlKey = $storedProduct->getUrlKey();
        return $storedUrlKey ?: $product->formatUrlKey($this->helper->getConfigUrlKey($product));
    }

    /**
     * Retrieve Product Url path with suffix
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param int $storeId
     * @param \Magento\Catalog\Model\Category $category
     * @return string
     */
    public function getUrlPathWithSuffix($product, $storeId, $category = null)
    {
        $product->setData('special_store', $storeId);
        return $this->getUrlPath($product, $category) . $this->getProductUrlSuffix($storeId);
    }
}
