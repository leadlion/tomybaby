<?php
namespace WebPanda\CatalogUrlRewrite\Plugin\Model;

/**
 * Class Product
 * @package WebPanda\CatalogUrlRewrite\Plugin\Model
 */
class Product
{
    /**
     * @var \WebPanda\CatalogUrlRewrite\Helper\Data
     */
    protected $helper;

    /**
     * Product constructor.
     * @param \WebPanda\CatalogUrlRewrite\Helper\Data $helper
     */
    public function __construct(\WebPanda\CatalogUrlRewrite\Helper\Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param $product
     */
    public function beforeSave($product)
    {
        // only generate images alt text automatically if the config option is set
        if (
            $this->helper->getConfigValue('product_image_alt_auto_generate', $product->getStoreId()) &&
            $gallery = $product->getMediaGallery()
        ) {
            foreach ($gallery['images'] as $key => $entry) {
                $gallery['images'][$key]['label'] = $this->helper->getAltText($product);
            }
            $product->setMediaGallery($gallery);
        }
    }
}
