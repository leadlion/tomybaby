<?php
namespace WebPanda\CatalogUrlRewrite\Ui\Component\Catalog\Product\MassAction;

use Magento\Framework\UrlInterface;
use Zend\Stdlib\JsonSerializable;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class UrlRewrites
 * @package WebPanda\CatalogUrlRewrite\Ui\Component\Catalog\Product\MassAction
 */
class UrlRewrites implements JsonSerializable
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Additional options params
     *
     * @var array
     */
    protected $data;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Base URL for subactions
     *
     * @var string
     */
    protected $urlPath;

    /**
     * Param name for subactions
     *
     * @var string
     */
    protected $paramName;

    /**
     * Additional params for subactions
     *
     * @var array
     */
    protected $additionalData = [];

    /**
     * UrlRewrites constructor.
     * @param UrlInterface $urlBuilder
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        UrlInterface $urlBuilder,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->data = $data;
        $this->urlBuilder = $urlBuilder;
        $this->storeManager = $storeManager;
    }

    /**
     * @return array|null
     */
    public function jsonSerialize()
    {
        if ($this->options === null) {
            $stores = $this->storeManager->getStores();
            if(!count($stores)){
                return $this->options;
            }

            $options = [];
            $i = 0;
            foreach ($stores as $key => $store) {
                $options[$i]['value'] = $store->getId();
                $options[$i]['label'] = $store->getName() . ' (code: ' . $store->getCode() . ')';
                $i++;
            }
            $this->prepareData();
            foreach ($options as $option) {
                $this->options[$option['value']] = [
                    'type' => 'url_rewrite_' . $option['value'],
                    'label' => $option['label'],
                ];

                if ($this->urlPath && $this->paramName) {
                    $this->options[$option['value']]['url'] = $this->urlBuilder->getUrl(
                        $this->urlPath,
                        [$this->paramName => $option['value']]
                    );
                }

                $this->options[$option['value']] = array_merge_recursive(
                    $this->options[$option['value']],
                    $this->additionalData
                );
            }

            // return the massaction data
            $this->options = array_values($this->options);
        }

        return $this->options;
    }

    /**
     * Prepare addition data for subactions
     *
     * @return void
     */
    protected function prepareData()
    {
        foreach ($this->data as $key => $value) {
            switch ($key) {
                case 'urlPath':
                    $this->urlPath = $value;
                    break;
                case 'paramName':
                    $this->paramName = $value;
                    break;
                default:
                    $this->additionalData[$key] = $value;
                    break;
            }
        }
    }
}
