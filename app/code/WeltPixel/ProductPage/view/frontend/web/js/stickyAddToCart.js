define([
	'jquery'
], function ($) {
	"use strict";

	var stickyAddToCart = {
        options: {
            mobileTreshold: '768',
            isOnDesktopEnabled: '1',
            isOnMobileEnabled: '1',
            isStickyScrollUpEnabled: '0',
        },

		init: function (mobileTreshold, isOnDesktopEnabled, isOnMobileEnabled, isStickyScrollUpEnabled) {
            this.options.mobileTreshold = mobileTreshold;
            this.options.isOnDesktopEnabled = isOnDesktopEnabled;
            this.options.isOnMobileEnabled = isOnMobileEnabled;
            this.options.isStickyScrollUpEnabled = isStickyScrollUpEnabled;

            if ($('.price-configured_price').length) {
                $('.fixed-cart-container .wp-st-product-price').html($('.price-configured_price').html());
            } else {
                $('.fixed-cart-container .wp-st-product-price').html($('.product-info-main .product-info-price .price-box').html());
            }

            if (isOnMobileEnabled == '1') {
                $('.btt-button').css("bottom", parseInt($('.btt-button').css("bottom")) + 60 + "px")
            }

            $('.wp-st-addtocart-container > a').bind('click', function () {
                $("#product-addtocart-button").trigger('click');
            });

            var that = this;
            $(window).scroll(function (event) {
                var screenWidth = $(window).width();
                if ($('.price-configured_price').length) {
                    $('.fixed-cart-container .wp-st-product-price').html($('.price-configured_price').html());
                } else {
                    $('.fixed-cart-container .wp-st-product-price').html($('.product-info-main .product-info-price .price-box').html());
                }
                if ((isOnDesktopEnabled == "1") && (screenWidth >= mobileTreshold)) {
                    that.makeStickyCart();
                } else if (isOnMobileEnabled == "1")  {
                    that.makeStickyCart();
                } else {
                    $('.fixed-cart-container').hide();
                }

            });
		},


        makeStickyCart: function() {
            let element = $('#product-addtocart-button'),
                fixedCartCotainer =  $('.fixed-cart-container');

            if ($('#bundleSummary') && $('#bundleSummary').is(":visible")) {
                element = $('#bundleSummary');
            }
            var containerShowLimit = 600;
            if (element.length) {
                containerShowLimit = element.position().top;
            }
            var sc = $(window).scrollTop();

            if (sc > containerShowLimit) {
                fixedCartCotainer.addClass("sticky-slide-up-desktop sticky-slide-up-mobile");
                if (fixedCartCotainer.is(':visible')) {$('.page-header').addClass('sticky-header-fade-out')}
            } else {
                fixedCartCotainer.removeClass("sticky-slide-up-desktop sticky-slide-up-mobile");
                $('.page-header').removeClass("sticky-header-fade-out");
            }
        },
        lastScrollPosition: 0
	};

	return stickyAddToCart;
});
