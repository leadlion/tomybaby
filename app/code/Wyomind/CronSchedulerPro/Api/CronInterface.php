<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Api;

/**
 * Cron Scheduler Pro API
 */
interface CronInterface
{
    /**
     * Run a job
     * @description see CronSchedulerPro-webapi-sample.php
     * @param string $jobCode
     * @return string
     */
    public function run($jobCode);
}