<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Block\Adminhtml\Job;

/**
 * Job form container
 * @version 1.0.0
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Container initialization
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Wyomind_CronSchedulerPro';
        $this->_controller = 'adminhtml_job';

        parent::_construct();

        $this->removeButton('save');
        $this->removeButton('reset');
        $this->removeButton('back');

        $this->addButton('save', [
            'label' => __('Save'),
            'class' => 'save primary',
            'onclick' => "javascript:void(require(['csp_job_edit'], function (edit) { edit.save('" . $this->getUrl(\Wyomind\CronSchedulerPro\Helper\Url::JOB_SAVE) . "'); }))",
        ]);

        $this->addButton('back', [
            'label' => __('Back'),
            'class' => 'back',
            'onclick' => "javascript:void(document.location.href='" . $this->getUrl(\Wyomind\CronSchedulerPro\Helper\Url::JOB_LISTING) . "');",
        ]);
    }
}