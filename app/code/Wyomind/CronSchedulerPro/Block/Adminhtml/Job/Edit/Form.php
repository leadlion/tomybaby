<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Block\Adminhtml\Job\Edit;

/**
 * Job form
 * @version 1.0.0
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind, \Magento\Backend\Block\Template\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Data\FormFactory $formFactory, array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $registry, $formFactory, $data);
    }
    /**
     * Prepare the job form
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]);
        $form->setUseContainer(true);
        $form->setHtmlIdPrefix('');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Settings')]);
        $fieldset->addField('code', 'text', ['name' => 'code', 'label' => __('Code'), 'title' => __('Code'), 'required' => true, 'note' => __("Unique code identifying the cron job") . "<br/>" . __("Example: <i>cronscheduler_heartbeat</i>")]);
        $configJobs = $this->_cronConfig->getJobs();
        $groups = [];
        foreach (array_keys($configJobs) as $group) {
            if (!in_array($group, $groups)) {
                $groups[] = ['label' => $group, 'value' => $group];
            }
        }
        $fieldset->addField('group', 'select', ['name' => 'group', 'label' => __('Group'), 'title' => __('Group'), 'required' => true, 'values' => $groups, 'note' => __("Cron group for processing the cron job")]);
        $fieldset->addField('instance', 'text', ['name' => 'instance', 'label' => __('Instance'), 'title' => __('Instance'), 'required' => true, 'note' => __("Class instance to call (must be a valid class instance)") . "<br/>" . __("Example: <i>Wyomind\\CronScheduler\\Cron\\HeartBeat</i>")]);
        $fieldset->addField('method', 'text', ['name' => 'method', 'label' => __('Method'), 'title' => __('Method'), 'required' => true, 'note' => __("Method from the instance to call (must exist in the instance class)") . "<br/>" . __("Example: <i>heartbeat</i>")]);
        $fieldset->addField('schedule', 'text', ['name' => 'schedule', 'label' => __('Schedule'), 'title' => __('Schedule'), 'required' => true, 'note' => __("Schedule for the cron job") . "<pre style='line-height: 13px;'>* * * * * *
│ │ │ │ │ │
│ │ │ │ │ └── " . __("year [optional]") . "
│ │ │ │ └──── " . __("day of week (0 - 7) (Sunday=0 or 7)") . "
│ │ │ └────── " . __("month (1 - 12)") . "
│ │ └──────── " . __("day of month (1 - 31)") . "
│ └────────── " . __("hour (0 - 23)") . "
└──────────── " . __("min (0 - 59)") . "
</pre><a target='_blank' href='" . __("https://en.wikipedia.org/wiki/Cron#CRON_expression") . "'>" . __("More details") . "</a>"]);
        $fieldset->addField('status', 'select', ['name' => 'status', 'label' => __('Status'), 'title' => __('Status'), 'required' => true, 'values' => [['value' => 1, 'label' => __('Enabled')], ['value' => 0, 'label' => __('Disabled')]]]);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}