<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Block\Adminhtml\Job\Listing;

/**
 * Toolbar block for the view System > Cron Scheduler > Jobs List (button : Generate Schedule)
 * @version 1.0.0
 *
 */
class Actions extends \Magento\Backend\Block\Template
{
    /**
     * @var \Magento\Framework\Authorization
     */
    protected $_authorization = null;
    /**
     * @var string
     */
    protected $_aclResource = "edit_job";
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind, \Magento\Backend\Block\Template\Context $context, array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $data);
        $this->_authorization = $context->getAuthorization();
        $this->setTemplate('job/listing/actions.phtml');
    }
    /**
     * Is the run tasks and schedule action allowed
     * @return boolean
     */
    public function isAllowed()
    {
        return $this->_authorization->isAllowed("Wyomind_CronSchedulerPro::" . $this->_aclResource);
    }
    /**
     * Get the url to generate schedule
     * @return string the url
     */
    public function getCreateJobUrl()
    {
        return $this->getUrl(\Wyomind\CronSchedulerPro\Helper\Url::JOB_EDIT);
    }
}