<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Block\Adminhtml\Task;

/**
 * Auto open modal window in the task listing view
 * @version 1.0.0
 */
class AutoOpenView extends \Magento\Backend\Block\Template
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    public $urlBuilder = null;
    /**
     * Class constructor
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(\Magento\Backend\Block\Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->setTemplate('task/autoOpenView.phtml');
        $this->urlBuilder = $context->getUrlBuilder();
    }
    /**
     * Do we need to automatically open the modal window
     * @return boolean
     */
    public function shouldOpen()
    {
        $params = $this->getRequest()->getParams();
        return isset($params['schedule_id']);
    }
    /**
     * Get the task id to display
     * @return int
     */
    public function getScheduleId()
    {
        $params = $this->getRequest()->getParams();
        return $params['schedule_id'];
    }
    /**
     * Get the task view backend url
     * @return string
     */
    public function getTaskViewUrl()
    {
        return $this->urlBuilder->getUrl(\Wyomind\CronScheduler\Helper\Url::TASK_VIEW);
    }
}