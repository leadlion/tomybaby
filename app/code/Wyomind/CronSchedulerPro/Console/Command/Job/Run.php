<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Console\Command\Job;

/**
 * wyomind:cronscheduler:run command line
 * @version 1.0.0
 * @description <pre>
 * $ bin/magento help wyomind:cronscheduler:run
 * Usage:
 * wyomind:cronscheduler:run job_code
 *
 * Arguments:
 *  job_code              The code of the job

 * Options:
 *  --help (-h)           Display this help message
 *  --quiet (-q)          Do not output any message
 *  --verbose (-v|vv|vvv) Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
 *  --version (-V)        Display this application version
 *  --ansi                Force ANSI output
 *  --no-ansi             Disable ANSI output
 *  --no-interaction (-n) Do not ask any interactive question
 * </pre>
 */
class Run extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Magento\Cron\Model\ConfigFactory
     */
    protected $_cronConfigFactory = null;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Helper\Job
     */
    protected $_jobHelperFactory = null;

    /**
     * Command line argument : code of the job to run
     */
    const JOB_CODE_ARG = "job_code";

    /**
     * Class constructor
     * @param \Magento\Cron\Model\ConfigFactory $cronConfigFactory
     * @param \Magento\Framework\App\State $state
     * @param \Wyomind\CronSchedulerPro\Helper\JobFactory $jobHelperFactory
     */
    public function __construct(
        \Magento\Cron\Model\ConfigFactory $cronConfigFactory,
        \Magento\Framework\App\State $state,
        \Wyomind\CronSchedulerPro\Helper\JobFactory $jobHelperFactory
    )
    {
        $this->_state = $state;
        $this->_cronConfigFactory = $cronConfigFactory;
        $this->_jobHelperFactory = $jobHelperFactory;
        parent::__construct();
    }

    /**
     * Configure the command line
     */
    protected function configure()
    {
        $this->setName('wyomind:cronscheduler:run')
                ->setDescription(__('Cron Scheduler : run a job'))
                ->setDefinition([
                    new \Symfony\Component\Console\Input\InputArgument(
                            self::JOB_CODE_ARG, \Symfony\Component\Console\Input\InputArgument::REQUIRED, __('The code of the job')
                    )
        ]);
        parent::configure();
    }

    /**
     * Execute the command line
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int \Magento\Framework\Console\Cli::RETURN_FAILURE or \Magento\Framework\Console\Cli::RETURN_SUCCESS
     */
    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    )
    {
        try {
            try {
                $this->_state->setAreaCode('adminhtml');
            } catch (\Exception $e) {

            }
            $jobCode = $input->getArgument(self::JOB_CODE_ARG);

            $jobsConfig = $this->_cronConfigFactory->create()->getJobs();
            $groupId = "";
            foreach (array_keys($jobsConfig) as $group) {
                if (isset($jobsConfig[$group][$jobCode])) {
                    $groupId = $group;
                }
            }

            if (isset($jobsConfig[$groupId][$jobCode])) {
                $jobConfig = $jobsConfig[$groupId][$jobCode];
                $result = $this->_jobHelperFactory->create()->runNow($jobConfig);

                if ($result["error"]) {
                    $output->writeln(sprintf("A error occurred when running the job %s:\n%s", $jobCode, $result["message"]));
                } else {
                    $output->writeln(sprintf("The job %s has been executed", $jobCode));
                }
            } else {
                $output->writeln(sprintf("No configuration found for the job %s", $jobCode));
            }
            
            $returnValue = \Magento\Framework\Console\Cli::RETURN_SUCCESS;
            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $output->writeln($e->getMessage());
            $returnValue = \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }

        return $returnValue;
    }
}