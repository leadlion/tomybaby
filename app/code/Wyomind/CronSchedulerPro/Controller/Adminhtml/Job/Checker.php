<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Check action for the job attributes values when creating/modifying a job
 * @version 1.0.0
 */
class Checker extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Helper\Checker
     */
    public $jobHelper = null;

    /**
     * Class constructor
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Wyomind\CronSchedulerPro\Helper\Checker $checkerHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Wyomind\CronSchedulerPro\Helper\Checker $checkerHelper
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->checkerHelper = $checkerHelper;
        parent::__construct($context);
    }

    /**
     * Execute a check
     * @return type
     */
    public function execute()
    {
        $result = ["error" => false, "messages" => []];
        $params = $this->getRequest()->getParams();
        if (isset($params['methods'])) {
            if (isset($params['value'])) {
                if (is_array($params['methods'])) {
                    foreach ($params['methods'] as $method) {
                        if (\method_exists($this->checkerHelper, $method)) {
                            $tmpResult = \call_user_func(array(&$this->checkerHelper, $method), $params['value']);
                            if ($tmpResult['error'] === true) {
                                $result['error'] = true;
                                if (isset($tmpResult['message'])) {
                                    $result['messages'][] = $tmpResult['message'];
                                }
                            }
                        } else {
                            $result = ["error" => true, "messages" => [__("Checker %1 doesn't exist", $method)]];
                        }
                    }
                } else {
                    if (\method_exists($this, $params['methods'])) {
                        $result = \call_user_func(array(&$this->checkerHelper, $params['methods']), $params['value']);
                    } else {
                        $result = ["error" => true, "messages" => [__("Checker %1 doesn't exist", $params['methods'])]];
                    }
                }
            } else {
                $result = ["error" => true, "messages" => [__("No value to check provided")]];
            }
        } else {
            $result = ["error" => true, "messages" => [__("No method name provided")]];
        }
        $result["messages"] = implode("<br/>", $result["messages"]);
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData($result);
    }
}