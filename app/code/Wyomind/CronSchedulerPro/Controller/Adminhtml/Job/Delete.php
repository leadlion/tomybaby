<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Delete a job action
 * @version 1.0.0
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory
     */
    public $jobCollectionFactory = null;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    public $resultRedirectFactory = null;

    /**
     * Class constructor
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
    )
    {
        parent::__construct($context);
        $this->jobCollectionFactory = $jobCollectionFactory;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
    }

    /**
     * Execute the delete action
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $code = $this->getRequest()->getParam('code');

        try {
            $jobCollection = $this->jobCollectionFactory->create();
            $job = $jobCollection->getJobByCode($code);
            if ($job) {
                $job->delete();
                $this->messageManager->addSuccess(__("The job <b>%1</b> has been deleted", $code));
            } else {
                $this->messageManager->addError(__("The job <b>%1</b> doesn't exist", $code));
            }
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        return $this->resultRedirectFactory->create()->setPath(\Wyomind\CronScheduler\Helper\Url::JOB_LISTING);
    }
}