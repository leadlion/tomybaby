<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Inline edit of a job configuration
 * @version 1.0.0
 */
class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Model\JobFactory
     */
    public $jobModelFactory = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory
     */
    public $jobCollectionFactory = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Helper\Checker
     */
    public $checkerHelper = null;

    /**
     * InlineEdit class constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Wyomind\CronSchedulerPro\Model\JobFactory $jobModelFactory
     * @param \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
     * @param \Wyomind\CronSchedulerPro\Helper\Checker $checkerHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Wyomind\CronSchedulerPro\Model\JobFactory $jobModelFactory,
        \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory,
        \Wyomind\CronSchedulerPro\Helper\Checker $checkerHelper
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->jobModelFactory = $jobModelFactory;
        $this->jobCollectionFactory = $jobCollectionFactory;
        $this->checkerHelper = $checkerHelper;
        parent::__construct($context);
    }

    /**
     * Execute the edition of the job configuration
     * @return string
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }
        $codes = array_keys($postItems);

        $jobs = $this->jobCollectionFactory->create();
        $jobs->addFieldtoFilter('code', ['in' => $codes]);

        $jobsArray = [];

        if (!count($jobs)) {
            foreach ($codes as $code) {
                $job = $this->jobModelFactory->create()->load(0);
                $job->setCode($code);
                $jobsArray[] = $job;
            }
        }

        foreach ($jobs as $job) {
            $jobsArray[] = $job;
        }

        $messages = [];
        foreach ($jobsArray as $job) {

            $data = $postItems[$job->getCode()];

            // checker validity
            $check = $this->checkerHelper->isAlphaNumAndBackslash($data['instance']);
            if ($check['error']) {
                $messages[] = $check['message'];
            }
            $check = $this->checkerHelper->classExists($data['instance']);
            if ($check['error']) {
                $messages[] = $check['message'];
            } else {
                $check = $this->checkerHelper->methodExists($data['instance'] . "#" . $data['method']);
                if ($check['error']) {
                    $messages[] = $check['message'];
                }
                $check = $this->checkerHelper->isAlphaNumAndBackslashAndHash($data['instance'] . "#" . $data['method']);
                if ($check['error']) {
                    $messages[] = $check['message'];
                }
            }
            $check = $this->checkerHelper->isScheduleValid($data['schedule']);

            if ($check['error']) {
                $messages[] = $check['message'];
            }

            if (!count($messages)) {
                $job->addData($data);

                try {
                    $job->save();
                } catch (\Exception $e) {
                    $messages[] = $e->getMessage();
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => count($messages)
        ]);
    }
}