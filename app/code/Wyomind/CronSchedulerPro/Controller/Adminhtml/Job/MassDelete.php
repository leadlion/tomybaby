<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Mass deletion of job's configurations
 * @version 1.0.0
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    public $resultRedirectFactory = null;

    /**
     * @var \Wyomind\CronScheduler\Helper\Job
     */
    public $jobHelper = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory
     */
    public $jobCollectionFactory = null;

    /**
     * @var \Magento\Framework\Authorization
     */
    public $authorization = null;

    /**
     * @var string
     */
    protected $_aclResource = "edit_job";

    /**
     * Class constructor
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
     * @param \Wyomind\CronScheduler\Helper\Job $jobHelper
     * @param \Magento\Framework\Authorization $authorization
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory,
        \Wyomind\CronScheduler\Helper\Job $jobHelper,
        \Magento\Framework\Authorization $authorization
    )
    {
        parent::__construct($context);
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->jobCollectionFactory = $jobCollectionFactory;
        $this->jobHelper = $jobHelper;
        $this->authorization = $authorization;
    }

    /**
     * Execute the mass deletion action
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if (!$this->authorization->isAllowed('Wyomind_CronSchedulerPro::' . $this->_aclResource)) {
            $this->messageManager->addError(__("You are not allowed to delete a cron job"));
        } else {
            $data = array_values($this->jobHelper->getJobData());

            $params = $this->getRequest()->getParams();

            if (isset($params['selected'])) {

                $codes = $params['selected'];

                $data = array_filter($data, function($item) use ($codes) {
                    return in_array($item['code'], $codes);
                });
            } else if (isset($params['excluded'])) {
                $filters = $params['filters'];

                foreach ($filters as $column => $value) {
                    if ($column != "placeholder") {
                        $data = array_filter($data, function($item) use ($column, $value) {
                            return stripos($item[$column], $value) !== false;
                        });
                    }
                }
            }

            $success = [];
            $errors = [];

            foreach ($data as $job) {
                if ($job['isSystem'] == 1) {
                    $errors[] = $job['code'];
                } else {
                    try {
                        $jobCollection = $this->jobCollectionFactory->create();
                        $job = $jobCollection->getJobByCode($job['code']);
                        if ($job) {
                            $job->delete();
                            $success[] = $job['code'];
                        } else {
                            $this->messageManager->addError(__("The job <b>%1</b> doesn't exist", $job['code']));
                        }
                    } catch (\Exception $e) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            }

            if (count($success) == 1) {
                $this->messageManager->addSuccess(__("The job <b>%1</b> has been deleted", $success[0]));
            } elseif (!empty($success)) {
                $this->messageManager->addSuccess(__("These jobs have been deleted: <br/><b>%1</b>", implode("</b><br/><b>", $success)));
            }
            if (count($errors) == 1) {
                $this->messageManager->addError(__("The job <b>%1</b> cannot be deleted", $errors[0]));
            } elseif (!empty($errors)) {
                $this->messageManager->addError(__("These jobs cannot be deleted: <br/><b>%1</b>", implode("</b><br/><b>", $errors)));
            }
        }

        return $this->resultRedirectFactory->create()->setPath(\Wyomind\CronScheduler\Helper\Url::JOB_LISTING);
    }
}