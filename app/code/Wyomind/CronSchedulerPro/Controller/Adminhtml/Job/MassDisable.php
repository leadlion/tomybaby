<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Mass disabling action
 * @version 1.0.0
 */
class MassDisable extends MassEnable
{
    /**
     * @var string
     */
    protected $_successMessageSingle = "The job <b>%1</b> has been disabled";

    /**
     * @var string
     */
    protected $_successMessageMulti = "These jobs have been disabled: <br/><b>%1</b>";

    /**
     * @var int
     */
    protected $_newStatus = 0;
}