<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Mass job enabling action
 * @version 1.0.0
 */
class MassEnable extends \Magento\Backend\App\Action
{
    /**
     * @var string
     */
    protected $_successMessageSingle = "The job <b>%1</b> has been enabled";

    /**
     * @var string
     */
    protected $_successMessageMulti = "These jobs have been enabled: <br/><b>%1</b>";

    /**
     * @var int
     */
    protected $_newStatus = 1;

    /**
     * @var \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory
     */
    public $jobCollectionFactory = null;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    public $resultRedirectFactory = null;

    /**
     * @var \Wyomind\CronScheduler\Helper\Job
     */
    public $jobHelper = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Model\JobFactory
     */
    public $jobModelFactory = null;

    /**
     * @var \Magento\Framework\Authorization
     */
    public $authorization = null;

    /**
     * @var string
     */
    protected $_aclResource = "edit_job";

    /**
     * Class constructor
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
     * @param \Wyomind\CronScheduler\Helper\Job $jobHelper
     * @param \Wyomind\CronSchedulerPro\Model\JobFactory $jobModelFactory
     * @param \Magento\Framework\Authorization $authorization
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory,
        \Wyomind\CronScheduler\Helper\Job $jobHelper,
        \Wyomind\CronSchedulerPro\Model\JobFactory $jobModelFactory,
        \Magento\Framework\Authorization $authorization
    )
    {
        parent::__construct($context);
        $this->jobCollectionFactory = $jobCollectionFactory;
        $this->jobModelFactory = $jobModelFactory;
        $this->jobHelper = $jobHelper;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->authorization = $authorization;
    }

    /**
     * Execute the mass job enable action
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if (!$this->authorization->isAllowed('Wyomind_CronSchedulerPro::' . $this->_aclResource)) {
            $this->messageManager->addError(__("You are not allowed to edit cron jobs"));
        } else {
            $data = array_values($this->jobHelper->getJobData());

            $params = $this->getRequest()->getParams();

            if (isset($params['selected'])) {
                $codes = $params['selected'];

                $data = array_filter($data, function($item) use ($codes) {
                    return in_array($item['code'], $codes);
                });
            } else if (isset($params['excluded'])) {
                $filters = $params['filters'];

                foreach ($filters as $column => $value) {
                    if ($column != "placeholder") {
                        $data = array_filter($data, function($item) use ($column, $value) {
                            return stripos($item[$column], $value) !== false;
                        });
                    }
                }
            }

            $codes = [];

            foreach ($data as $job => $info) {
                $info['status'] = $this->_newStatus;
                $codes[$info['code']] = $info;
            }

            $jobs = $this->jobCollectionFactory->create();
            $jobs->addFieldtoFilter('code', ['in' => array_keys($codes)]);

            $jobsArray = [];

            if (!count($jobs)) {
                foreach (array_keys($codes) as $code) {
                    $job = $this->jobModelFactory->create()->load(0);
                    $job->setCode($code);
                    $jobsArray[] = $job;
                }
            }

            foreach ($jobs as $job) {
                $jobsArray[] = $job;
            }

            $errors = [];
            $success = [];
            foreach ($jobsArray as $job) {
                $job->addData($codes[$job->getCode()]);
                try {
                    $job->save();
                    $success[] = $job->getCode();
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }
            }

            if (count($errors)) {
                $this->messageManager->addError(__("Error: %1", implode(',', $errors)));
            } else {
                if (count($success) == 1) {
                    $this->messageManager->addSuccess(__($this->_successMessageSingle, $success[0]));
                } elseif (!empty($success)) {
                    $this->messageManager->addSuccess(__($this->_successMessageMulti, implode("</b><br/><b>", $success)));
                }
            }
        }

        return $this->resultRedirectFactory->create()->setPath(\Wyomind\CronScheduler\Helper\Url::JOB_LISTING);
    }
}