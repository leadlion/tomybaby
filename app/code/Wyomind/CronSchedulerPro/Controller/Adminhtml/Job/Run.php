<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Run a job action
 * @version 1.0.0
 */
class Run extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Cron\Model\ConfigInterface
     */
    public $cronConfig = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Helper\Job
     */
    public $jobHelper = null;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    public $resultRedirectFactory = null;

    /**
     * Class constructor
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Cron\Model\ConfigInterface $cronConfig
     * @param \Wyomind\CronSchedulerPro\Helper\Job $jobHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Cron\Model\ConfigInterface $cronConfig,
        \Wyomind\CronSchedulerPro\Helper\Job $jobHelper
    )
    {
        parent::__construct($context);
        $this->cronConfig = $cronConfig;
        $this->jobHelper = $jobHelper;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
    }

    /**
     * "Run a job" action execution
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $jobConfig = $this->cronConfig->getJobs()[$params['group']][$params['code']];
        $result = $this->jobHelper->runNow($jobConfig);

        if ($result["error"]) {
            $this->messageManager->addError(__("A error occurred when running the job <b>%1</b>: %2", $params['code'], $result["message"]));
        } else {
            $this->messageManager->addSuccess(__("The job <b>%1</b> has been executed", $params['code']));
        }

        return $this->resultRedirectFactory->create()->setPath(\Wyomind\CronScheduler\Helper\Url::JOB_LISTING);
    }
}