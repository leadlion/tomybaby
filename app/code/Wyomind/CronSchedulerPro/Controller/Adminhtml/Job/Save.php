<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Save a new job action
 * @version 1.0.0
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Wyomind\CronSchedulerPro\Model\JobFactory
     */
    public $jobModelFactory = null;
    
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    public $resultRedirectFactory = null;

    /**
     * Class constructor
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Wyomind\CronSchedulerPro\Model\JobFactory $jobModelFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Wyomind\CronSchedulerPro\Model\JobFactory $jobModelFactory
    )
    {
        $this->jobModelFactory = $jobModelFactory;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        parent::__construct($context);
    }

    /**
     * Save a job execution
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if (isset($data['key'])) {
            unset($data['key']);
        }
        if (isset($data['form_key'])) {
            unset($data['form_key']);
        }

        $job = $this->jobModelFactory->create();
        $job->load(0);

        foreach ($data as $index => $value) {
            $job->setData($index, $value);
        }

        try {
            $job->save();
            $this->messageManager->addSuccess(__('The job has been created'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Unable to create the job') . '<br/><br/>' . $e->getMessage());
        }

        return $this->resultRedirectFactory->create()->setPath(\Wyomind\CronScheduler\Helper\Url::JOB_LISTING);
    }
}