<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Controller\Adminhtml\Job;

/**
 * Inline edit of a job configuration (validation before saving)
 * @version 1.0.0
 * 
 
 */
class ValidateEdit extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Model\JobFactory
     */
    public $jobModelFactory = null;

    /**
     * @var \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory
     */
    public $jobCollectionFactory = null;

    /**
     * @var \Wyomind\CronScheduler\Helper\Job
     */
    public $jobHelper = null;

    /**
     * @var \Magento\Framework\Authorization
     */
    public $authorization = null;

    /**
     * @var string
     */
    protected $_aclResource = "edit_job";

    /**
     * @var \Wyomind\Framework\Helper\Module
     */
    public $framework = null;

    /**
     * @var string
     */
    

    /**
     * Class constructor
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Wyomind\CronScheduler\Helper\Job $jobHelper
     * @param \Magento\Framework\Authorization $authorization
     * @param \Wyomind\Framework\Helper\Module $framework
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Wyomind\CronScheduler\Helper\Job $jobHelper,
        \Magento\Framework\Authorization $authorization,
        \Wyomind\Framework\Helper\Module $framework
    )
    {

        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->jobHelper = $jobHelper;
        $this->authorization = $authorization;
        $this->framework = $framework;
    }

    /**
     * Execute the edition of the jobs configuration
     * @return string
     */
    public function execute()
    {
        try {
            $messages = [];


            
            
            
            
            
            

            if (!$this->authorization->isAllowed('Wyomind_CronSchedulerPro::' . $this->_aclResource)) {
                $messages[] = __("You are not allowed to edit a cron job.");
            } else {
                $postItems = $this->getRequest()->getParam('items', []);
                $codes = array_keys($postItems); // codes of jobs modified

                $allJobs = $this->jobHelper->getJobData();

                $groups = [];
                $instances = [];
                $methods = [];

                foreach ($codes as $code) {
                    if (isset($allJobs[$code]) && $allJobs[$code]['isSystem'] == 1) {
                        // is the job is a system job, checking if the group, the method or the instance has been changed
                        $group = $postItems[$code]['group'] != $allJobs[$code]['group'];
                        $method = $postItems[$code]['method'] != $allJobs[$code]['method'];
                        $instance = $postItems[$code]['instance'] != $allJobs[$code]['instance'];
                        if ($group) {
                            $groups[] = $code;
                        }
                        if ($instance) {
                            $instances[] = $code;
                        }
                        if ($method) {
                            $methods[] = $code;
                        }
                    }
                }

                if (!empty($groups)) {
                    $messages[] = __("You cannot modify the group of jobs: %1", implode(", ", $groups));
                }
                if (!empty($instances)) {
                    $messages[] = __("You cannot modify the instance of jobs: %1", implode(", ", $instances));
                }
                if (!empty($methods)) {
                    $messages[] = __("You cannot modify the methods of jobs: %1", implode(", ", $methods));
                }
            }
        } catch (\Exception $e) {
            $messages[] = $e->getMessage();
        }

        $response = new \Magento\Framework\DataObject();
        $response->setMessages($messages);
        $response->setError(!empty($messages));

        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData($response);
        return $resultJson;
    }
}