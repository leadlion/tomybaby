<html>
<head>
<title>Web API call samples for Cron Scheduler Pro</title>
</head>
<body>

<?php
if (!file_exists(__DIR__ . '/app/bootstrap.php')) {
    echo "The sample file must be placed in the Magento root folder!";
    return;
}

require __DIR__ . '/app/bootstrap.php';

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$app = $bootstrap->createApplication('Magento\Framework\App\Http');

$login = "*******";
$password = "******";
$consumerKey = '*********';
$consumerSecret = '********';
$accessTokenSecret = '********';
$accessToken = "********";
$website = "********";

$cronJob = "cronscheduler_heartbeat";

echo "<pre>";

/**
 * Retrieve the access token
 */
echo " .--------------------------------------------------------------------------.\n";
echo " |  Retrieving access token                                                 |\n";
echo " '--------------------------------------------------------------------------'\n";
echo "\n";
echo " == Manually\n";
echo "\n";
echo "  >> access token: ".$accessToken."\n";
echo "\n";

echo " == Soap request\n";
echo "\n";
$userData = array("username" => $login, "password" => $password);
$ch = curl_init($website."/index.php/rest/V1/integration/admin/token");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Length: " . strlen(json_encode($userData))));
$accessTokenSoap = json_decode(curl_exec($ch));
echo "  >> access token: ".$accessTokenSoap."\n";
echo "\n";

echo " == Rest request\n";
echo "\n";
$request = new SoapClient($website . "/index.php/soap/?wsdl&services=integrationAdminTokenServiceV1", array("soap_version" => SOAP_1_2));
$response = $request->integrationAdminTokenServiceV1CreateAdminAccessToken(array("username" => $login, "password" => $password));
$accessTokenRest = $response->result;
echo "  >> access token: ".$accessTokenRest."\n";
echo "\n";

/*******************************************************************************
 * Session based authentication (requires anonymous acl)
 *******************************************************************************/
// simple open {website}/rest/V1/cron/run/{JOB_CODE} in your browser
//echo " .--------------------------------------------------------------------------.\n";
//echo " |  Session based authentication                                            |\n";
//echo " '--------------------------------------------------------------------------'\n";
//echo "\n";
//echo " == Executing the job '".$cronJob."' using the current session\n";
//echo "\n";
//echo "  >> open ".$website."/rest/V1/cron/run/".$cronJob." in your browser\n";
//echo "\n\n";

/*******************************************************************************
 * Token based authentication
 *******************************************************************************/

echo " .--------------------------------------------------------------------------.\n";
echo " |  Token based authentication                                              |\n";
echo " '--------------------------------------------------------------------------'\n";
echo "\n";

/**
 * SOAP V2 API
 */
echo " == Executing the job '".$cronJob."' using the Soap v2 web API\n";
echo "\n";

$opts = ["http" => ["header" => "Authorization: Bearer " . $accessToken]];
$context = stream_context_create($opts);

$wsdlService = $website . "/index.php/soap/default?wsdl&services=wyomindCronSchedulerProCronV1";

$soapClient = new \Zend\Soap\Client($wsdlService);
$soapClient->setSoapVersion(SOAP_1_2);
$soapClient->setStreamContext($context);

// run the job 'cronscheduler_heartbeat'
$soapResult = $soapClient->wyomindCronSchedulerProCronV1run(array("jobCode" => $cronJob));

if ($soapResult) {
    $result = json_decode($soapResult->result);
    echo "  >> Raw result: ".$soapResult->result."\n";
    if ($result->error) {
        echo "  >> Error when running the '".$cronJob."' job.\n";
        echo "  >> Message: " . $result->message;
    } else {
        echo "  >> The '".$cronJob."' job has been successfully executed.\n";
    }
}

echo "\n\n";

/**
 * REST V1 API
 */
echo " == Executing the job '".$cronJob."' using the REST v1 web API\n";
echo "\n";

$httpHeaders = new \Zend\Http\Headers();
$httpHeaders->addHeaders([
    'Authorization' => 'Bearer ' . $accessToken,
    'Accept' => 'application/json',
    'Content-Type' => 'application/json'
]);

$client = new \Zend\Http\Client();
$options = [
    'adapter' => 'Zend\Http\Client\Adapter\Curl',
    'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
    'maxredirects' => 0,
    'timeout' => 30
];

$client->setOptions($options);

$request = new \Zend\Http\Request();
$request->setHeaders($httpHeaders);

$request->setMethod(\Zend\Http\Request::METHOD_GET);
$request->setUri($website . "/index.php/rest/V1/cron/run/".$cronJob);
$response = $client->send($request);
$result = json_decode(json_decode($response->getContent()));

echo "  >> Raw result: ".$response->getContent()."\n";
if ($result->error) {
    echo "  >> Error when running the '".$cronJob."' job.\n";
    echo "  >> Message: " . $result->message;
} else {
    echo "  >> The 'cronscheduler_heartbeat' job has been successfully executed.\n";
}
echo "\n\n";

/*******************************************************************************
 * OAuth based authentication
 *******************************************************************************/
echo " .--------------------------------------------------------------------------.\n";
echo " |  OAuth based authentication                                              |\n";
echo " '--------------------------------------------------------------------------'\n";
echo "\n";
echo " == Executing the job '".$cronJob."' using OAuth base authentication\n";
echo "\n";

function sign($method, $url, $data, $consumerSecret, $tokenSecret)
{
    $url = urlEncodeAsZend($url);

    $data = urlEncodeAsZend(http_build_query($data, '', '&'));
    $data = implode('&', [$method, $url, $data]);

    $secret = implode('&', [$consumerSecret, $tokenSecret]);

    return base64_encode(hash_hmac('sha1', $data, $secret, true));
}
 
function urlEncodeAsZend($value)
{
    $encoded = rawurlencode($value);
    $encoded = str_replace('%7E', '~', $encoded);
    return $encoded;
}

$method = 'GET';
$url = $website."/index.php/rest/V1/cron/run/".$cronJob;

$data = [
    'oauth_consumer_key' => $consumerKey,
    'oauth_nonce' => md5(uniqid(rand(), true)),
    'oauth_signature_method' => 'HMAC-SHA1',
    'oauth_timestamp' => time(),
    'oauth_token' => $accessToken,
    'oauth_version' => '1.0',
];
 
$data['oauth_signature'] = sign($method, $url, $data, $consumerSecret, $accessTokenSecret);
 
$curl = curl_init();
 
curl_setopt_array($curl, [
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $url,
    CURLOPT_HTTPHEADER => [
        'Authorization: OAuth ' . http_build_query($data, '', ',')
    ]
]);
 
$result = curl_exec($curl);
curl_close($curl);
$response = json_decode(json_decode($result));
echo "  >> Raw result : ".$result."\n";
if ($response->error) {
    echo "  >> Error when running the '".$cronJob."' job.\n";
    echo "  >> Message: " . $response->message;
} else {
    echo "  >> The '".$cronJob."' job has been successfully executed.\n";
}
?>
</body>
</html>