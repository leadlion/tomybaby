<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Helper;

/**
 * Checker Helper
 * @version 1.0.0
 */
class Checker extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind, \Magento\Framework\App\Helper\Context $context)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context);
    }
    /**
     * Check if a code is unique
     * @param string $code
     * @return array: [error=>true,message=>...] or [error=>false]
     */
    public function isCodeUnique($code)
    {
        $jobs = $this->_jobHelper->getJobData();
        if (array_key_exists($code, $jobs)) {
            return ["error" => true, "message" => __("The code '%1' already exists", $code)];
        } else {
            return ["error" => false];
        }
    }
    /**
     * Check if a value is alpha-num (with possibility of allowed chars)
     * @param string $value
     * @param array $allowed
     * @return array: [error=>true,message=>...] or [error=>false]
     */
    public function isAlphaNum($value, $allowed = ["_"])
    {
        if (!ctype_alnum(str_replace($allowed, "", $value))) {
            return ["error" => true, "message" => __("The value '%1' is not an alphanumeric value", $value)];
        } else {
            return ["error" => false];
        }
    }
    /**
     * Check if a value is alpha-num AND backslash
     * @param type $value
     * @return array: [error=>true,message=>...] or [error=>false]
     */
    public function isAlphaNumAndBackslash($value)
    {
        return $this->isAlphaNum($value, ["_", "\\"]);
    }
    /**
     * Check if a value is alpha-num AND backslash AND #
     * @param string $value
     * @return array: [error=>true,message=>...] or [error=>false]
     */
    public function isAlphaNumAndBackslashAndHash($value)
    {
        return $this->isAlphaNum($value, ["_", "\\", "#"]);
    }
    /**
     * Check if a class exists
     * @param type $class
     * @return array: [error=>true,message=>...] or [error=>false]
     */
    public function classExists($class)
    {
        if (!class_exists($class)) {
            return ["error" => true, "message" => __("The class '%1' doesn't exist", $class)];
        } else {
            return ["error" => false];
        }
    }
    /**
     * Check if a method exists in a class
     * @param string $method (must be like class#method)
     * @return array: [error=>true,message=>...] or [error=>false]
     */
    public function methodExists($method)
    {
        list($class, $method) = explode("#", $method);
        if (!method_exists($class, $method)) {
            return ["error" => true, "message" => __("The method '%1' doesn't exist in the class '%2'", $method, $class)];
        } else {
            return ["error" => false];
        }
    }
    /**
     * Check if a cron expression is valid
     * @param type $schedule
     * @return array: [error=>true,message=>...] or [error=>false]
     */
    public function isScheduleValid($schedule)
    {
        if (!class_exists("\\Cron\\CronExpression")) {
            return ["error" => true, "message" => __("The class \\Cron\\CronExpression doesn't exist. Please run `composer require mtdowling/cron-expression:^1.2` in the Magento root folder")];
        }
        try {
            \Cron\CronExpression::factory($schedule);
        } catch (\Exception $e) {
            return ["error" => true, "message" => $e->getMessage()];
        }
    }
}