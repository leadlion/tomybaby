<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Helper;

/**
 * Stores > Configuration > Cron Scheduler Pro Helper
 */
class Config
{

    const XML_PATH_SYSTEM_BACKEND_NOTIFICATION_ON_FAIL = 'cronschedulerpro/system/backend_notification_on_fail';
    const XML_PATH_SYSTEM_BACKEND_NOTIFICATION_ON_FAIL_SETTINGS_SUBJECT = 'cronschedulerpro/system/backend_notification_on_fail_settings/subject';
    const XML_PATH_SYSTEM_BACKEND_NOTIFICATION_ON_FAIL_SETTINGS_CONTENT = 'cronschedulerpro/system/backend_notification_on_fail_settings/content';
    const XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL = 'cronschedulerpro/system/mail_notification_on_fail';
    const XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTINGS_EMAILS = 'cronschedulerpro/system/mail_notification_on_fail_settings/emails';
    const XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTINGS_SUBJECT = 'cronschedulerpro/system/mail_notification_on_fail_settings/subject';
    const XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTING_CONTENT = 'cronschedulerpro/system/mail_notification_on_fail_settings/content';
    const XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTINGS_SENDER_EMAIL = 'cronschedulerpro/system/mail_notification_on_fail_settings/sender_email';
    const XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTINGS_SENDER_NAME = 'cronschedulerpro/system/mail_notification_on_fail_settings/sender_name';
    
    const MAIL_NOTIFICATION_TEMPLATE = "wyomind_cronschedulerpro_task_failed";

}
