<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Helper;

/**
 * Job Helper for Magento 2.1
 * 
 
 */
class Job extends \Wyomind\CronScheduler\Plugin\Cron\Observer\ProcessCronQueueObserver
{
    /**
     * @var \Wyomind\Framework\Helper\Module
     */
    public $framework = null;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public $dateTime = null;

    /**
     * @var string
     */
    

    /**
     * Class constructor
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Cron\Model\ScheduleFactory $scheduleFactory
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param \Magento\Cron\Model\ConfigInterface $config
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Console\Request $request
     * @param \Magento\Framework\ShellInterface $shell
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Framework\Process\PhpExecutableFinderFactory $phpExecutableFinderFactory
     * @param \Magento\Framework\Event\Manager $eventManager
     * @param \Wyomind\CronScheduler\Helper\Task $taskHelper
     * @param \Wyomind\Framework\Helper\Module $framework
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Cron\Model\ScheduleFactory $scheduleFactory,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Cron\Model\ConfigInterface $config,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Console\Request $request,
        \Magento\Framework\ShellInterface $shell,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\Process\PhpExecutableFinderFactory $phpExecutableFinderFactory,
        \Magento\Framework\Event\Manager $eventManager,
        \Wyomind\CronScheduler\Helper\Task $taskHelper,
        \Wyomind\Framework\Helper\Module $framework,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    )
    {

        $construct= "__construct"; // in order to bypass the compiler
        parent::$construct($objectManager, $scheduleFactory, $cache, $config, $scopeConfig, $request, $shell, $timezone, $phpExecutableFinderFactory, $eventManager, $taskHelper);
        $this->framework = $framework;
        $this->dateTime = $dateTime;
    }

    /**
     * Get the job configuration for a specific job code
     * @param string $jobCode
     * @return array | null
     */
    public function getJobConfig($jobCode)
    {
        $jobGroupsRoot = $this->_config->getJobs();
        $groups = array_values($jobGroupsRoot);
        foreach (array_values($groups) as $jobs) {
            foreach ($jobs as $job) {
                if ((isset($job['code']) && $job['code'] == $jobCode) || (isset($job['name']) && $job['name'] == $jobCode)) {
                    return $job;
                }
            }
        }
        return null;
    }

    /**
     * Run a job
     * @param array $jobConfig
     * @return array
     */
    public function runNow($jobConfig)
    {
        try {
            if ($jobConfig['status'] == 0) {
                return ["error" => true, "message" => "The job is disabled"];
            }



            $currentTime = $this->timezone->scopeTimeStamp();
            $scheduledTime = $currentTime;

            $schedule = $this->_scheduleFactory->create();
            $schedule->setScheduledAt($this->dateTime->gmtDate('Y-m-d H:i:s'));
            $schedule->setJobCode($jobConfig['code']);

            $groupId = $jobConfig['group'];

            try {
                $this->_eventManager->dispatch('cronscheduler_task_run', ['task' => $schedule]);
                $this->_runJob($scheduledTime, $currentTime, $jobConfig, $schedule, $groupId);
                $this->_taskHelper->setTrace($schedule);
                $schedule->save();
                $this->_eventManager->dispatch('cronscheduler_task_success', ['task' => $schedule]);
                return ["error" => false];
            } catch (\Exception $e) {
                $this->_taskHelper->setTrace($schedule);
                $schedule->setErrorFile($e->getFile());
                $schedule->setErrorLine($e->getLine());
                $schedule->setMessages($e->getMessage());
                $schedule->save();
                $this->_eventManager->dispatch('cronscheduler_task_failed', ['task' => $schedule]);
                return ["error" => true, "message" => $e->getMessage()];
            }
        } catch (\Exception $e) {
            return ["error" => true, "message" => $e->getMessage()];
        }
    }
}