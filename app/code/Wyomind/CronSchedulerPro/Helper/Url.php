<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Helper;

class Url
{
    const JOB_SAVE = "cronschedulerpro/job/save";
    const JOB_EDIT = "cronschedulerpro/job/edit";
    const JOB_DELETE = "cronschedulerpro/job/delete";
    const JOB_RUN = "cronschedulerpro/job/run";
    const JOB_LISTING = "cronscheduler/job/listing";
}