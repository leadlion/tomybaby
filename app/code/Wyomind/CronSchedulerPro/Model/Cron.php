<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Model;

class Cron implements \Wyomind\CronSchedulerPro\Api\CronInterface
{
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    public function run($jobCode)
    {
        $jobConfig = $this->jobHelper->getJobConfig($jobCode);
        if ($jobConfig === null) {
            $result = ["error" => 1, "message" => __("The job doesn't exist")];
        } else {
            $result = $this->jobHelper->runNow($jobConfig);
        }
        return $this->jsonHelper->jsonEncode($result);
    }
}