<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Model;

/**
 * Cron job model
 * @version 1.0.0
 */
class Job extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Wyomind\CronScheduler\Model\ResourceModel\Task\CollectionFactory
     */
    protected $_taskCollectionFactory = null;
    /**
     * Class constructor
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Wyomind\CronScheduler\Model\ResourceModel\Task\CollectionFactory $taskCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(\Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Wyomind\CronScheduler\Model\ResourceModel\Task\CollectionFactory $taskCollectionFactory, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = [])
    {
        $this->_taskCollectionFactory = $taskCollectionFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    /**
     * Class sub constructor
     */
    public function _construct()
    {
        $this->_init('Wyomind\\CronSchedulerPro\\Model\\ResourceModel\\Job');
    }
    /**
     * Override of the delete method in order to delete the tasks associated with the job
     */
    public function delete()
    {
        // delete the tasks associated with the job too
        $code = $this->getCode();
        $taskCollection = $this->_taskCollectionFactory->create();
        $taskCollection->addFieldToFilter("job_code", ["eq" => $code]);
        foreach ($taskCollection as $task) {
            $task->delete();
        }
        // delete the job
        parent::delete();
    }
}