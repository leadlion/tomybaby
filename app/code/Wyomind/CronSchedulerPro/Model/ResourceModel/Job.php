<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Model\ResourceModel;

/**
 * Cron job resource model
 * @version 1.0.0
 */
class Job extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init('cron_job', 'id');
    }
}