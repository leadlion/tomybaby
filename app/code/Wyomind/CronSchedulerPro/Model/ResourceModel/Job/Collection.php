<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Model\ResourceModel\Job;

/**
 * Cron job collection
 * @version 1.0.0
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Wyomind\CronSchedulerPro\Model\Job', 'Wyomind\CronSchedulerPro\Model\ResourceModel\Job');
    }

    /**
     * Get a job by its code
     * @param string $code
     * @return boolean | mixed
     */
    public function getJobByCode($code) {
        $this->addFieldToFilter("code",["eq"=>$code]);
        $item = $this->getFirstItem();
        if ($item !== null) {
            return $item;
        } else {
            return false;
        }
    }
}