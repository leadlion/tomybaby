<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Observer;

/**
 * Check if a task failed.
 * If failed, then send a notification via email and/or a notification in the Magento backend
 * @version 1.0.0
 */
class CronSchedulerTaskFailed implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\AdminNotification\Model\InboxFactory
     */
    public $inboxFactory = null;
    /**
     * @var string
     */
    private $_taskListingUrl = \Wyomind\CronScheduler\Helper\Url::TASK_LISTING;
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind, \Magento\AdminNotification\Model\InboxFactory $inboxFactory)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->inboxFactory = $inboxFactory;
    }
    /**
     * Execute the observer
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $task = $observer->getTask();
        $fullTrace = "<pre>" . "<b>" . __("Job code: ") . "</b>" . $task->getJobCode() . "<br/>" . "<b>" . __("Executed at: ") . "</b>" . $task->getExecutedAt() . "<br/>" . "<b>" . __("Message: ") . "</b>" . $task->getMessages() . "<br/>" . "<b>" . __("File: ") . "</b>" . $task->getErrorFile() . "<br/>" . "<b>" . __("Line: ") . "</b>" . $task->getErrorLine() . "<br/>" . "<b>" . __("Origin: ") . "</b>" . $this->taskHelper->getOriginToString($task->getOrigin()) . "<br/>" . "<b>" . __("User: ") . "</b>" . $task->getUser() . "<br/>" . "<b>" . __("Ip: ") . "</b>" . $task->getIp() . "</pre>";
        // backend notification
        if ($this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_BACKEND_NOTIFICATION_ON_FAIL)) {
            $subject = $this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_BACKEND_NOTIFICATION_ON_FAIL_SETTINGS_SUBJECT);
            $subject = str_replace(["{{job_code}}", "{{message}}", "{{executed_at}}"], [$task->getJobCode(), $task->getMessages(), $task->getExecutedAt()], $subject);
            $content = $this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_BACKEND_NOTIFICATION_ON_FAIL_SETTINGS_CONTENT);
            $content = str_replace("{{full_trace}}", $fullTrace, $content);
            $content = str_replace(["{{job_code}}", "{{message}}", "{{executed_at}}", "{{file}}", "{{line}}"], [$task->getJobCode(), $task->getMessages(), $task->getExecutedAt(), $task->getErrorFile(), $task->getErrorLine()], $content);
            $content = str_replace(["{{ip}}", "{{origin}}", "{{user}}"], [$task->getIp(), $this->taskHelper->getOriginToString($task->getOrigin()), $task->getUser()], $content);
            $feedData = [['severity' => \Magento\Framework\Notification\MessageInterface::SEVERITY_MAJOR, 'date_added' => $this->dateTime->gmtDate('Y-m-d H:i:s'), 'title' => $subject, 'description' => $content, 'url' => $this->backendHelper->getUrl($this->_taskListingUrl, ['schedule_id' => $task->getScheduleId()])]];
            $this->inboxFactory->create()->parse($feedData);
        }
        // email notification
        if ($this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL)) {
            $emails = explode(',', $this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTINGS_EMAILS));
            if (count($emails) > 0) {
                $template = \Wyomind\CronSchedulerPro\Helper\Config::MAIL_NOTIFICATION_TEMPLATE;
                $subject = $this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTINGS_SUBJECT);
                $subject = str_replace(["{{job_code}}", "{{message}}", "{{executed_at}}"], [$task->getJobCode(), $task->getMessages(), $task->getExecutedAt()], $subject);
                $content = $this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTING_CONTENT);
                $content = str_replace("{{full_trace}}", $fullTrace, $content);
                $content = str_replace(["{{job_code}}", "{{message}}", "{{executed_at}}", "{{file}}", "{{line}}"], [$task->getJobCode(), $task->getMessages(), $task->getExecutedAt(), $task->getErrorFile(), $task->getErrorLine()], $content);
                $content = str_replace(["{{ip}}", "{{origin}}", "{{user}}"], [$task->getIp(), $this->taskHelper->getOriginToString($task->getOrigin()), $task->getUser()], $content);
                $transport = $this->transportBuilder->setTemplateIdentifier($template)->setTemplateOptions(['area' => \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE, 'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])->setTemplateVars(['content' => $content, 'subject' => $subject])->setFrom(['email' => $this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTINGS_SENDER_EMAIL), 'name' => $this->framework->getStoreConfig(\Wyomind\CronSchedulerPro\Helper\Config::XML_PATH_SYSTEM_MAIL_NOTIFICATION_ON_FAIL_SETTINGS_SENDER_NAME)])->addTo($emails[0]);
                $count = count($emails);
                for ($i = 1; $i < $count; $i++) {
                    $transport->addCc($emails[$i]);
                }
                $transport->getTransport()->sendMessage();
            }
        }
    }
}