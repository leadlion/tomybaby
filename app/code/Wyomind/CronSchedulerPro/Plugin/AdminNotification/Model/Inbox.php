<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Plugin\AdminNotification\Model;

class Inbox
{
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    public function aroundGetData($subject, \Closure $proceed, $key = '', $index = null)
    {
        $value = $proceed($key, $index);
        if ($key == "url") {
            if (strpos($value, "cronscheduler/task/listing") !== false) {
                $scheduleId = substr($value, strpos($value, '/schedule_id/') + 13);
                $scheduleId = substr($scheduleId, 0, strpos($scheduleId, '/'));
                $value = $this->urlBuilder->getUrl(\Wyomind\CronScheduler\Helper\Url::TASK_LISTING, ["schedule_id" => $scheduleId]);
            }
        }
        return $value;
    }
}