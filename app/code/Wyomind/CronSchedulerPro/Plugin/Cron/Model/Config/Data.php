<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Plugin\Cron\Model\Config;

/**
 * Plugin on Cron\Model\Config
 * Add data to the cron job config
 * 
 */
class Data
{
    /**
     * @var \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory
     */
    protected $_jobCollectionFactory = null;

    /**
     * Class constructor
     * @param \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
     */
    public function __construct(
        \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
    )
    {
        $this->_jobCollectionFactory = $jobCollectionFactory;
    }

    /**
     * Add data to the configJobs return of the getJobs method from the class Cron\Model\Config
     * @param $subject
     * @param array $configJobs
     * @return mixed
     */
    public function afterGetJobs($subject, $configJobs)
    {
        foreach ($configJobs as $group => $jobs) {
            foreach ($jobs as $code => $job) {
                $configJobs[$group][$code]['code'] = $code;
                $configJobs[$group][$code]['status'] = 1;
                $configJobs[$group][$code]['group'] = $group;
                $configJobs[$group][$code]['deleteAction'] = 0;
                $configJobs[$group][$code]['isSystem'] = 1;
                $configJobs[$group][$code]['config_schedule'] = isset($job['schedule'])?$job['schedule']:"";
            }
        }

        $collection = $this->_jobCollectionFactory->create();
        foreach ($collection as $job) {
            if (!isset($configJobs[$job->getGroup()])) {
                $configJobs[$job->getGroup()] = [];
            }
            $configJobs[$job->getGroup()][$job->getCode()] = [
                "code" => $job->getCode(),
                "group" => $job->getGroup(),
                "name" => $job->getCode(),
                "instance" => $job->getInstance(),
                "method" => $job->getMethod(),
                // modified schedule
                "schedule" => $job->getSchedule(), 
                // original schedule
                "config_schedule" => isset($configJobs[$job->getGroup()][$job->getCode()]['config_schedule'])?$configJobs[$job->getGroup()][$job->getCode()]['config_schedule']:$job->getSchedule(),
                "isSystem" => isset($configJobs[$job->getGroup()][$job->getCode()]['isSystem'])?1:0,
                'status' => $job->getStatus(),
                'deleteAction' => !isset($configJobs[$job->getGroup()][$job->getCode()]['isSystem']),
            ];
        }
        
        return $configJobs;
    }
}