<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Plugin\CronScheduler\Block\Adminhtml\Task;

/**
 * Tasks Timeline block - add disabled job support
 * @version 1.0.0
 */
class Timeline
{
    /**
     * @var array
     */
    protected $_jobStatus = [];
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $jobGroupsRoot = $this->cronConfig->getJobs();
        $groups = array_values($jobGroupsRoot);
        foreach ($groups as $group => $jobs) {
            foreach ($jobs as $job) {
                $this->_jobStatus[$job['code']] = isset($job['status']) ? $job['status'] : 1;
            }
        }
    }
    /**
     * Add specific style for disabled job
     * @param type $subject
     * @param array $data
     * @return array
     */
    public function afterGetTimelineData($subject, $data)
    {
        foreach ($data as &$task) {
            if (isset($this->_jobStatus[$task[0]]) && $this->_jobStatus[$task[0]] == 0) {
                $task[0] = "<span class=\"task-disabled\">" . $task[0] . "</span>";
            }
        }
        return $data;
    }
}