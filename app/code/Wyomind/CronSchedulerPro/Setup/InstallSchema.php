<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Cron Scheduler Pro install schema setup script
 * @version 1.0.0
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'cron_job'
         */
        $table = $installer->getConnection()
                ->newTable($installer->getTable('cron_job'))
                ->addColumn('id',
                    Table::TYPE_INTEGER, 11, [
                        'nullable' => false,
                        'precision' => '10',
                        'auto_increment' => true,
                        'primary' => true,
                    ], 'null'
                )
                ->addColumn('code',
                    Table::TYPE_TEXT, 250, [
                        'nullable' => false,
                    ], 'null'
                )
                ->addColumn('group',
                    Table::TYPE_TEXT, 100, [
                        'nullable' => false,
                    ], 'null'
                )
                ->addColumn('instance',
                    Table::TYPE_TEXT, 500, [
                        'nullable' => false,
                    ], 'null'
                )
                ->addColumn('method',
                    Table::TYPE_TEXT, 250, [
                        'nullable' => false,
                    ], 'null'
                )
                ->addColumn('schedule',
                    Table::TYPE_TEXT, 200, [
                        'nullable' => false,
                    ], 'null'
                )
                ->addColumn('status',
                    Table::TYPE_INTEGER, 1, [
                        'nullable' => false,
                        'precision' => '10',
                    ], 'null'
                )
                ->setComment('Cron Jobs');
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}