<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Setup;

class Recurring implements \Magento\Framework\Setup\InstallSchemaInterface
{
    private $_framework = null;

    /**
     * Recurring constructor.
     * @param \Wyomind\Framework\Helper\Install $framework
     */
    public function __construct(
        \Wyomind\Framework\Helper\Install $framework
    )
    {
        $this->_framework = $framework;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    )
    {
        $files = [
            "Helper/Job.php",
            "view/adminhtml/ui_component/cronscheduler_job_listing.xml"
        ];
        $this->_framework->copyFilesByMagentoVersion(__FILE__, $files);
    }
}