<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Ui\Component\JobListing\Column;

/**
 * Job listing actions
 * @version 1.0.0
 * 
 */
class Actions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var string
     */
    private $_deleteUrl = \Wyomind\CronSchedulerPro\Helper\Url::JOB_DELETE;
    /**
     * @var string
     */
    private $_runUrl = \Wyomind\CronSchedulerPro\Helper\Url::JOB_RUN;
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind, \Magento\Framework\View\Element\UiComponent\ContextInterface $context, \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory, array $components = [], array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    /**
     * Prepare Data Source
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $authorized = true;
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (isset($item['code'])) {
                    if (isset($item['status']) && $item['status'] == 1) {
                        if ($this->_authorization->isAllowed('Wyomind_CronSchedulerPro::run_job') && $authorized) {
                            $item[$name]['run'] = ['href' => $this->_urlBuilder->getUrl($this->_runUrl, ['group' => $item['group'], 'code' => $item['code'], 'redirect' => 'none']), 'label' => __('Run now'), 'confirm' => ['title' => __('Run a job'), 'message' => __("Are you sure you want to run the job <b>%1</b> now?", $item['code'])]];
                        }
                    }
                    if (isset($item['deleteAction']) && $item['deleteAction'] == 1) {
                        if ($this->_authorization->isAllowed('Wyomind_CronSchedulerPro::edit_job')) {
                            $item[$name]['delete'] = ['href' => $this->_urlBuilder->getUrl($this->_deleteUrl, ['code' => $item['code']]), 'label' => __('Delete'), 'confirm' => ['title' => __('Delete a job'), 'message' => __("Are you sure you want to delete the job <b>%1</b> and the tasks associated with it?", $item['code'])]];
                        }
                    }
                }
            }
        }
        return $dataSource;
    }
}