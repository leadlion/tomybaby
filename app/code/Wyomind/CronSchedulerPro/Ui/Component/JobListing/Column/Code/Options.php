<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\CronSchedulerPro\Ui\Component\JobListing\Column\Code;

/**
 * Define the options available for the column "code" in the jobs listing
 * @version 1.0.0
 */
class Options extends \Wyomind\CronScheduler\Ui\Component\JobListing\Column\Code\Options
{
    /**
     * @var \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory
     */
    protected $_jobCollectionFactory = null;

    /**
     * Class constructor
     * @param \Magento\Cron\Model\ConfigInterface $cronConfig
     * @param \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
     */
    public function __construct(
        \Magento\Cron\Model\ConfigInterface $cronConfig,
        \Wyomind\CronSchedulerPro\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
    )
    {
        parent::__construct($cronConfig);
        $this->_jobCollectionFactory = $jobCollectionFactory;
    }

    /**
     * Get all options available
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            parent::toOptionArray();
            $collection = $this->_jobCollectionFactory->create();
            foreach ($collection as $job) {
                $this->options[] = [
                    "label" => $job->getCode(), "value" => $job->getCode()
                ];
            }
            $this->options = array_map("unserialize", array_unique(array_map("serialize", $this->options)));
        }

        return $this->options;
    }
}