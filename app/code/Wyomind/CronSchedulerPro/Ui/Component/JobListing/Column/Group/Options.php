<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Ui\Component\JobListing\Column\Group;

/**
 * Job group options
 * @version 1.0.0
 */
class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;
    public function __construct(\Wyomind\CronSchedulerPro\Helper\Delegate $wyomind)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
    }
    /**
     * Get the options as array
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = [];
            $configJobs = $this->_cronConfig->getJobs();
            $groups = [];
            foreach (array_keys($configJobs) as $group) {
                if (!in_array($group, $groups)) {
                    $this->options[] = ['label' => $group, 'value' => $group];
                }
            }
            return $this->options;
        }
    }
}