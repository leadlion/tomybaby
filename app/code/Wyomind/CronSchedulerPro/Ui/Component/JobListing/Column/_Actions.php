<?php



/**     
 * The technical support is guaranteed for all modules proposed by Wyomind.
 * The below code is obfuscated in order to protect the module's copyright as well as the integrity of the license and of the source code.
 * The support cannot apply if modifications have been made to the original source code (https://www.wyomind.com/terms-and-conditions.html).
 * Nonetheless, Wyomind remains available to answer any question you might have and find the solutions adapted to your needs.
 * Feel free to contact our technical team from your Wyomind account in My account > My tickets. 
 * Copyright © 2018 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\CronSchedulerPro\Ui\Component\JobListing\Column;


class Actions extends \Magento\Ui\Component\Listing\Columns\Column
{public $xb3=null;public $xe0=null;public $x0c=null;

    
    protected $_urlBuilder;

    
    private $x3913 = \Wyomind\CronSchedulerPro\Helper\Url::JOB_DELETE;

    
    private $x3921 = \Wyomind\CronSchedulerPro\Helper\Url::JOB_RUN;

    
    private $x387e = null;

    
    public $coreHelper = null;

    
    public $error = "\111n\166\141\x6c\x69\144\40\x6c\151c\145\156\163\x65";

    

    
    public function __construct(
    \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
            \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
            \Magento\Framework\UrlInterface $urlBuilder,
            \Magento\Framework\Authorization $authorization,
            \Wyomind\Core\Helper\Data $coreHelper,
            array $components = [],
            array $data = []
    )
    {
        $coreHelper->constructor($this, func_get_args());
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->{$this->xb3->x3863->x392e} = $authorization;
        $this->{$this->x0c->x387d->{$this->x0c->x387d->x3c91}} = $urlBuilder;
        $this->{$this->xb3->x3863->{$this->xe0->x3863->{$this->xe0->x3863->{$this->x0c->x3863->x38bf}}}} = $coreHelper;
    }

    
    public function prepareDataSource(array $x3836)
    {$x37e4 = $this->xb3->x387d->{$this->x0c->x387d->{$this->x0c->x387d->x3edc}};$x37bf = $this->xb3->x387d->{$this->xb3->x387d->{$this->x0c->x387d->x3eed}};
        ${$this->x0c->x3863->{$this->xe0->x3863->x3ab5}} = true;
        try {
            
            ${$this->xb3->x3863->{$this->xb3->x3863->{$this->xb3->x3863->{$this->xe0->x3863->x3abf}}}} = $this;
            ${$this->x0c->x387d->{$this->xb3->x387d->x3e14}} = $x37e4($x37bf());
            $this->${$this->xe0->x3863->{$this->x0c->x3863->x3ac6}} = "";
            ${$this->x0c->x3863->{$this->x0c->x3863->x3ad0}} = "\145r\x72\157\162";
            ${$this->x0c->x387d->x3e1f} = "\\\x45\x78\x63\145\160\164\x69on";
            ${$this->x0c->x387d->x3e06}->coreHelper->{$this->x0c->x3863->x3bb8}(${$this->xb3->x387d->{$this->xe0->x387d->{$this->xb3->x387d->x3e0d}}}, ${$this->xe0->x3863->{$this->x0c->x3863->x3ac6}});
            ${$this->xe0->x3863->{$this->xe0->x3863->x3adf}} = new ${$this->x0c->x3863->{$this->x0c->x3863->{$this->xb3->x3863->x3ad9}}}(__(${$this->xb3->x3863->{$this->xb3->x3863->{$this->xb3->x3863->{$this->xb3->x3863->{$this->xb3->x3863->x3ac0}}}}}->${$this->xe0->x387d->{$this->xe0->x387d->{$this->xe0->x387d->x3e1a}}}));
            if (${$this->xb3->x3863->{$this->xb3->x3863->x3abb}}->${$this->xe0->x3863->{$this->xe0->x3863->{$this->xe0->x3863->x3ac8}}} != $x37e4(${$this->xe0->x387d->x3e12})) {
                throw ${$this->xb3->x3863->x3adb};
            }
            
        } catch (\Exception $e) {
            ${$this->xe0->x387d->{$this->x0c->x387d->x3dff}} = false;
        }

        if (isset(${$this->xe0->x3863->x3aaf}['data']['items'])) {
            foreach (${$this->xe0->x3863->x3aaf}['data']['items'] as & ${$this->x0c->x387d->{$this->xb3->x387d->x3e3c}}) {
                ${$this->x0c->x387d->x3e3d} = $this->{$this->x0c->x3863->x3be1}('name');
                if (isset(${$this->xe0->x3863->{$this->x0c->x3863->{$this->x0c->x3863->{$this->xb3->x3863->x3af9}}}}['code'])) {
                    if (isset(${$this->xe0->x387d->x3e38}['status']) && ${$this->xe0->x3863->{$this->x0c->x3863->{$this->x0c->x3863->{$this->xb3->x3863->x3af9}}}}['status'] == 1) {
                        if ($this->{$this->xe0->x3863->{$this->xe0->x3863->x3885}}->{$this->xe0->x3863->x3be8}('Wyomind_CronSchedulerPro::run_job') && ${$this->x0c->x3863->x3ab4}) {
                            ${$this->x0c->x3863->x3aed}[${$this->x0c->x3863->x3afb}]['run'] = [
                                'href' => $this->{$this->xb3->x3863->{$this->xb3->x3863->x3911}}->{$this->xb3->x3863->x3bf0}($this->{$this->xb3->x3863->{$this->x0c->x3863->x3926}}, ['group' => ${$this->xe0->x3863->{$this->x0c->x3863->{$this->x0c->x3863->{$this->xb3->x3863->x3af9}}}}['group'], 'code' => ${$this->xe0->x387d->x3e38}['code'], 'redirect' => 'none' ]),
                                'label' => __('Run now'),
                                'confirm' => [
                                    'title' => __('Run a job'),
                                    'message' => __("Ar\145\40\171\157u s\165r\x65 \171\x6f\165 \x77\141\156t\x20\x74o\x20\x72u\156\40\164\x68\x65\40jo\x62\40\74b\76%\61<\57b\x3e\40\x6e\x6fw?", ${$this->xe0->x3863->{$this->x0c->x3863->{$this->x0c->x3863->{$this->xb3->x3863->x3af9}}}}['code'])
                                ]
                            ];
                        }
                    }
                    if (isset(${$this->xe0->x387d->x3e38}['deleteAction']) && ${$this->x0c->x387d->{$this->xb3->x387d->x3e3c}}['deleteAction'] == 1) {
                        if ($this->{$this->xe0->x3863->{$this->x0c->x3863->{$this->xb3->x3863->x388a}}}->{$this->xe0->x3863->x3be8}('Wyomind_CronSchedulerPro::edit_job')) {
                            ${$this->xe0->x387d->x3e38}[${$this->x0c->x3863->x3afb}]['delete'] = [
                                'href' => $this->{$this->x0c->x387d->{$this->x0c->x387d->x3c91}}->{$this->xb3->x3863->x3bf0}($this->{$this->xb3->x3863->{$this->x0c->x3863->{$this->xb3->x3863->x391f}}}, ['code' => ${$this->xe0->x387d->x3e38}['code']]),
                                'label' => __('Delete'),
                                'confirm' => [
                                    'title' => __('Delete a job'),
                                    'message' => __("\101\x72\x65 \171\x6f\165 \163\x75\162e \x79\157u\40\x77a\156t\40\x74o\40d\145\154\145\x74\145 \x74\150\145 \152\x6f\142\x20\x3c\x62\x3e\x25\61\74\57b>\40a\156\144\40\164\150e\x20t\141sks\40\x61\163\x73o\x63\x69\141\x74\145\144\40\167i\x74\150 i\x74\x3f", ${$this->xe0->x3863->{$this->x0c->x3863->{$this->xe0->x3863->x3af4}}}['code'])
                                ]
                            ];
                        }
                    }
                }
            }
        }

        return ${$this->xe0->x387d->x3df6};
    }

}
