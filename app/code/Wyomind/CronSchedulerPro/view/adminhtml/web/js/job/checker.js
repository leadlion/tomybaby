/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 *
 * Job creation for checker
 * @param {type} $
 * @returns {checker_L9.checkerAnonym$1}
 */
define(['jquery'], function ($) {
    'use strict';
    return {
        checkerUrl: "",
        check: function (methods, elt, value) {
            if (value === "") {
                return;
            }
            elt.attr("disabled","disabled");
            elt.addClass("checking");

            var eltId = elt.attr('id');

            $.ajax({
                url: this.checkerUrl,
                type: 'POST',
                showLoader: false,
                data: {
                    methods: methods,
                    value: value
                },
                success: function (data) {
                    if (data.error) {
                        if ($("#"+eltId+"-error").length === 0) {
                            var errorLabel = $("<label>", {for : eltId, generated: "true", class: "mage-error", id: eltId+"-error"});
                            errorLabel.html(data.messages);
                            errorLabel.insertAfter("#"+eltId);
                            elt.addClass("checker-error");
                        } else {
                            $("#"+eltId+"-error").html(data.messages);
                            $("#"+eltId+"-error").css({display: "block"});
                            elt.addClass("checker-error");
                        }
                    } else {
                        if ($("#"+eltId+"-error").length > 0) {
                            $("#"+eltId+"-error").html("");
                            $("#"+eltId+"-error").css({display: "none"});
                            elt.removeClass("checker-error");
                        }
                    }
                    elt.removeClass("checking");
                    elt.attr("disabled",false);
                }.bind(this)
            });
        }
    };
});
