/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 *
 * New job form
 * @param {type} $
 * @param {type} alert
 * @returns {edit_L9.editAnonym$1}
 */
define(['jquery', 'Magento_Ui/js/modal/alert'], function ($, alert) {
    'use strict';
    return {
        /**
         * Save the new job. First check that all fields are ok
         * @returns void
         */
        save: function () {
            var errors = $(".checker-error");
            if (errors.length > 0) {
                var alertContent = "<br/><br/>";
                errors.each(function (error) {
                    var label = errors[error].up().up().select('span')[0].innerHTML;
                    var messages = errors[error].up().select('label')[0].innerHTML;
                    alertContent += "<b>" + label + "</b> : " + messages + " (value: <i>" + errors[error].value + "</i>)<br/><br/>";
                });

                alert({
                    title: "The job cannot be saved!",
                    content: alertContent
                });
            } else {
                $('#edit_form').submit();
            }
        }
    };
});
