<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer;

/**
 *
 */
class Action extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Action
{
    protected $_framework;
    protected $_watchlogHelper;
    public function __construct(\Wyomind\WatchlogPro\Helper\Delegate $wyomind, \Magento\Backend\Block\Context $context, \Magento\Framework\Json\EncoderInterface $jsonEncoder, array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $jsonEncoder, $data);
    }
    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $actions = [];
        $ip = $row->getIp();
        $currentIp = strtok($this->_request->getClientIp(), ',');
        $isBlackListed = $this->_watchlogHelper->isBlacklisted($ip);
        $isWhiteListed = $this->_watchlogHelper->isWhitelisted($ip);
        if (!$isBlackListed && !$isWhiteListed && $ip != $currentIp) {
            // you cannot blacklist yourself !!
            $actions[] = ['url' => $this->getUrl('watchlogpro/blacklist/add', ['ip' => $ip]), 'confirm' => __("Please confirm that you want to add the IP '") . $ip . __("' to the blacklist"), 'caption' => __("Add IP to the blacklist")];
        }
        if ($isBlackListed && !$isWhiteListed) {
            $actions[] = ['url' => $this->getUrl('watchlogpro/blacklist/remove', ['ip' => $ip]), 'confirm' => __("Please confirm that you want to remove the IP '") . $ip . __("' from the blacklist"), 'caption' => __("Remove IP from the blacklist")];
        }
        if (!$isWhiteListed && !$isBlackListed) {
            $actions[] = ['url' => $this->getUrl('watchlogpro/whitelist/add', ['ip' => $ip]), 'confirm' => __("Please confirm that you want to add the IP '") . $ip . __("' to the whitelist"), 'caption' => __("Add IP to the whitelist")];
        }
        if ($isWhiteListed && !$isBlackListed) {
            $actions[] = ['url' => $this->getUrl('watchlogpro/whitelist/remove', ['ip' => $ip]), 'confirm' => __("Please confirm that you want to remove the IP '") . $ip . __("'  from the whitelist"), 'caption' => __("Remove IP from the whitelist")];
        }
        $this->getColumn()->setActions($actions);
        return parent::render($row);
    }
}