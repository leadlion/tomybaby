<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer;

/**
 * Class IpAdvanced
 * @package Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer
 */
class IpAdvanced extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_watchlogHelper;
    public function __construct(\Wyomind\WatchlogPro\Helper\Delegate $wyomind, \Magento\Backend\Block\Context $context, array $data = [])
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        parent::__construct($context, $data);
    }
    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $ip = $row->getIp();
        if ($this->_watchlogHelper->isWhitelisted($ip)) {
            return "<span class='listed whitelisted' title='" . __('Whitelisted') . "
" . __('Check this ip') . "'><span><a target='_blank' href='http://www.abuseipdb.com/check/" . $ip . "' >" . $ip . "</a></span></span>";
        } elseif ($this->_watchlogHelper->isBlacklisted($ip)) {
            return "<span class='listed blacklisted' title='" . __('Blacklisted') . "
" . __('Check this ip') . "'><span><a target='_blank' href='http://www.abuseipdb.com/check/" . $ip . "' >" . $ip . "</a></span></span>";
        } else {
            return "<span class='listed'><span><a target='_blank' href='http://www.abuseipdb.com/check/" . $ip . "' title='" . __('Check this ip') . "'>" . $ip . "</a></span><span>";
        }
    }
}