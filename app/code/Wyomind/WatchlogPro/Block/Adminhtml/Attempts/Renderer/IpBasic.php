<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer;

/**
 * Class IpBasic
 * @package Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer
 */
class IpBasic extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $ip = $row->getIp();
        if ($row->getIpStatus() == 1) {
            return "<span class='listed whitelisted' title='" . __('Whitelisted') . "\n" . __('Check this ip') . "'><span><a target='_blank' href='http://www.abuseipdb.com/check/".$ip."' >".$ip."</a></span></span>";
        } elseif ($row->getIpStatus() === 0) {
            return "<span class='listed blacklisted' title='" . __('Blacklisted') . "\n" . __('Check this ip') . "'><span><a target='_blank' href='http://www.abuseipdb.com/check/".$ip."' >".$ip."</a></span></span>";
        } else {
            return "<span class='listed'><span><a target='_blank' href='http://www.abuseipdb.com/check/".$ip."' title='".__('Check this ip')."'>".$ip."</a></span><span>";
        }
    }
}
