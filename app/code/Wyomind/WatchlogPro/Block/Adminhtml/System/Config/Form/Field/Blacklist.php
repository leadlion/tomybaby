<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Block\Adminhtml\System\Config\Form\Field;

/**
 * Class Blacklist
 * @package Wyomind\WatchlogPro\Block\Adminhtml\System\Config\Form\Field
 */
class Blacklist extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{


    /**
     * @var string
     */
    protected $_template = 'Wyomind_WatchlogPro::system/config/form/field/array.phtml';

    /**
     *
     */
    protected function _construct()
    {
        $this->addColumn('bip', ['label' => __('IP')]);
        $this->addColumn('until', ['label' => __('Until')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Ip');
        parent::_construct();
    }

    /**
     * @param string $columnName
     * @return string
     * @throws \Exception
     */
    public function renderCellTemplate($columnName)
    {
        if ($columnName != "until") {
            return parent::renderCellTemplate($columnName);
        } else {
            $inputName = $this->_getCellInputElementName($columnName);
            $html = '<input type="text" name="' . $inputName . '" id="' . $this->_getCellInputElementId('<%- _id %>', $columnName) . '"/>';
            return $html;
        }
    }
}
