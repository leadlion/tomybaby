<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Block\Adminhtml\System\Config\Form\Field;

/**
 * Class Whitelist
 * @package Wyomind\WatchlogPro\Block\Adminhtml\System\Config\Form\Field
 */
class Whitelist extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{

    /**
     *
     */
    protected function _construct()
    {
        $this->addColumn('ip', ['label' => __('IP')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Ip');
        parent::_construct();
    }
}
