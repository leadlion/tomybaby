<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\WatchlogPro\Controller\Adminhtml\Blacklist;

/**
 * Class Add
 * @package Wyomind\WatchlogPro\Controller\Adminhtml\Blacklist
 */
class Add extends \Wyomind\WatchlogPro\Controller\Adminhtml\Blacklist
{

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $ip = $this->_request->getParam('ip');
        if ($ip == 'me') {
            $ip = strtok($this->getRequest()->getClientIp(), ',');
        }
        $this->_watchlogHelper->removeFromWhitelist($ip);
        $this->_watchlogHelper->addToBlacklist($ip);
        return $this->resultRedirectFactory->create()->setPath('watchlog/attempts/advanced');
        
    }
}
