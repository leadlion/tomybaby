<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\WatchlogPro\Controller\Adminhtml\Blacklist;

/**
 * Class Remove
 * @package Wyomind\WatchlogPro\Controller\Adminhtml\Blacklist
 */
class Remove extends \Wyomind\WatchlogPro\Controller\Adminhtml\Blacklist
{

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
        $this->_watchlogHelper->removeFromBlacklist($this->_request->getParam('ip'));
        return $this->resultRedirectFactory->create()->setPath('watchlog/attempts/advanced');
        
    }
}
