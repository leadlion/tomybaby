<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\WatchlogPro\Controller\Adminhtml\Whitelist;

/**
 * Class Remove
 * @package Wyomind\WatchlogPro\Controller\Adminhtml\Whitelist
 */
class Remove extends \Wyomind\WatchlogPro\Controller\Adminhtml\Whitelist
{

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_watchlogHelper->removeFromWhitelist($this->_request->getParam('ip'));
        return $this->resultRedirectFactory->create()->setPath('watchlog/attempts/advanced');
        
    }
}
