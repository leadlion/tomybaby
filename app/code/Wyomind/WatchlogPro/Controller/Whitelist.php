<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Controller;

use Magento\Store\Model\StoreManager;

/**
 * Simple google shopping frontend controller
 */
abstract class Whitelist extends \Magento\Framework\App\Action\Action
{

    /**
     * @var null
     */
    protected $_resultForwardFactory = null;
    /**
     * @var null|\Wyomind\Framework\Helper\Module
     */
    protected $_framework = null;
    /**
     * @var null|\Wyomind\WatchlogPro\Helper\Data
     */
    protected $_watchlogproHelper = null;
    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Controller construtor
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Wyomind\Framework\Helper\Module $framework
     * @param \Wyomind\WatchlogPro\Helper\Data $watchlogproHelper
     * @param StoreManager $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Wyomind\Framework\Helper\Module $framework,
        \Wyomind\WatchlogPro\Helper\Data $watchlogproHelper,
        StoreManager $storeManager
    ) {
        $this->_resultRawFactory = $resultRawFactory;
        $this->_framework = $framework;
        $this->_watchlogproHelper = $watchlogproHelper;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * execute action
     */
    abstract public function execute();
}
