<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Controller\Whitelist;

/**
 * Class Add
 * @package Wyomind\WatchlogPro\Controller\Whitelist
 */
class Add extends \Wyomind\WatchlogPro\Controller\Whitelist
{


    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
        $resultRaw = $this->_resultRawFactory->create();
        $secretKey = $this->_framework->getDefaultConfigUncrypted('watchlogpro/white_black_list/secret_key');
        $localSecretKey = $this->getRequest()->getParam('key');
        $ip = strtok($this->getRequest()->getClientIp(), ',');
        if ($secretKey == $localSecretKey) {
            $this->_watchlogproHelper->addToWhitelist($ip);
            return $resultRaw->setContents("<body>".__('You IP has been added to the whitelist.')."<br/><a href='".$this->storeManager->getStore()->getBaseUrl()."'>".__('Back to the website')."</a></body>");
        } else {
            return $resultRaw->setContents("");
        }
    }
}
