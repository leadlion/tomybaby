<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Helper;

/**
 * Class Data
 * @package Wyomind\WatchlogPro\Helper
 * 
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{


    const BLOCKED = 2;
    const WHITELISTED = 1;
    const BLACKLISTED = 0;
    const IGNORED = -1;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone|null
     */
    protected $_timezone = null;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime|null
     */
    protected $_datetime = null;
    /**
     * @var null|\Wyomind\Framework\Helper\Module
     */
    protected $_framework = null;
    /**
     * @var null|\Wyomind\WatchlogPro\Model\ResourceModel\Attempts\CollectionFactory
     */
    protected $_attemptsCollectionFactory = null;
    /**
     * @var null|\Wyomind\WatchlogPro\Logger\Logger
     */
    protected $_log = null;
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilderFactory|null
     */
    protected $_transportBuilderFactory = null;
    /**
     * @var \Magento\Framework\App\CacheInterface|null
     */
    protected $_cacheManager = null;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\Timezone $timezone
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $datetime
     * @param \Wyomind\WatchlogPro\Model\ResourceModel\Attempts\CollectionFactory $attemptsCollectionFactory
     * @param \Wyomind\Framework\Helper\Module $framework
     * @param \Wyomind\WatchlogPro\Logger\Logger $log
     * @param \Magento\Framework\Mail\Template\TransportBuilderFactory $transportBuilderFactory
     * @param \Magento\Framework\App\CacheInterface $cacheManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        \Wyomind\WatchlogPro\Model\ResourceModel\Attempts\CollectionFactory $attemptsCollectionFactory,
        \Wyomind\Framework\Helper\Module $framework,
        \Wyomind\WatchlogPro\Logger\Logger $log,
        \Magento\Framework\Mail\Template\TransportBuilderFactory $transportBuilderFactory,
        \Magento\Framework\App\CacheInterface $cacheManager
    )
    {
        parent::__construct($context);
        $this->_timezone = $timezone;
        $this->_datetime = $datetime;
        $this->_framework = $framework;
        $this->_attemptsCollectionFactory = $attemptsCollectionFactory;
        $this->_log = $log;
        $this->_transportBuilderFactory = $transportBuilderFactory;
        $this->_cacheManager = $cacheManager;
    }

    /**
     * @param $msg
     */
    public function log($msg)
    {
        if ($this->_framework->getDefaultConfig('watchlogpro/reporting/enable_log') == 1) {
            $this->_log->notice($msg);
        }
    }

    /**
     * @return string
     */
    public function getBlockDuration()
    {
        return $this->_framework->getDefaultConfig('watchlogpro/white_black_list/block_duration');
    }

    /**
     * @return string
     */
    public function getNbAttemptsBeforeBlocked()
    {
        return $this->_framework->getDefaultConfig("watchlogpro/white_black_list/attempts");
    }

    /**
     * @return string
     */
    public function getUseWhitelistOnly()
    {
        return $this->_framework->getDefaultConfig("watchlogpro/white_black_list/use_whitelist_only");
    }

    /**
     * @return mixed
     */
    public function getWhitelist()
    {
        if (version_compare($this->_framework->getMagentoVersion(), '2.2.0') < 0) {
            // MAGE < 2.2.0
            // @codingStandardsIgnoreStart
            return unserialize($this->_framework->getDefaultConfig("watchlogpro/white_black_list/whitelist"));
            // @codingStandardsIgnoreEnd
        } else {
            // FROM MAGE 2.2.0
            return json_decode($this->_framework->getDefaultConfig("watchlogpro/white_black_list/whitelist"), true);
        }
    }

    /**
     * @return string
     */
    public function getWhitelistToString()
    {
        $whitelist = $this->getWhitelist();
        $ips = [];
        if (is_array($whitelist)) {
            foreach ($whitelist as $ip) {
                $ips[] = $ip['ip'];
            }
        }
        return implode(", ", $ips);
    }

    /**
     * @return string
     */
    public function getBlacklistToString()
    {
        $blacklist = $this->getBlacklist();
        $ips = [];
        if (is_array($blacklist)) {
            foreach ($blacklist as $ip) {
                $ips[] = $ip['bip'] . " : " . $ip['until'];
            }
        }
        return implode("\n", $ips);
    }

    /**
     * @param $whitelist
     */
    public function setWhitelist($whitelist)
    {
        if (version_compare($this->_framework->getMagentoVersion(), '2.2.0') < 0) {
            // MAGE < 2.2.0
            // @codingStandardsIgnoreStart
            $this->_framework->setDefaultConfig("watchlogpro/white_black_list/whitelist", serialize($whitelist));
            // @codingStandardsIgnoreEnd
        } else {
            $this->_framework->setDefaultConfig("watchlogpro/white_black_list/whitelist", json_encode($whitelist));
        }
    }

    /**
     * @return array|mixed
     */
    public function getBlacklist()
    {
        if (version_compare($this->_framework->getMagentoVersion(), '2.2.0') < 0) {
            // MAGE < 2.2.0
            if ($this->_framework->getDefaultConfig("watchlogpro/white_black_list/blacklist") == "[]") {
                return [];
            }
            // @codingStandardsIgnoreStart
            return unserialize($this->_framework->getDefaultConfig("watchlogpro/white_black_list/blacklist"));
            // @codingStandardsIgnoreEnd
        } else {
            // FROM MAGE 2.2.0
            return json_decode($this->_framework->getDefaultConfig("watchlogpro/white_black_list/blacklist"), true);
        }
    }

    /**
     * @param $blacklist
     */
    public function setBlacklist($blacklist)
    {
        if (version_compare($this->_framework->getMagentoVersion(), '2.2.0') < 0) {
            // MAGE < 2.2.0
            // @codingStandardsIgnoreStart
            $this->_framework->setDefaultConfig("watchlogpro/white_black_list/blacklist", serialize($blacklist));
            // @codingStandardsIgnoreEnd
        } else {
            $this->_framework->setDefaultConfig("watchlogpro/white_black_list/blacklist", json_encode($blacklist));
        }
    }

    /**
     * @param $ip
     * @return string
     */
    public function blockedUntil($ip)
    {
        $blacklist = $this->getBlacklist();
        foreach ($blacklist as $ipTmp) {
            if ($ipTmp['bip'] == $ip) {
                if (isset($ipTmp['until'])) {
                    return $ipTmp['until'];
                } else {
                    return "";
                }
            }
        }
        return "";
    }

    /**
     * @param $ip
     * @return bool
     */
    public function isBlacklisted($ip)
    {
        $tmpArray = [];
        $blacklist = $this->getBlacklist();
        if ($blacklist == "") {
            return false;
        }
        foreach ($blacklist as $ipTmp) {
            $tmpArray[] = $ipTmp['bip'];
        }
        return in_array($ip, $tmpArray);
    }

    /**
     * @param $ip
     * @return bool
     */
    public function isWhitelisted($ip)
    {
        $tmpArray = [];
        $whitelist = $this->getWhitelist();

        if ($whitelist == "") {
            return false;
        }

        $ipSplitted = explode(".", $ip);

        foreach ($whitelist as $ipTmp) { // for each whitelisted IP
            $splitted = explode(".", $ipTmp['ip']);
            $diff = array_diff($splitted, $ipSplitted);
            if (count($diff) == 0) { // no difference between IP and whitelisted IP => IP whitelisted
                return true;
            } else { // differences => check if wildcards in use
                foreach ($diff as $d) {
                    if ($d != '*') {
                        return false;
                    }
                } // wildcards in use => IP whitelisted
                return true;
            }
        }
        return false;
    }

    /**
     * @param $ip
     */
    public function addToWhitelist($ip)
    {
        if (!$this->isWhitelisted($ip)) {
            $whitelist = $this->getWhitelist();
            $whitelist[] = ['ip' => $ip];
            $this->setWhitelist($whitelist);
            $this->_cacheManager->clean(['config']);
        }
    }

    /**
     * @param $ip
     */
    public function removeFromWhitelist($ip)
    {
        $tmpArray = [];
        $whitelist = $this->getWhitelist();
        foreach ($whitelist as $key => $ipTmp) {
            if ($ip == $ipTmp['ip']) {
                unset($whitelist[$key]);
            }
        }
        $this->setWhitelist($whitelist);
        $this->_cacheManager->clean(['config']);
    }

    /**
     * @param $ip
     * @param null $until
     */
    public function addToBlacklist(
        $ip,
        $until = null
    )
    {
        if (!$this->isBlacklisted($ip)) {
            $blacklist = $this->getBlacklist();
            $blacklist[] = ['bip' => $ip, 'until' => $until];
            $this->setBlacklist($blacklist);
            $this->_cacheManager->clean(['config']);
        }
    }

    /**
     * @param $ip
     */
    public function removeFromBlacklist($ip)
    {
        $tmpArray = [];
        $blacklist = $this->getBlacklist();
        foreach ($blacklist as $key => $ipTmp) {
            if ($ip == $ipTmp['bip']) {
                unset($blacklist[$key]);
            }
        }
        $this->setBlacklist($blacklist);
        $this->_cacheManager->clean(['config']);
    }

    /**
     * @param $ip
     * @return mixed
     */
    public function getLastSuccessfulAttempt($ip)
    {
        return $this->_attemptsCollectionFactory->create()->getLastSuccessfulAttempt($ip);
    }

    /**
     * @param $date
     * @param $ip
     * @return mixed
     */
    public function getNbFailureAttemptsAfter(
        $date,
        $ip
    )
    {
        return $this->_attemptsCollectionFactory->create()->getNbFailureAttemptsAfter($date, $ip);
    }

    /**
     * @param $ip
     * @return bool
     */
    public function checkNeedToBlacklist($ip)
    {
        $this->log(">> Attempts before being blocked : " . $this->getNbAttemptsBeforeBlocked());
        if (!$this->isWhitelisted($ip) && $this->getNbAttemptsBeforeBlocked() != 0) {
            $lastSuccessfulAttempt = $this->getLastSuccessfulAttempt($ip);
            $this->log(">> Last successful attempt : " . $lastSuccessfulAttempt);
            $nbAttempts = $this->getNbFailureAttemptsAfter($lastSuccessfulAttempt, $ip);
            $this->log(">> Nb attempts : " . $nbAttempts);
            if ($nbAttempts >= $this->getNbAttemptsBeforeBlocked()) {
                $this->log(">> Adding IP to blacklist");
                $this->addToBlacklistTemporary($ip, $nbAttempts);
                return true;
            } else {
                $this->log(">> No need to blacklist the IP");
            }
        }
        return false;
    }

    /**
     * @param $ip
     * @param $nbAttempts
     * @throws \Exception
     */
    public function addToBlacklistTemporary(
        $ip,
        $nbAttempts
    )
    {
        $blockDuration = $this->getBlockDuration();
        $this->log(">> Temporary blacklist duration : $blockDuration");
        if (!$this->isBlacklisted($ip)) {
            if ($blockDuration != 0) {
                $date = $this->_timezone->date();
                $dateInterval = new \DateInterval("PT" . ($blockDuration * 60) . "S");
                $date->add($dateInterval);
                $until = $date->format('Y-m-d H:i:s');
            } else {
                $until = null;
            }
            $this->log(">> Adding $ip to blacklist until $until");
            $this->addToBlacklist($ip, $until);
            $this->sendNotificationTempBlacklist($ip, $until, $nbAttempts);
        }
    }

    /**
     * @param $ip
     * @param $until
     * @param $nbAttempts
     */
    public function sendNotificationTempBlacklist(
        $ip,
        $until,
        $nbAttempts
    )
    {

        $template = "wyomind_watchlogpro_ip_blocked_report";
        $emails = explode(',', $this->_framework->getDefaultConfig("watchlogpro/reporting/emails"));

        if ($this->_framework->getDefaultConfig("watchlogpro/reporting/enable_reporting") && count($emails) > 0) {
            $emailTemplateVariables = [
                "data" => [
                    "ip" => $ip,
                    "nb_attempts" => $nbAttempts,
                    "until" => $this->_timezone->formatDateTime($until)
                ],
                "subject" => $this->_framework->getDefaultConfig('watchlogpro/reporting/report_title')
            ];

            $transport = $this->_transportBuilderFactory->create()
                ->setTemplateIdentifier($template)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                    ]
                )
                ->setTemplateVars($emailTemplateVariables)
                ->setFrom(
                    [
                        'email' => $this->_framework->getDefaultConfig('watchlogpro/reporting/sender_email'),
                        'name' => $this->_framework->getDefaultConfig('watchlogpro/reporting/sender_name')
                    ]
                )
                ->addTo($emails[0]);

            $count = count($emails);
            for ($i = 1; $i < $count; $i++) {
                $transport->addCc($emails[$i]);
            }

            $transport->getTransport()->sendMessage();
        }
    }
}
