<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\WatchlogPro\Logger;

/**
 * Class Handler
 * @package Wyomind\WatchlogPro\Logger
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * @var string
     */
    public $fileName = '/var/log/WatchlogPro.log';
    /**
     * @var int
     */
    public $loggerType = \Monolog\Logger::NOTICE;
    /**
     * @param \Magento\Framework\Filesystem\DriverInterface $filesystem
     * @param string $filePath
     * @throws \Exception
     */
    public function __construct(\Magento\Framework\Filesystem\DriverInterface $filesystem, $filePath = null)
    {
        parent::__construct($filesystem, $filePath);
        $this->setFormatter(new \Monolog\Formatter\LineFormatter("[%datetime%] %message%
", null, true));
    }
}