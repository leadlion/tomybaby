<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Logger;

/**
 * Class Logger
 * @package Wyomind\WatchlogPro\Logger
 */
class Logger extends \Monolog\Logger
{

    /**
     * @var \Magento\Framework\Stdlib\Datetime\Timezone|null
     */
    protected $_timezone = null;

    /**
     * Logger constructor.
     * @param string $name
     * @param \Magento\Framework\Stdlib\Datetime\Timezone $timezone
     * @param array $handlers
     * @param array $processors
     */
    public function __construct(
        $name,
        \Magento\Framework\Stdlib\Datetime\Timezone $timezone,
        array $handlers = [],
        array $processors = []
    ) {
        $this->_timezone = $timezone;
        parent::__construct($name, $handlers, $processors);
    }

    /**
     * @param int $level
     * @param string $message
     * @param array $context
     * @return bool
     */
    public function addRecord(
        $level,
        $message,
        array $context = []
    ) {
        if (!$this->handlers) {
            $this->pushHandler(new StreamHandler('php://stderr', static::DEBUG));
        }

        $levelName = static::getLevelName($level);

        // check if any handler will handle this message so we can return early and save cycles
        $handlerKey = null;
        foreach ($this->handlers as $key => $handler) {
            if ($handler->isHandling(['level' => $level])) {
                $handlerKey = $key;
                break;
            }
        }

        if (null === $handlerKey) {
            return false;
        }


        $record = [
            'message' => str_replace("\n", "\n                      ", $message),
            'context' => $context,
            'level' => $level,
            'level_name' => $levelName,
            'channel' => $this->name,
            'datetime' => $this->_timezone->date()->format('Y-m-d H:i:s'),
            'extra' => [],
        ];

        foreach ($this->processors as $processor) {
            $record = call_user_func($processor, $record);
        }

        while (isset($this->handlers[$handlerKey]) &&
        false === $this->handlers[$handlerKey]->handle($record)) {
            $handlerKey++;
        }

        return true;
    }
}
