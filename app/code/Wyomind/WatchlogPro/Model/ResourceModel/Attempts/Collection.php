<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Model\ResourceModel\Attempts;

/**
 * Custom functions collection
 */
class Collection extends \Wyomind\Watchlog\Model\ResourceModel\Attempts\Collection
{

    /**
     * @var null|\Wyomind\WatchlogPro\Helper\Data
     */
    protected $_watchlogProHelper = null;

    /**
     * Collection constructor.
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Wyomind\Watchlog\Helper\Data $watchlogHelper
     * @param \Magento\Framework\Stdlib\DateTime\Datetime $datetime
     * @param \Wyomind\WatchlogPro\Helper\Data $watchlogProHelper
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Wyomind\Watchlog\Helper\Data $watchlogHelper,
        \Magento\Framework\Stdlib\DateTime\Datetime $datetime,
        \Wyomind\WatchlogPro\Helper\Data $watchlogProHelper,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->_watchlogProHelper = $watchlogProHelper;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $watchlogHelper, $datetime, $connection, $resource);
    }

    /**
     * @param null $date
     * @return $this|\Wyomind\Watchlog\Model\ResourceModel\Attempts\Collection
     */
    public function getHistory($date = null)
    {
        if ($date != null) {
            $this->addFieldToFilter('date', ['gteq' => $date]);
        }
        $this->getSelect()
                ->columns('COUNT(id) AS attempts')
                ->columns('MAX(date) AS date')
                ->columns('SUM(IF(status=0,1,0)) AS failed')
                ->columns('SUM(IF(status=1,1,0)) AS succeeded')
                ->order("SUM(IF(status=0,1,0)) DESC")
                ->group("ip");
        return $this;
    }

    /**
     * @param $ip
     * @return null
     */
    public function getLastSuccessfulAttempt($ip)
    {
        $this->getSelect()->columns('MAX(date) AS date')->where("ip = '$ip' AND `status` = " . \Wyomind\Watchlog\Helper\Data::SUCCESS);
        $row = $this->getFirstItem();
        if ($row != null) {
            return $row->getDate();
        } else {
            return null;
        }
    }

    /**
     * @param $date
     * @param $ip
     * @return int
     */
    public function getNbFailureAttemptsAfter(
        $date,
        $ip
    ) {
        $this->getSelect()->columns("COUNT(id) AS nb")->where("date >= '" . $date . "' AND ip='" . $ip . "' AND `status` = " . \Wyomind\Watchlog\Helper\Data::FAILURE);
        $row = $this->getFirstItem();
        if ($row != null) {
            return $row->getNb();
        } else {
            return 0;
        }
    }
}
