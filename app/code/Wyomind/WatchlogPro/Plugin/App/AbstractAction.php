<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\WatchlogPro\Plugin\App;

/**
 *
 */
class AbstractAction
{
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory|null
     */
    public $_resultRawFactory = null;
    public $_watchlogProHelper = null;
    /**
     * @var null|\Wyomind\Watchlog\Model\AttemptsFactory
     */
    public $_attemptsFactory = null;
    public $_timezone = null;
    public $_datetime = null;
    /**
     * @var null
     */
    public $_request = null;
    public $framework = null;
    public $messageManager = null;
    public function __construct(\Wyomind\WatchlogPro\Helper\Delegate $wyomind, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Wyomind\Watchlog\Model\AttemptsFactory $attemptsFactory)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->_resultRawFactory = $resultRawFactory;
        $this->_attemptsFactory = $attemptsFactory;
    }
    /**
     * @param $ip
     */
    public function addBlockedRow($ip)
    {
        if ($this->_watchlogProHelper->isWhitelisted($ip)) {
            $status = \Wyomind\WatchlogPro\Helper\Data::WHITELISTED;
        } elseif ($this->_watchlogProHelper->isBlacklisted($ip)) {
            $status = \Wyomind\WatchlogPro\Helper\Data::BLACKLISTED;
        } else {
            $status = \Wyomind\WatchlogPro\Helper\Data::IGNORED;
        }
        $data = ["login" => "", "password" => "", "ip" => $ip, "date" => $this->_datetime->gmtTimestamp(), "status" => \Wyomind\WatchlogPro\Helper\Data::BLOCKED, "message" => "", "url" => $this->_request->getRequestUri(), "ip_status" => $status];
        $attempt = $this->_attemptsFactory->create()->load(0);
        $attempt->setData($data);
        $attempt->save();
    }
    /**
     * @param \Magento\Backend\App\AbstractAction $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\App\RequestInterface $request
     * @return mixed|null
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function aroundDispatch(\Magento\Backend\App\AbstractAction $subject, \Closure $proceed, \Magento\Framework\App\RequestInterface $request)
    {
        $this->_watchlogProHelper->log(">>>>>>>>>> New Admin URL Called <<<<<<<<<<");
        $this->_request = $request;
        $resultRaw = $this->_resultRawFactory->create();
        $result = null;
        $blockMessage = $this->framework->getDefaultConfig("watchlogpro/white_black_list/blocked_message");
        $ip = strtok($request->getClientIp(), ',');
        $this->_watchlogProHelper->log(">> URL : " . $this->_request->getRequestUri());
        $this->_watchlogProHelper->log(">> IP : " . $ip);
        $this->_watchlogProHelper->log(">> WHITELIST : " . $this->_watchlogProHelper->getWhitelistToString());
        $this->_watchlogProHelper->log(">> BLACKLIST : " . $this->_watchlogProHelper->getBlacklistToString());
        if ($this->_watchlogProHelper->isWhitelisted($ip)) {
            // WHITELISTED ?
            $this->_watchlogProHelper->log(">>>> IP whitelisted : proceed");
            $result = $proceed($request);
        } elseif ($this->_watchlogProHelper->getUseWhitelistOnly()) {
            // ONLY WHITELISTED ?
            $this->_watchlogProHelper->log(">>>> IP not whitelisted, use only whitelisted IPs");
            $result = $resultRaw->setContents($blockMessage);
            $this->addBlockedRow($ip);
        } elseif ($this->_watchlogProHelper->isBlacklisted($ip)) {
            // BLACKLISTED ?
            $this->_watchlogProHelper->log(">>>> IP balcklisted");
            $until = $this->_watchlogProHelper->blockedUntil($ip);
            $this->_watchlogProHelper->log(">>>> Until : {$until}");
            $now = $this->_timezone->date()->format('Y-m-d H:i:s');
            $this->_watchlogProHelper->log(">>>> Current time : " . $now);
            if ($until != "" && $until < $now) {
                $this->_watchlogProHelper->log(">>>> IP removed from the blacklist : proceed");
                $this->_watchlogProHelper->removeFromBlacklist($ip);
                $result = $proceed($request);
            } else {
                $this->_watchlogProHelper->log(">>>> IP still blocked");
                $result = $resultRaw->setContents($blockMessage);
                $this->addBlockedRow($ip);
            }
        } else {
            $this->_watchlogProHelper->log(">>>>>> No restrictions for this IP : proceed");
            $result = $proceed($request);
        }
        return $result;
    }
}