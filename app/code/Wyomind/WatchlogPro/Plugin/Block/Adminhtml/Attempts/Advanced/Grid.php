<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Plugin\Block\Adminhtml\Attempts\Advanced;

/**
 * Class Grid
 * @package Wyomind\WatchlogPro\Plugin\Block\Adminhtml\Attempts\Advanced
 */
class Grid
{

    /**
     * @param $subject
     * @param $return
     * @return mixed
     */
    public function after_prepareCollection($subject, $return)
    {
        $subject->addColumn('blocked', ['header' => __('Blocked'), 'index' => 'blocked']);

        $subject->addColumn('page_actions', [
            'header' => __('Action'),
            'sortable' => false,
            'filter' => false,
            'renderer' => "\Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer\Action",
        ]);
        
        
        $subject->getColumn('ip')->setData('renderer', 'Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer\IpAdvanced');
     
        return $return;
        
    }
}
