<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Plugin\Block\Adminhtml\Attempts\Basic;

/**
 * Class Grid
 * @package Wyomind\WatchlogPro\Plugin\Block\Adminhtml\Attempts\Basic
 */
class Grid
{

    /**
     * @param $subject
     * @param $return
     * @return mixed
     */
    public function after_prepareCollection($subject, $return)
    {
        $subject->getColumn('ip')->setData('renderer', 'Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer\IpBasic');
        $subject->getColumn('status')->setData('renderer', 'Wyomind\WatchlogPro\Block\Adminhtml\Attempts\Renderer\Status');
        
        return $return;
    }
}
