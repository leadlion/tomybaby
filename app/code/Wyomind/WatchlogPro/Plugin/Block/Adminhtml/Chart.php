<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Wyomind\WatchlogPro\Plugin\Block\Adminhtml;

/**
 * 
 */
class Chart
{
    private $_datetime = null;
    /**
     * @var null|\Wyomind\Watchlog\Model\ResourceModel\Attempts\CollectionFactory
     */
    private $_attemptsCollectionFactory = null;
    protected $_framework = null;
    public function __construct(\Wyomind\WatchlogPro\Helper\Delegate $wyomind, \Wyomind\Watchlog\Model\ResourceModel\Attempts\CollectionFactory $attemptsCollectionFactory)
    {
        $wyomind->constructor($this, $wyomind, __CLASS__);
        $this->_attemptsCollectionFactory = $attemptsCollectionFactory;
    }
    /**
     * @param $subject
     * @param $proceed
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function aroundGetChartDataSummaryMonth($subject, $proceed)
    {
        $data = [];
        $headers = [__('Date'), __('Success'), __('Failed'), __('Blocked')];
        $data[] = $headers;
        $tmpData = [];
        $nbDays = $this->_framework->getDefaultConfig("watchlogpro/settings/history");
        $currentTimestamp = $this->_datetime->gmtTimestamp() + $this->_datetime->getGmtOffset();
        $yestermonthTimestamp = $currentTimestamp - ($nbDays - 1) * 24 * 60 * 60;
        while ($yestermonthTimestamp <= $currentTimestamp) {
            $key = $this->_datetime->date('Y-m-d', $yestermonthTimestamp);
            $tmpData[$key] = [\Wyomind\Watchlog\Helper\Data::FAILURE => 0, \Wyomind\Watchlog\Helper\Data::SUCCESS => 0, \Wyomind\WatchlogPro\Helper\Data::BLOCKED => 0];
            $yestermonthTimestamp += 24 * 60 * 60;
        }
        $collection = $this->_attemptsCollectionFactory->create()->getSummaryMonth();
        foreach ($collection as $entry) {
            $key = $this->_datetime->date('Y-m-d', strtotime($entry->getDate()) + $this->_datetime->getGmtOffset());
            if (!isset($tmpData[$key])) {
                $tmpData[$key] = [\Wyomind\Watchlog\Helper\Data::FAILURE => 0, \Wyomind\Watchlog\Helper\Data::SUCCESS => 0, \Wyomind\WatchlogPro\Helper\Data::BLOCKED => 0];
            }
            $tmpData[$key][$entry->getStatus()] = $entry->getNb();
        }
        ksort($tmpData);
        foreach ($tmpData as $date => $entry) {
            $data[] = ["#new Date('" . $date . "')#", (int) $entry[\Wyomind\Watchlog\Helper\Data::SUCCESS], (int) $entry[\Wyomind\Watchlog\Helper\Data::FAILURE], (int) $entry[\Wyomind\WatchlogPro\Helper\Data::BLOCKED]];
        }
        return $data;
    }
    /**
     * @param $subject
     * @param $proceed
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function aroundGetChartDataSummaryDay($subject, $proceed)
    {
        $data = [];
        $headers = [__('Date'), __('Success'), __('Failed'), __('Blocked')];
        $data[] = $headers;
        $tmpData = [];
        $currentTimestamp = $this->_datetime->gmtTimestamp() + $this->_datetime->getGmtOffset();
        $yesterdayTimestamp = $currentTimestamp - 23 * 60 * 60;
        while ($yesterdayTimestamp <= $currentTimestamp) {
            $key = $this->_datetime->date('M d, Y H:00:00', $yesterdayTimestamp);
            $tmpData[$key] = [\Wyomind\Watchlog\Helper\Data::FAILURE => 0, \Wyomind\Watchlog\Helper\Data::SUCCESS => 0, \Wyomind\WatchlogPro\Helper\Data::BLOCKED => 0];
            $yesterdayTimestamp += 60 * 60;
        }
        $collection = $this->_attemptsCollectionFactory->create()->getSummaryDay();
        foreach ($collection as $entry) {
            $key = $this->_datetime->date('M d, Y H:00:00', strtotime($entry->getDate()) + $this->_datetime->getGmtOffset());
            if (!isset($tmpData[$key])) {
                $tmpData[$key] = [\Wyomind\Watchlog\Helper\Data::FAILURE => 0, \Wyomind\Watchlog\Helper\Data::SUCCESS => 0, \Wyomind\WatchlogPro\Helper\Data::BLOCKED => 0];
            }
            $tmpData[$key][$entry->getStatus()] = $entry->getNb();
        }
        foreach ($tmpData as $date => $entry) {
            $data[] = ["#new Date('" . $date . "')#", (int) $entry[\Wyomind\Watchlog\Helper\Data::SUCCESS], (int) $entry[\Wyomind\Watchlog\Helper\Data::FAILURE], (int) $entry[\Wyomind\WatchlogPro\Helper\Data::BLOCKED]];
        }
        return $data;
    }
}