<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Plugin\Cron;

/**
 * Plugin for Watchlog\Cron\PeriodicalReport
 * => add the 'blocked' column to the report
 */
class PeriodicalReport
{

    /**
     * @param $subject
     * @param $closure
     * @param $date
     * @return array
     */
    public function aroundGetHistory($subject, $closure, $date)
    {
        $history = $subject->getAttemptsCollectionFactory()->create()->getHistory($date);
        $data = [];
        foreach ($history as $line) {
            $data[] = [
                "ip" => $line->getIp(),
                "attempts" => $line->getAttempts(),
                "date" => $line->getDate(),
                "failed" => $line->getFailed(),
                "succeeded" => $line->getSucceeded(),
                "blocked" => $line->getBlocked()
            ];
        }
        return $data;
    }

    /**
     * @param $subject
     * @param $proceed
     * @return string
     */
    public function aroundGetTemplate($subject, $proceed)
    {
        return "wyomind_watchlogpro_periodical_report";
    }
}
