<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Plugin\Helper;

/**
 * Class Data
 * @package Wyomind\WatchlogPro\Plugin\Helper
 */
class Data
{

    /**
     * @param $subject
     * @param $proceed
     * @param $key
     * @return mixed
     */
    public function aroundGetDefaultConfig($subject, $proceed, $key)
    {
        $newKey = str_replace("watchlog/", "watchlogpro/", $key);
        return $proceed($newKey);
    }

    /**
     * @param $subject
     * @param $proceed
     * @param $key
     * @param $value
     * @return mixed
     */
    public function aroundSetDefaultConfig($subject, $proceed, $key, $value)
    {
        $newKey = str_replace("watchlog/", "watchlogpro/", $key);
        return $proceed($newKey,$value);
    }
}
