<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Plugin\Model;

/**
 * 
 */
class Auth
{

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime|null
     */
    private $_datetime = null;
    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\Request|null
     */
    private $_request = null;
    /**
     * @var null|\Wyomind\Watchlog\Model\AttemptsFactory
     */
    private $_attemptsFactory = null;
    /**
     * @var null|\Wyomind\Watchlog\Helper\Data
     */
    public $_watchlogHelper = null;
    /**
     * @var null|\Wyomind\WatchlogPro\Helper\Data
     */
    private $_watchlogProHelper = null;
    /**
     * @var null|\Wyomind\Framework\Helper\Module
     */
    public $framework = null;

    /**
     * Auth constructor.
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $datetime
     * @param \Magento\Framework\HTTP\PhpEnvironment\Request $request
     * @param \Wyomind\Watchlog\Model\AttemptsFactory $attemptsFactory
     * @param \Wyomind\Watchlog\Helper\Data $watchlogHelper
     * @param \Wyomind\WatchlogPro\Helper\Data $watchlogProHelper
     * @param \Wyomind\Framework\Helper\Module $framework
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        \Magento\Framework\HTTP\PhpEnvironment\Request $request,
        \Wyomind\Watchlog\Model\AttemptsFactory $attemptsFactory,
        \Wyomind\Watchlog\Helper\Data $watchlogHelper,
        \Wyomind\WatchlogPro\Helper\Data $watchlogProHelper,
        \Wyomind\Framework\Helper\Module $framework
    ) {
        $this->_datetime = $datetime;
        $this->_request = $request;
        $this->_attemptsFactory = $attemptsFactory;
        $this->_watchlogHelper = $watchlogHelper;
        $this->_watchlogProHelper = $watchlogProHelper;
        $this->framework = $framework;
    }

    /**
     * @param \Wyomind\Watchlog\Model\Auth $auth
     * @param \Closure $closure
     * @param $login
     * @param $password
     * @param null $e
     * @return mixed|null
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function aroundAddAttempt(
        \Wyomind\Watchlog\Model\Auth $auth,
        \Closure $closure,
        $login,
        $password,
        $e = null
    ) {
        $this->_watchlogProHelper->log(">>>>>>>>>> New incoming attempt <<<<<<<<<<");
       
        
        
        
        
        
        

        $ip = strtok($this->_request->getClientIp(), ',');
        
        $this->_watchlogProHelper->log(">> IP : $ip");
        
        if ($this->_watchlogProHelper->isWhitelisted($ip)) {
            $this->_watchlogProHelper->log(">> IP whitelisted");
            $status = \Wyomind\WatchlogPro\Helper\Data::WHITELISTED;
        } elseif ($this->_watchlogProHelper->isBlacklisted($ip)) {
            $this->_watchlogProHelper->log(">> IP blacklisted");
            $status = \Wyomind\WatchlogPro\Helper\Data::BLACKLISTED;
        } else {
            $this->_watchlogProHelper->log(">> IP not whitelisted, not blacklisted");
            $status = \Wyomind\WatchlogPro\Helper\Data::IGNORED;
        }
        
        $data = [
            "login" => $login,
            "password" => $password,
            "ip" => $ip,
            "date" => $this->_datetime->gmtDate('Y-m-d H:i:s'),
            "status" => \Wyomind\Watchlog\Helper\Data::SUCCESS,
            "message" => "",
            "url" => $this->_request->getRequestUri(),
            "ip_status" => $status
        ];

        if ($e != null) { // failed
            $this->_watchlogProHelper->log(">> Attempt failure");
            $data['password'] = $password;
            $data['status'] = \Wyomind\Watchlog\Helper\Data::FAILURE;
            $data['message'] = $e->getMessage();
        } else { // success
            $this->_watchlogProHelper->log(">> Attempt successful");
            $data['password'] = "***";
            $this->_watchlogHelper->checkNotification();
        }

        $attempt = $this->_attemptsFactory->create()->load(0);
        $attempt->setData($data);
        $attempt->save();
        
        if ($e != null) {
            $this->_watchlogProHelper->log(">> Do we need to block the IP from now ?");
            $blocked = $this->_watchlogProHelper->checkNeedToBlacklist($ip);
            if ($blocked) {
                $auth->throwException(__("Too many attempts ! Your IP have been blacklisted."));
            }
        } else {
            $this->_watchlogProHelper->log(">> Successfully logged !!");
        }
        
        return null;
    }
}
