<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Plugin\Model\ResourceModel\Attempts;

/**
 * Custom functions collection
 */
class Collection
{

    /**
     * @param $subject
     * @param $closure
     * @param null $date
     * @return mixed
     */
    public function aroundGetHistory(
        $subject,
        $closure,
        $date = null
    ) {
        $collection = $closure($date);
        $collection->getSelect()->columns('SUM(IF(`status`=2,1,0)) as blocked');
        return $collection;
    }
}
