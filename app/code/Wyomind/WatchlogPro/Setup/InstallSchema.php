<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Install schema
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * @version 2.0.0
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable('watchlog'),
            'ip_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'default' => '0',
                'length' => 1,
                "comment" => "Enable SSL"
            ]);


        $installer->endSetup();
    }
}
