<?php
/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Setup;

/**
 * Class Recurring
 * @package Wyomind\WatchlogPro\Setup
 */
class Recurring implements \Magento\Framework\Setup\InstallSchemaInterface
{


    /**
     * @var null|\Wyomind\Framework\Helper\Module
     */
    private $_framework = null;

    /**
     * Recurring constructor.
     * @param \Wyomind\Framework\Helper\Install $framework
     */
    public function __construct(
    \Wyomind\Framework\Helper\Install $framework
    )
    {
        $this->_framework = $framework;
    }

    /**
     * {@inheritdoc}
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $files = [
            "etc/config.xml"
        ];
        $this->_framework->copyFilesByMagentoVersion(__FILE__, $files);

    }

}
