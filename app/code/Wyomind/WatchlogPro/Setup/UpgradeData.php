<?php

/**
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wyomind\WatchlogPro\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Upgrade data
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var \Magento\Store\Model\ResourceModel\Store\CollectionFactory
     */
    protected $_storeCollectionFactory = null;

    /**
     * UpgradeData constructor.
     * @param \Wyomind\Framework\Helper\Module $framework
     * @param \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Cms\Model\Page $pageModel
     * @param \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageCollectionFactory
     * @param \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $customerGroupCollectionFactory
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Framework\App\DeploymentConfig $deploymentConfig
     */
    public function __construct(
    \Wyomind\Framework\Helper\Module $framework,
            \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\Cms\Model\Page $pageModel,
            \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageCollectionFactory,
            \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $customerGroupCollectionFactory,
            \Magento\Framework\App\State $state,
            \Magento\Framework\App\DeploymentConfig $deploymentConfig
    )
    {
        $this->_framework = $framework;
        $this->_storeCollectionFactory = $storeCollectionFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup,
            ModuleContextInterface $context)
    {

        if (version_compare($context->getVersion(), '2.0.5') < 0) {
            $installer = $setup;
            $installer->startSetup();

            try {
                $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
            } catch (\Exception $e) {
                
            }
            try {
                // @codingStandardsIgnoreStart
                $blacklist = @unserialize($this->_framework->getDefaultConfig("watchlogpro/white_black_list/blacklist", 0));
                // @codingStandardsIgnoreEnd
            } catch (\Exception $e) {
                $blacklist = [];
            }
            if (!$blacklist) { // mage 2.2
                $blacklist = json_decode($this->_framework->getDefaultConfig("watchlogpro/white_black_list/blacklist", 0), true);
                $newBlacklist = [];
                if ($blacklist) {
                    foreach ($blacklist as $key => $info) {
                        if (isset($info["ip"])) {
                            $newBlacklist[$key] = [
                                "bip" => $info["ip"],
                                "until" => $info["until"]
                            ];
                        } else {
                            $newBlacklist[$key] = $info;
                        }
                    }
                }
                $blacklist = json_encode($newBlacklist);
                $this->_framework->setDefaultConfig("watchlogpro/white_black_list/blacklist", $blacklist);
            } else { // mage < 2.2
                $newBlacklist = [];
                if ($blacklist) {
                    foreach ($blacklist as $key => $info) {
                        if (isset($info["ip"])) {
                            $newBlacklist[$key] = [
                                "bip" => $info["ip"],
                                "until" => $info["until"]
                            ];
                        } else {
                            $newBlacklist[$key] = $info;
                        }
                    }
                }
                $blacklist = serialiaze($newBlacklist);
                $this->_framework->setDefaultConfig("watchlogpro/white_black_list/blacklist", $blacklist);
            }

            $installer->endSetup();
        }
    }

}
