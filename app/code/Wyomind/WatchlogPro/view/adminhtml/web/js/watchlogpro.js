/*
 * Copyright © 2019 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

require(["jquery", "mage/mage"], function ($) {
    $(function () {
        jQuery(document).ready(function () {
           
            if (jQuery('#watchlog_settings')) {
                jQuery(jQuery('#watchlog_settings').parent()[0]).css({display:"none"});
                jQuery(jQuery('#watchlog_settings').parent()[0]).remove();
                jQuery(jQuery('#watchlog_periodical_report').parent()[0]).css({display:"none"});
                jQuery(jQuery('#watchlog_periodical_report').parent()[0]).remove();
            }
            
        });
    });
});